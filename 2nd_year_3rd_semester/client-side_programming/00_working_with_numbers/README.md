## 00 Task:

1. Write a programme to check whether two natural (integer) numbers are mutually prime.
2. Write a programme that checks whether a given natural number is prime.
3. Write a programme that displays all prime numbers from the interval 1..N using the Eratosthenes Sieve.
4. Write a programme that displays the first N prime numbers.
5. Find all divisors of the natural number N.
6. Decompose an integer into prime multipliers. Display all prime multipliers (in ascending order) and their orders.
7. A natural number is called perfect if it is equal to the sum of all its divisors, including one.
of its divisors, including one. Print the first N (N<5) perfect numbers on the screen.
numbers on the screen.
8. Check which odd natural numbers from the interval N..M can be represented as the sum of three prime numbers.
9. Check whether the given number will be a Fibonacci number.
10. Calculate (N)!!! where
(2N)!!    = 2*4*...*(2N)
(2N+1)!!  = 1*3*...*(2N+1).
11. Find all distinct Pythagorean triples from the interval from N to M.
12. Write a programme to multiply (divide) two given rational numbers. The answer must be an irreducible fraction.
13. Write a programme to add (subtract) two given rational numbers. The answer must be an irreducible fraction.
14. Find all integers in the interval from N to M that are divisible by each of their digits.
15. Find all integers from N to M that are divisible by the sum of all their digits.

## Demonstration of work [script.js](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/client-side_programming/00_working_with_numbers/script.js):

![1](1.png "1")

PS: [script110.js](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/client-side_programming/00_working_with_numbers/script110.js): "Программа 1.10 15"