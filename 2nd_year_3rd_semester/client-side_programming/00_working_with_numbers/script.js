﻿function isСoprime_1_1(a, b)
{
	for (let c = 2; c <= Math.min(a, b); c++)
	{
		if (!(a % c) && !(b % c))
		{
			return false;
		}
	}
	return true;
}


function isPrime_1_2(a)
{
	for (let c = 2; c < (a / 2); c++)
	{
		if (!(a % c)) 
		{
			return false;
		}
	}
	if (a == 1)
	{
		return false;
	}
	return true;
}


function sieveEratosthenes_1_3(n) 
{
	n += 2;
	let lst = [];
	for (let i = 2; i <= n; i++)
	{
		lst.push(true);
	}
	for (let i = 2; (i * i) <= n; i++)
	{
		if (lst[i] === true)
		{
			for (let c = 0, j = (i ** 2); j <= n; j = (i ** 2) + (c * i), c++)
			{
				lst[j] = false;
			}
		}
	}
	let lst2 = [];
	for (let i = 2; i < lst.length; i++)
	{
		if (lst[i] === true) 
		{
			lst2.push(i);
		}
	}
	return lst2;
}


function primeNumbers_1_4(n)
{
	let lst = [];
	let check = false;
	for (let i = 2; lst.length < n; i++)
	{
		check = false;
		for (let j = 2; j <= (i / 2); j++)
		{
			if (!(i % j))
			{
				check = true;
				break;
			}
		}
		if (!(check))
		{
			lst.push(i);
		}
	}
	return lst;
}


function divisors_1_5(n)
{
	let lst = [];
	for (let i = 1; i <= n; i++)
	{
		if (!(n % i))
		{
			lst.push(i);
		}
	}
	return lst;
}


function factorize_1_6(n)
{
	let lst = [];
	c = n;
	for (let i = 2; i <= c; i++)
	{
		while (!(c % i))
		{
			lst.push(i);
			c = c / i;
		}
	}
	let lst2 = [];
	for (let i = 1, c = 1; i <= lst.length; i++)
	{
		if (lst[i] === lst[i - 1])
		{
			c++;
		}
		else
		{
			lst2.push(c);
			c = 1;
		}
	}
	let lst_set = Array.from(new Set(lst));
	let lst3 = [];
	for (let i = 0; i < lst2.length; i++)
	{
		lst3.push(lst_set[i] + "^" + lst2[i]);
	}
	return lst3;
}


function perfectNumbers_1_7(n)
{
	let lst = [];
	let lst_d = [];
	for (let i = 1; lst.length < n; i++)
	{
		while(lst_d.length > 0)
		{
			lst_d.pop();
		}
		for (let j = 1; j <= (i / 2); j++)
		{
			if (i % j === 0)
			{
				lst_d.push(j);
			}
		}
		if (sumLst(lst_d) === i)
		{
			lst.push(i);
		}
	}
	return lst;
}


function sumLst(a)
{
	let sum_list = 0;
	for (let i = 0; i < a.length; i++)
	{
		sum_list += a[i];
	}
	return sum_list;
}


function sumThreePrimes_1_8(n, m)
{
	let lst = [];
	for (let i = n; i <= m; i++)
	{
		if (i % 2 && i > 5)
		{
			let lst2 = sieveEratosthenes_1_3(i);
			let c = lst2.length - 3;
			let razn = i - lst2[c];
			let lef = (razn / 2);
			let rig = (razn / 2);
			while (isPrime_1_2(lef) === false || isPrime_1_2(rig) === false)
			{
				lef++;
				rig--;
			}
			lst.push([i, [lst2[c], lef, rig]]);
		}
	}
	return lst;
}


function isFibonacci_1_9(a)
{
	let summ = 0;
	let lst = [0, 1];
	for (; summ < a;)
	{
		summ = lst[lst.length - 1] + lst[lst.length - 2]
		if (a === summ)
		{
			return true;
		}
		lst.push(summ)
	}
	return false;
}


function isFactorial_1_10(a)
{
	let summ = 1;
	for (let i = 1; i <= a; i++)
	{
		summ *= i;
	}
	return summ;
}


function pythagoreanTriple_1_11(n, m)
{
	let lst = [];
	for (let i = n + 1; i <= m; i++)
	{
		for (let j = i - 1; j > 0; j--)
		{
			let c = i * i + (i - j) * (i - j)
			if (c ** .5 <= m && Number.isInteger(c ** .5))
			{
				lst.push([i - j, i, c ** 0.5]);
			}
		}
	}
	return lst;
}


function multiplicationAndDivisionRationalNumbers_1_12(a1, b1, a2, b2, operation)
{
	let a = 0
	let b = 0
	if (operation === 0)
	{
		a = a1 * a2;
		b = b1 * b2;
	}
	else
	{
		a = a1 * b2;
		b = b1 * a2;
	}
	let c = 1;
	for (let i = 1; i <= Math.min(a, b); i++)
	{
		if (((a % i === 0) && (b % i === 0)))
		{
			c = i;
		}
	}
	return (a/c + "/" + b/c);
}


function additioAndSubtractionRationalNumbers_1_13(a1, b1, a2, b2, operation)
{
	let a = 0;
	let b = 0;
	if (operation === 0)
	{
		if (b1 == b2)
		{
			a = a1 + a2;
			b = b1;
		}
		else
		{
			b = b1 * b2;
			a = a1 * (b / b1) + a2 * (b / b2);
		}
	}
	else
	{
		if (b1 == b2)
		{
			a = a1 - a2;
			b = b1;
		}
		else
		{
			b = b1 * b2;
			a = a1 * (b / b1) - a2 * (b / b2);
		}
	}
	let c = 1;
	for (let i = 1; i <= Math.min(Math.abs(a), b); i++)
	{
		if (((Math.abs(a) % i === 0) && (b % i === 0)))
		{
			c = i;
		}
	}
	return (a/c + "/" + b/c);
}


function divisionByEachItsDigits_1_14(n, m)
{
	let lst = [];
	for (let i = n; i <= m; i++)
	{
		let e = 0;
		let d = 0;
		for (let a = Math.abs(i); a > 0; a = Math.floor(a/10), d++)
		{
			if ((a % 10) != 0 && i % (a % 10) == 0)
			{
				e++;
			}
		}
		if (d === e && e !== 0)
		{
			lst.push(i);
		}
	}
	return lst;
}


function divisionBySumEachItsDigits_1_15(n, m)
{
	let lst = [];
	for (let i = n; i <= m; i++)
	{
		let summ = 0;
		for (let a = Math.abs(i); a > 0; a = Math.floor(a/10))
		{
			if ((a % 10) !== 0)
			{
				summ += a % 10;
			}
		}
		if (i % summ === 0)
		{
			lst.push(i);
		}
	}
	return lst;
}


console.log("Программа 1.1", isСoprime_1_1(8, 15));
console.log("Программа 1.2", isPrime_1_2(41));
console.log("Программа 1.3", sieveEratosthenes_1_3(41));
console.log("Программа 1.4", primeNumbers_1_4(10));
console.log("Программа 1.5", divisors_1_5(41));
console.log("Программа 1.6", factorize_1_6(23412512));
console.log("Программа 1.7", perfectNumbers_1_7(4));
console.log("Программа 1.8", sumThreePrimes_1_8(1, 10)); // 370373: [370247, 67, 59]
console.log("Программа 1.9", isFibonacci_1_9(8));
console.log("Программа 1.10", isFactorial_1_10(10));
console.log("Программа 1.11", pythagoreanTriple_1_11(0, 20));
console.log("Программа 1.12", multiplicationAndDivisionRationalNumbers_1_12(1, 3, 1, 24, 1));
console.log("Программа 1.13", additioAndSubtractionRationalNumbers_1_13(1, 3, 2, 4, 1));
console.log("Программа 1.14", divisionByEachItsDigits_1_14(-15, 100));
console.log("Программа 1.15", divisionBySumEachItsDigits_1_15(-45, 45))
