﻿function camelize_2_1(str)
{
	let arr = str.split('-');
	for (let i = 1; i < arr.length; i++)
	{
		arr[i] = arr[i].at(0).toUpperCase() + arr[i].slice(1);
	}
	return arr.join("");
}

function program_2_2(arr)
{
	let firstCopy = Array.from(arr);
	let secondCopy = [];
	for (let i = 0; i < arr.length; i++)
	{
		secondCopy.push(arr[i]);
	}
	firstCopy.reverse()
	for (let i = 0; i < arr.length / 2; i++)
	{
		let temp = secondCopy[i];
		secondCopy[i] = secondCopy[secondCopy.length - i - 1];
		secondCopy[secondCopy.length - i - 1] = temp;
	}
	return "Заданный массив:  " + "[" + arr + "],\nПервая переупорядоченная копия: [" + firstCopy + "],\nВторая переупорядоченная копия: [" + secondCopy + "]";
}

function translate_2_3(str, languageFrom, languageTo)
{
	let myMap = { "rus": ["ввод", "пользователь", "из", "клавиатура"],
	              "eng": ["input", "user", "from", "keyboard"],
	              "blr": ["увод", "карыстальнік", "з", "клавіятура"],
	              "chn": ["输入", "用户", "从", "键盘"] }
	let arr = str.split(" ");
	let result = [];
	for (let j = 0; j < arr.length; j++)
	{
		for (let i = 0; i < myMap[languageFrom].length; i++)
		{
			if (myMap[languageFrom][i] == arr[j])
			{
				result.push(myMap[languageTo][i]);
			}
		}
	}
	return result.join(" ");
}

function weekday_2_4()
{
	return returnWeekday()[(new Date).getDay()];
}

function returnWeekday()
{
	return {0: "воскресенье", 1: "понедельник", 2: "вторник", 3: "среда", 4: "четверг", 5: "пятница", 6: "суббота"}
}

function personal_2_5()
{
	let personal = {"Сергей" : "Блинник", "Владислав" : "Кочегар", "Максим" : "Плотник"}
	let personal_2 = Object.assign({}, personal);
	personal_2["Сергей"] = "Модельер";
	personal_2["Владислав"] = "Бариста";
	personal_2["Максим"] = "Кавалерист";
	return "\n" + show_personal(personal) + "\n" + show_personal(personal_2);
}

function show_personal(personal)
{
	let result = "";
	for (i in personal)
	{
		result += i + " : " + personal[i] + "\n";
	}
	return result;
}

function abbreviatedWeekday_2_6()
{
	let abbreviated_weekday = "вс,пн,вт,ср,чт,пт,сб";
	let weekday = returnWeekday();
	abbreviated_weekday = abbreviated_weekday.split(",");
	for (let i = 0; i < abbreviated_weekday.length; i++)
	{
		weekday[i] = abbreviated_weekday[i];
	}
	return weekday[(new Date).getDay()];
}

function addSubject_2_7(subj)
{
	let subjects = {"listSubjects" : "русский язык, геометрия, алгебра"}
	let arr = subjects["listSubjects"].split(",");
	for (let i = 0; i < subjects["listSubjects"].length; i++)
	{
		if (subj == arr[i])
		{
			arr.splice(i, 1);
			subjects["listSubjects"] = arr.join(",")
			return subjects["listSubjects"];
		}
	}
	arr.push(subj);
	subjects["listSubjects"] += ", " + subj;
	return subjects["listSubjects"];
}


console.log("Программа 2.1", camelize_2_1("background-color"));
console.log("Программа 2.2", program_2_2([9, 8, 7, 6, 5, 4, 3, 2, 1]));
console.log("Программа 2.3", translate_2_3("user input from keyboard", "eng", "rus"));
console.log("Программа 2.4", weekday_2_4());
console.log("Программа 2.5", personal_2_5());
console.log("Программа 2.6", abbreviatedWeekday_2_6());
console.log("Программа 2.7", addSubject_2_7("английский язык"))
