## 01 Task:

1. Using the at, split and toUpperCase string methods, create a function that converts a string of the form "background-color" into "backgroundColor", i.e. removes the "-" sign and converts the following letter into an uppercase letter.
2. Given an array [9,8,7,6,5,4,3,2,1]. Create two copies of this array using different methods and reverse the order of the array using the same two methods. Output all three arrays.
3. Using the associative array, create a function to translate the given string into the required language. The words and phrases to be translated are limited to a given set (4 words for the example).
4. Create an object with days of the week. The keys in it should be the numbers of days from the beginning of the week (Monday is the first, etc.). Output the current day of the week.
5. Create a "personnel" object containing positions and names. Copy it to the "staff 2" object and change the names in it. Output both objects by first converting them to a string and using line feed characters.
6. There is a variable that lists the abbreviations of the days of the week in commas. Having converted this variable into an array and using enumeration, add a new object property to task #3. Create a method to display the abbreviated current day of the week.
7. Create an object "subjects" with a property that lists comma separated subjects taught at the university/school (list 2-6). Write a function to it that will add an item if it is not already there. Use array conversion and array properties split, join, push for this. 

Addition*: Remove an item if it is already there using the splice property.

## Demonstration of work [script.js](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/client-side_programming/01_working_with_data_structures/script.js):

![1](1.png "1")