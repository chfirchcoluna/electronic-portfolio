## 05 Task:

Create a simple game of MineSweeper.

Rules:
A flat playing field is divided into adjacent cells, some of which are "mined"; the number of "mined" cells is known. The goal of the game is to open all cells that do not contain mines.

The player opens cells, trying not to open a cell with a mine. If he opens a cell with a mine, he loses. Mines are placed after the first move. If there is no mine under an open cell, a number appears in the cell showing how many cells adjacent to the newly opened cell are "mined"; using these numbers, the player tries to calculate the location of mines. If there are no mines under neighbouring cells either, then some "unmined" area opens up to the cells with numbers in them. The player can mark the "unmined" cells to avoid accidentally opening them. By opening all "unmined" cells, the player wins.

Mechanics:
The field is 12x12 cells. Left click opens a cell. Right click marks it as potentially mined.

Provide for the possibility of playing without a mouse using only the keyboard: Arrows move the cursor to the desired cell (the cell should be illuminated in this case). Pressing the Spacebar or Enter button opens the cell. Ctrl+Space or Ctrl-Enter button marks as potentially mined.

An example of the game board [layout](https://xd.adobe.com/spec/a154109e-7def-41a2-72b4-078f8ceb8312-d765/)


## Demonstration of work [minesweeper](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/client-side_programming/05_minesweeper/minesweeper/MineSweeper.html):

![1](1.png "1")
![2](2.png "2")
![3](3.png "3")
![4](4.png "4")
![5](5.png "5")
![6](6.png "6")
![7](7.png "7")