﻿function add() {
    let phoneLabel = document.getElementsByClassName('phone--label')['phone--label'];

    let phoneNewLabel = document.createElement('label');
    phoneNewLabel.className = 'phone--label';
    phoneNewLabel.style.padding = '1.5% 0 0 0';
    phoneNewLabel.setAttribute('name', 'phone--label');

    let phoneInput = document.createElement('input');
    phoneInput.type = 'tel';
    phoneInput.setAttribute('name', 'phone-sub');
    phoneInput.style.display = 'inline-block';
    phoneInput.setAttribute('maxlength', '28');
    phoneInput.style.margin = '1.5% 0 0 0';
    phoneInput.style.width = '220px';
    phoneInput.setAttribute('onkeydown', 'if(event.keyCode==13){return false;}');
    phoneNewLabel.append(phoneInput);

    let phoneSub = document.createElement('input');
    phoneSub.type = 'button';
    phoneSub.setAttribute('name', 'sub-phone');
    phoneSub.setAttribute('id', 'sub-phone');
    phoneSub.style.display = 'inline-block';
    phoneSub.setAttribute('onkeydown', 'if(event.keyCode==13){return false;}');

    let phoneSpan = document.createElement('span');
    phoneSpan.id = 'phoneSpanInverse';

    let labelButton = document.createElement('label');
    labelButton.append(phoneSub, phoneSpan);

    phoneNewLabel.append(labelButton);
    phoneLabel.replaceWith(phoneNewLabel);

    let sub_phone = document.getElementById('sub-phone');

    sub_phone.onclick = sub;
}


function sub() {
    let phoneLabel = document.getElementsByClassName('phone--label')['phone--label'];

    let phoneNewLabel = document.createElement('label');
    phoneNewLabel.className = 'phone--label';
    phoneNewLabel.setAttribute('name', 'phone--label');

    let phoneAdd = document.createElement('input');
    phoneAdd.type = 'button';
    phoneAdd.setAttribute('name', 'add-phone');
    phoneAdd.setAttribute('id', 'add-phone');
    phoneAdd.setAttribute('onkeydown', 'return false;');
    phoneAdd.setAttribute('onkeydown', 'if(event.keyCode==13){return false;}');

    let phoneSpan = document.createElement('span');
    phoneSpan.textContent = 'Добавить еще один телефон';
    phoneSpan.style.color = '#707070';
    phoneSpan.style.fontSize = '12px';
    phoneSpan.style.fontFamily = 'Roboto';
    phoneSpan.style.padding = '0 0 0 8px';
    phoneSpan.className = 'label--span-small';
    phoneSpan.id = 'span';

    let phoneSub = document.createElement('input');
    phoneSub.setAttribute('name', 'phone-sub');
    phoneSub.value = '';
    phoneSub.setAttribute('onkeydown', 'if(event.keyCode==13){return false;}');
    phoneSub.style.display = 'none';

    phoneNewLabel.append(phoneAdd, phoneSpan, phoneSub);
    phoneLabel.replaceWith(phoneNewLabel);

    let add_phone = document.getElementById('add-phone');

    add_phone.onclick = add;
}


let add_phone = document.getElementById('add-phone');

add_phone.onclick = add;


window.handleForm = function(form) {

    let data = {
        orgname: form.orgname.value,
        fullname: form.fullname.value,
        name__text_align: form['name--text-align'].value,
        name__text_size: form['name--text-size'].value,
        name__color: form['name--color'].value,
        workpost: form.workpost.value,
        workpost__text_align: form['workpost--text-align'].value,
        workpost__text_size: form['workpost--text-size'].value,
        workpost__color: form['workpost--color'].value,
        phone: form.phone.value,
        phone_sub : form['phone-sub'].value,
        email: form.email.value,
        email__opacity: form['email--opacity'].checked,
        address: form.address.value,
        address__opacity: form['address--opacity'].checked
    };

    let resultDiv = document.createElement('div');

    resultDiv.className = 'card';
    resultDiv.setAttribute('name', 'card');

    let card__orgname = document.createElement('div');
    card__orgname.append(data.orgname);
    card__orgname.className = 'card--orgname';
    card__orgname.style.textAlign = "center";
    card__orgname.style.fontSize = 10 + "px";
    card__orgname.style.color = '#202020';
    card__orgname.style.padding = "22px 0 0 0";
    resultDiv.append(card__orgname);

    let card__fullname = document.createElement('div');
    card__fullname.append(data.fullname);
    card__fullname.style.fontSize = data.name__text_size + "px";
    if (data.name__text_align === "left") {
        card__fullname.style.textAlign = data.name__text_align;
        card__fullname.style.padding = "32px 0 0 22px";
    }
    else if (data.name__text_align === "right") {
        card__fullname.style.textAlign = data.name__text_align;
        card__fullname.style.padding = "32px 22px 0 0";
    }
    else {
        card__fullname.style.textAlign = data.name__text_align;
        card__fullname.style.padding = "32px 0 0 0";
    }
    card__fullname.style.color = data.name__color;
    card__fullname.style.fontWeight = 'bold';
    card__fullname.className = 'card--fullname';
    resultDiv.append(card__fullname);

    let card__workpost = document.createElement('div');
    card__workpost.append(data.workpost);
    card__workpost.style.fontSize = data.workpost__text_size + "px";
    card__workpost.style.color = data.workpost__color;
    if (data.workpost__text_align === "left") {
        card__workpost.style.textAlign = data.workpost__text_align;
        card__workpost.style.padding = "8px 0 0 22px";
    }
    else if (data.workpost__text_align === "right") {
        card__workpost.style.textAlign = data.workpost__text_align;
        card__workpost.style.padding = "8px 22px 0 0";
    }
    else {
        card__workpost.style.textAlign = data.workpost__text_align;
        card__workpost.style.padding = "8px 0 0 0";
    }
    card__workpost.className = 'card--workpost';
    resultDiv.append(card__workpost);

    let card__phone = document.createElement('div');
    card__phone.append(data.phone);
    card__phone.className = 'card--phone';
    card__phone.style.textAlign = "left";
    card__phone.style.fontSize = 12 + "px";
    card__phone.style.color = '#202020';
    card__phone.style.padding = "20px 0 0 50%";
    resultDiv.append(card__phone);

    if (data.phone_sub != "") {
        let card__phone_sub = document.createElement('div');
        card__phone_sub.append(data.phone_sub);
        card__phone_sub.className = 'card--phone-sub';
        card__phone_sub.style.textAlign = "left";
        card__phone_sub.style.fontSize = 12 + "px";
        card__phone_sub.style.color = '#202020';
        card__phone_sub.style.padding = "10px 0 0 50%";
        resultDiv.append(card__phone_sub);
    }

    let card__email = document.createElement('div');
    card__email.append(data.email);
    card__email.className = 'card--email';
    card__email.style.textAlign = "left";
    card__email.style.fontSize = 12 + "px";
    card__email.style.color = '#202020';
    card__email.style.padding = "10px 0 0 50%";

    if (!data.email__opacity) {
        card__email.style.visibility = 'hidden';
    };
    resultDiv.append(card__email);

    let card__address = document.createElement('div');
    card__address.append(data.address);
    card__address.className = 'card--address';
    card__address.style.textAlign = "left";
    card__address.style.fontSize = 12 + "px";
    card__address.style.color = '#202020';
    card__address.style.padding = "10px 0 0 50%";

    if (!data.address__opacity) {
        card__address.style.visibility = 'hidden';
    };
    resultDiv.append(card__address);

    let card_result = document.getElementsByClassName('card-result')['card-result'];
    card_result.style.display = 'inline-block';

    let cart_base = document.getElementsByClassName('card-base')['card-base'];
    cart_base.style.margin = '0 20px 0 0';

    let result = document.getElementsByClassName('card');

    result.card.replaceWith(resultDiv);

    return false;
}
