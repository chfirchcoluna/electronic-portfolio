## 03 Task:

Create a simple business card builder according to the layout:

[Version 1](https://xd.adobe.com/spec/a71dd7c9-121d-4797-6b50-88d81fcc239e-d12c/)
Request the required data and display the business card layout with this data.

[Version 2](https://xd.adobe.com/spec/a71dd7c9-121d-4797-6b50-88d81fcc239e-d12c/screen/54275751-2ace-44a0-8cb3-8f9e8248ae98/-2)
Add the ability to customise the appearance of the business card.

[Version 3](https://xd.adobe.com/spec/a71dd7c9-121d-4797-6b50-88d81fcc239e-d12c/screen/6d77b6bd-8b97-4b60-ba31-97ab929e03bb/-3)
Add the ability to change the colour of some elements.

## Demonstration of work [business card designer](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/client-side_programming/03_business_card_designer/business_card_designer/card.html):

![1](1.png "1")
![2](2.png "2")
![3](3.png "3")