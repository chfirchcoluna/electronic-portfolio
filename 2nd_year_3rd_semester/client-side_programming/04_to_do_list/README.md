## 04 Task:

Create a simple task list according to the [layout](https://xd.adobe.com/spec/450c0967-37c0-4ff6-6b4d-b6a4973dfdfa-333d/)

The task card should be displayed either by clicking the "Create" button or by clicking the "Edit" button in the list.
The task card should be placed in a fixed position (even when scrolling through a large list of tasks, it remains in its place).
When performing the task, you should show your ability to work with DOM-structure.

## Demonstration of work [to do list](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/client-side_programming/04_to_do_list/to_do_list/index.html):

![1](1.png "1")
![2](2.png "2")
![3](3.png "3")
![4](4.png "4")
![5](5.png "5")
![6](6.png "6")