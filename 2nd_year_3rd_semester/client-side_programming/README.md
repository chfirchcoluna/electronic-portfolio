<h1 align="center">Client-side Programming</h1>

### About subject:
<p align="left">
    Study of the main regularities and modern tendencies
    development of computer languages of web-programming in relation to the content of Internet network resources, obtaining skills and competences within the framework of general cultural and professional competences.
    and professional competences.
    The objectives are: 

1. To study the basic concepts, methods, goals and objectives of web-programming;
2. To be able to define and/or understand the tasks set for web-programmer and, depending on this, to compose an appropriate programme algorithm, to write and debug programme code (scripts).
debug the code of the programme (script).
</p>

<h2></h2>

<h2><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester/client-side_programming/00_working_with_numbers">TASK working with numbers 00</a></h2>

<h2><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester/client-side_programming/01_working_with_data_structures">TASK working with data structures 01</a></h2>

<h2><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester/client-side_programming/02_DOM_operation">TASK DOM operation 02</a></h2>

<h2><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester/client-side_programming/03_business_card_designer">TASK business card designer 03</a></h2>

<h2><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester/client-side_programming/04_to_do_list">TASK to do list 04</a></h2>

<h2><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester/client-side_programming/05_minesweeper">TASK minesweeper 05</a></h2>

<h2><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester/client-side_programming/06_puzzle">TASK puzzle 06</a></h2>
