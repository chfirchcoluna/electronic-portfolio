﻿document.addEventListener('DOMContentLoaded', e => {
    const boxPuzzles = document.querySelector('.boxPuzzles');
    const numberHorizontalPieces = 9;
    const numberVerticalPieces = 6;

    for (let y = 0; y < numberVerticalPieces; y++) {
        for (let x = 0; x < numberHorizontalPieces; x++) {
            let dropPlaceItem = document.createElement('div');
            dropPlaceItem.className = 'drop';
            boxPuzzles.append(dropPlaceItem);
            let piecePuzzle = document.createElement('div');
            piecePuzzle.className = 'puzzle picture';
            piecePuzzle.style.position = 'absolute';
            piecePuzzle.draggable = true;
            let randX = Math.round(Math.random() * (numberHorizontalPieces - 1));
            let randY = Math.round(Math.random() * (numberVerticalPieces - 1));
            piecePuzzle.style['background-position'] = 'top ' + y * -50 + 'px left ' + x * -50 + 'px';
            piecePuzzle.style.left = randX * 34 + 50 + 'px';
            piecePuzzle.style.top = randY * 34 + 350 + 'px';
            document.body.appendChild(piecePuzzle);
        }
    }

    const listPuzzles = document.querySelectorAll('.puzzle');

    let dragged;
    for (let i = 0; i < listPuzzles.length; i++) {
        console.log(listPuzzles[i]);
        listPuzzles[i].addEventListener('dragstart', event => {
            console.log(event.target.className + " 1");
            dragged = event.target;
            let whatWasDragged = event.target.className
            if (whatWasDragged === 'puzzle' || whatWasDragged === 'puzzle picture') {
                event.target.style.opacity = '0.5';
            }
            document.addEventListener('drop', event => {
                event.preventDefault()
                if (whatWasDragged === 'puzzle' || whatWasDragged === 'puzzle picture') {
                    if (event.target.className === 'drop') {
                        dragged.style.position = '';
                        event.target.style.background = '';
                        dragged.parentNode.removeChild(dragged);
                        event.target.appendChild(dragged);
                    }
                    else {
                        dragged.style.left = event.pageX - dragged.offsetWidth / 2 + 'px';
                        dragged.style.top = event.pageY - dragged.offsetHeight / 2 + 'px';
                        dragged.style.position = 'absolute';
                        // event.target.style.background = ''
                        // dragged.parentNode.removeChild(dragged);
                        // event.target.appendChild(dragged);
                    }
                }
            }, false)
        }, false)
    }
    document.addEventListener('dragend', event => event.target.style.opacity = '', false)
    document.addEventListener('dragover', event => event.preventDefault(), false);
    document.addEventListener('dragenter', event => {
        if (event.target.className === 'drop') event.target.style.background = '#2b2d48';
    }, false)
    document.addEventListener('dragleave', event => {
        if (event.target.className === 'drop') event.target.style.background = '';
    }, false)
})