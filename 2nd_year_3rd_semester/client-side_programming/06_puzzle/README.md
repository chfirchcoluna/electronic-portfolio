## 06 Task:

Create a puzzle game - break a picture into pieces and let the player reassemble it into its original form.

[Prototype](https://xd.adobe.com/view/683d0778-0c1e-4ef1-5ad3-8040a3b5ef27-1e0c/)

1. Original picture size: 450 x 300
2. Tile size: 50 x 50
3. Game board size: 9 х 6


## Demonstration of work [puzzle](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/client-side_programming/06_puzzle/puzzle/puzzle.html)

![1](1.png "1")
![2](2.png "2")
![3](3.png "3")