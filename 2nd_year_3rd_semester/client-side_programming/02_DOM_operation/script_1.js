﻿function table_multiply(start, stop)
{
	let table = document.createElement('table');
	let tbody = document.createElement('tbody');


	let row = 0
	list_row = []
	for (let i = start; i <= stop; i++)
	{
		list_row.push(document.createElement('tr'));
		for (let j = start; j <= stop; j++)
		{
			let cell = document.createElement('td');
			cell.textContent = i * j;
			list_row[row].appendChild(cell);
		}
		row++;
    }


	table.appendChild(tbody);
	for (let i = 0; i < stop - start + 1; i++)
	{
		tbody.appendChild(list_row[i]);
	}
	document.body.appendChild(table);

	
	table.style['border-collapse'] = 'collapse';
	for (let i = 0; i < table.rows.length; i++)
	{
		for (let j = 0; j < (table.rows)[i].cells.length; j++)
		{
			table.rows[i].cells[j].style['border'] = '1px solid #000'
			table.rows[i].cells[j].style['width'] = '19px';
			table.rows[i].cells[j].style['height'] = '19px';
			table.rows[i].cells[j].style['text-align'] = 'center';
			if (i == 0 || j == 0) {
				table.rows[i].cells[j].style['background-color'] = '#d4d0d6';
			}
		}
	}
} 


table_multiply(1, 10)
