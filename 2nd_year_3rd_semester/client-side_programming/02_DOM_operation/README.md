## 02 Task:

1. Create a web page that displays the calendar for the current month in the form of a table.
2. Create a web page that displays a multiplication table for numbers from 1 to 10. 

## Demonstration of work [script_0.js](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/client-side_programming/02_DOM_operation/script_0.js):

![1](1.png "1")

## Demonstration of work [script_1.js](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/client-side_programming/02_DOM_operation/script_1.js):

![2](2.png "2")