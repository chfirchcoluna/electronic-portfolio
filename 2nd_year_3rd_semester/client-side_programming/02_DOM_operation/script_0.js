﻿function calendar(year, month)
{
	let table = document.createElement('table');
	let thead = document.createElement('thead');
	let tbody = document.createElement('tbody');
	let firstDay = new Date(year, month - 1, 1);
	let lastDay = new Date(year, month, 0);
	let today = new Date();
	let list_weeks = [];
	let list_days = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];


	for (let i = 0; i < 7; i++)
	{
		list_weeks.push(document.createElement('tr'));
	}


	for (let i = 0; i < 7; i++)
	{
		let cell = document.createElement('th');
		cell.textContent = list_days[i];
		cell.style['border'] = '1 px solid #000'
        list_weeks[0].appendChild(cell);
    }


	let firstDayGetDay = firstDay.getDay();
	if (firstDayGetDay == 0) {
		firstDayGetDay = 7;
	}


	for (let i = 1; i < firstDayGetDay; i++)
	{
		let cell = document.createElement('td');
		list_weeks[1].appendChild(cell);
    }


	let week = 1
	for (let i = 1, day = new Date(year, month - 1, i); i <= lastDay.getDate(); i++)
	{
		day = new Date(year, month - 1, i)
		let cell = document.createElement('td');
		cell.textContent = i;
		list_weeks[week].appendChild(cell);
		if (day.getDay() % 7 == 0) {
			week += 1;
		}
    }


    for (let i = lastDay.getDay(); i < 7; i++)
	{
		let cell = document.createElement('td');
		list_weeks[week].appendChild(cell);
    }


	if (week != 6) {
		week += 1;
		for (let i = 0; i < 7; i++)
		{
			let cell = document.createElement('td');
			list_weeks[week].appendChild(cell);
    	}
	}

	// console.log(window.getComputedStyle(table));

	table.appendChild(thead);
	table.appendChild(tbody);
	thead.appendChild(list_weeks[0]);
	for (let i = 1; i < 7; i++)
	{
		tbody.appendChild(list_weeks[i]);
	}
	document.body.appendChild(table);

	
	table.style['border-collapse'] = 'collapse';
	let count = 0
	for (let i = 0; i < table.rows.length; i++)
	{
		for (let j = 0; j < (table.rows)[i].cells.length; j++)
		{
			table.rows[i].cells[j].style['border'] = '1px solid #000'
			table.rows[i].cells[j].style['width'] = '19px';
			table.rows[i].cells[j].style['height'] = '19px';
			table.rows[i].cells[j].style['text-align'] = 'center';

			if (i == 0) {
				table.rows[i].cells[j].style['background-color'] = '#d4d0d6';
				table.rows[i].cells[j].style['font-weight'] = 'bold';
			}
			else {
				count++;
			}

			if (today.getMonth() == month - 1 && today.getFullYear() == year && count == today.getDate() + firstDayGetDay - 1) {
				table.rows[i].cells[j].style['background-color'] = '#f00';
				table.rows[i].cells[j].style['font-weight'] = 'bold';
			}
		}
	}
} 


calendar((new Date).getFullYear(), (new Date).getMonth() + 1)
