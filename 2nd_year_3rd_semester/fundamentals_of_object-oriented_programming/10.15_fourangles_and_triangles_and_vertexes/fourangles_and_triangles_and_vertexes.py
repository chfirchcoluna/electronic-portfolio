import math


class Vertex(list):
    """Точка (вершина)"""

    def __init__(self, coordinates):
        """Инициализация"""
        list.__init__(self, coordinates)
        self.__x = coordinates[0]
        self.__y = coordinates[1]

    @property
    def get_x(self):
        """Гет для x"""
        return self.__x

    @property
    def get_y(self):
        """Гет для y"""
        return self.__y

    @property
    def unpack_vertex(self):
        """Распаковка вершины в лист"""
        return [self.__x, self.__y]

    def __str__(self):
        """Возврат удобочитаемой вершины"""
        return str(self.unpack_vertex)


class Figure:
    """Абстрактный класс фигуры"""

    @staticmethod
    def calculate_side(fv, sv):
        """Вычисление стороны"""
        return math.sqrt((sv.get_x - fv.get_x) ** 2 + (sv.get_y - fv.get_y) ** 2)

    @property
    def area(self):
        """Возврат площади фигуры"""
        return self._area

    def __str__(self):
        """Возврат удобочитаемой фигуры"""
        return str(self.unpack_figure())

    def __eq__(self, other):
        """Сравнение равенства элементов фигур"""
        if isinstance(other, list):
            return False not in [x == y for x, y in zip(self.unpack_figure(), other)]
        else:
            return False not in [x == y for x, y in zip(self.unpack_figure(), other.unpack_figure())]

    def __ne__(self, other):
        """Сравнение неравенства элементов фигур"""
        if isinstance(other, list):
            return True in [x != y for x, y in zip(self.unpack_figure(), other)]
        else:
            return True in [x != y for x, y in zip(self.unpack_figure(), other.unpack_figure())]

    def __lt__(self, other):
        """Проверка, что левое меньше правого"""
        if isinstance(other, float) or isinstance(other, int):
            return self.area < other
        else:
            return self.area < other.area

    def __gt__(self, other):
        """Проверка, что левое больше правого"""
        if isinstance(other, float) or isinstance(other, int):
            return self.area > other
        else:
            return self.area > other.area

    def __le__(self, other):
        """Проверка, что левое меньше или равно правого"""
        if isinstance(other, float) or isinstance(other, int):
            return self.area <= other
        else:
            return self.area <= other.area

    def __ge__(self, other):
        """Проверка, что левое больше или равно правого"""
        if isinstance(other, float) or isinstance(other, int):
            return self.area >= other
        else:
            return self.area >= other.area


class Triangle(Figure):
    """Треугольник"""

    def __init__(self, vertex_a, vertex_b, vertex_c):
        """Инициализация"""
        super(Figure, self).__init__()
        self.__vx_a = vertex_a
        self.__vx_b = vertex_b
        self.__vx_c = vertex_c
        self.__ab = self.calculate_side(self.__vx_a, self.__vx_b)
        self.__ac = self.calculate_side(self.__vx_a, self.__vx_c)
        self.__bc = self.calculate_side(self.__vx_b, self.__vx_c)
        self.__perimeter = self.calculate_perimeter()
        self._area = self.calculate_area()

    def calculate_perimeter(self):
        """Вычисление периметра"""
        return self.__ab + self.__ac + self.__bc

    def calculate_area(self):
        """Вычисление площади"""
        semiper = self.__perimeter / 2
        radicand = semiper * (semiper - self.__ab) * (semiper - self.__ac) * (semiper - self.__bc)
        if radicand < 0:
            radicand = 0.0
        return math.sqrt(radicand)

    def unpack_figure(self):
        """Распаковка треугольника в лист"""
        return [self.__vx_a.unpack_vertex,
                self.__vx_b.unpack_vertex,
                self.__vx_c.unpack_vertex]

    @property
    def get_perimeter(self):
        """Гет для периметра"""
        return self.__perimeter

    @property
    def get_vertex_a(self):
        """Гет для вершины A"""
        return self.__vx_a

    @property
    def get_vertex_b(self):
        """Гет для вершины B"""
        return self.__vx_b

    @property
    def get_vertex_c(self):
        """Гет для вершины C"""
        return self.__vx_c


class Fourangle(Figure):
    """Четырёхугольник"""

    def __init__(self, vertex_a, vertex_b, vertex_c, vertex_d):
        """Инициализация"""
        super(Figure, self).__init__()
        self.__vx_a = vertex_a
        self.__vx_b = vertex_b
        self.__vx_c = vertex_c
        self.__vx_d = vertex_d
        self.__ab = self.calculate_side(self.__vx_a, self.__vx_b)
        self.__bc = self.calculate_side(self.__vx_b, self.__vx_c)
        self.__cd = self.calculate_side(self.__vx_c, self.__vx_d)
        self.__ac = self.calculate_side(self.__vx_a, self.__vx_c)
        self.__da = self.calculate_side(self.__vx_d, self.__vx_a)
        self._area = self.calculate_area()

    def unpack_figure(self):
        """Распаковка четырёхугольника в лист"""
        return [self.__vx_a.unpack_vertex,
                self.__vx_b.unpack_vertex,
                self.__vx_c.unpack_vertex,
                self.__vx_d.unpack_vertex]

    @staticmethod
    def calculate_cosa(a, b, c):
        """Расчёт косинуса угла"""
        cosa = (b * b + c * c - a * a) / (2 * b * c)
        return cosa

    @staticmethod
    def calculate_sina(cosine):
        """Расчёт синуса угла"""
        return math.sqrt(1 - cosine * cosine)

    def calculate_diagonals(self):
        """Расчёт диагоналей четырёхугольника"""
        d1 = self.calculate_side(self.__vx_a, self.__vx_c)
        d2 = self.calculate_side(self.__vx_b, self.__vx_d)
        return d1, d2

    def calculate_area_old(self):
        """Расчёт площади четырёхугольника"""
        d1, d2 = self.calculate_diagonals()
        cosine = self.calculate_cosa(self.__ab, d1/2, d2/2)
        sine = self.calculate_sina(cosine)
        return d1 * d2 * sine * 1/2

    def calculate_area(self):
        """Расчёт площади четырёхугольника"""
        # f = Triangle(Vertex(self.__vx_a), Vertex(self.__vx_b), Vertex(self.__vx_c))
        # s = Triangle(Vertex(self.__vx_c), Vertex(self.__vx_d), Vertex(self.__vx_a))
        f = (self.__vx_a.get_x - self.__vx_b.get_x) * (self.__vx_a.get_y + self.__vx_b.get_y)
        s = (self.__vx_b.get_x - self.__vx_c.get_x) * (self.__vx_b.get_y + self.__vx_c.get_y)
        t = (self.__vx_c.get_x - self.__vx_d.get_x) * (self.__vx_c.get_y + self.__vx_d.get_y)
        fo = (self.__vx_d.get_x - self.__vx_a.get_x) * (self.__vx_d.get_y + self.__vx_a.get_y)
        # return f.area + s.area
        return (1/2) * abs(f + s + t + fo)


class Task:
    """Вызов методов по заданию"""

    @staticmethod
    def show_error(err, msg):
        """Вывод ошибки"""
        print(msg)
        print(err)

    @staticmethod
    def show_info(std_msg='\n', *msg, end=''):
        """Вывод сообщения"""
        print(std_msg, *msg, end=end)

    @staticmethod
    def try_input_filename():
        """Попытка ввода файла с клавиатуры"""
        try:
            filename = input("Enter the file name with the extension:\n")
            with open(filename, "r") as file:
                line = file.readline()
                return line
        except OSError as err:
            return Task.show_error(err, "\nError receiving data")

    @staticmethod
    def try_process_string(line: str):
        """Перевод строки в лист"""
        try:
            replace_list = [']', "None", '[', ',']
            for replace_element in replace_list:
                line = line.replace(replace_element, ' ')
            return list(map(float, line.split()))
        except ValueError as err:
            return Task.show_error(err, "\nError in data processing. Wrong Value")
        except TypeError as err:
            return Task.show_error(err, "\nError in data processing. Wrong Type")

    @staticmethod
    def check_length_list_vertexes(plist, number_vertex=3):
        """Проверка на достаточность вершин для создания хотя бы одной фигуры"""
        try:
            if len(plist) < number_vertex:
                raise ValueError("insufficient number of vertexes")  # недостаточное количество вершин
            else:
                return True
        except ValueError as err:
            return Task.show_error(err, "\nError in initialization. Wrong Value")

    @staticmethod
    def pop_repeats(lst: list):
        """Удаление повторяющихся элементов в списке"""
        for first_index in range(len(lst)):
            remember_this_element = lst[first_index]
            for second_index in range(1, len(lst)):
                if lst[second_index] == remember_this_element and first_index != second_index:
                    lst.pop(second_index)
                    return Task.pop_repeats(lst)
        return lst

    @staticmethod
    def create_triangles(vlist, combinations):
        """Создание треугольников"""
        index_figure = 0
        list_triangles = []
        for fi in range(len(vlist)):
            Task.show_info("\rStatus: complete [", round((index_figure / combinations) * 100), end=" % ] ")
            for si in range(fi + 1, len(vlist)):
                for ti in range(si + 1, len(vlist)):
                    index_figure += 1
                    is_triangle = Task.check_is_triangle(
                        [vlist[fi].unpack_vertex, vlist[si].unpack_vertex, vlist[ti].unpack_vertex])
                    if is_triangle:
                        list_triangles.append(Triangle(vlist[fi], vlist[si], vlist[ti]))
        Task.show_info("\rStatus: completed successfully!\n")
        return list_triangles, True

    @staticmethod
    def create_fourangles(vlist, combinations):
        """Создание четырёхугольников"""
        index_figure = 0
        list_fourangles = []
        for fi in range(len(vlist)):
            Task.show_info("\rStatus: complete [", round((index_figure / combinations) * 100), end=" % ] ")
            for si in range(fi + 1, len(vlist)):
                for ti in range(si + 1, len(vlist)):
                    for foi in range(ti + 1, len(vlist)):
                        index_figure += 1
                        is_triangle_first = Task.check_is_triangle(
                            [vlist[fi].unpack_vertex, vlist[si].unpack_vertex, vlist[ti].unpack_vertex])
                        is_triangle_second = Task.check_is_triangle(
                            [vlist[si].unpack_vertex, vlist[ti].unpack_vertex, vlist[foi].unpack_vertex])
                        is_triangle_third = Task.check_is_triangle(
                            [vlist[ti].unpack_vertex, vlist[foi].unpack_vertex, vlist[fi].unpack_vertex])
                        is_triangle_fourth = Task.check_is_triangle(
                            [vlist[foi].unpack_vertex, vlist[fi].unpack_vertex, vlist[si].unpack_vertex])
                        if is_triangle_first and is_triangle_second and is_triangle_third and is_triangle_fourth:
                            is_cross_line = Task.check_cross_lines(vlist[fi].get_x, vlist[fi].get_y,
                                                                   vlist[foi].get_x, vlist[foi].get_y,
                                                                   vlist[si].get_x, vlist[si].get_y,
                                                                   vlist[ti].get_x, vlist[ti].get_y)
                            is_cross_line_2 = Task.check_cross_lines(vlist[fi].get_x, vlist[fi].get_y,
                                                                     vlist[si].get_x, vlist[si].get_y,
                                                                     vlist[ti].get_x, vlist[ti].get_y,
                                                                     vlist[foi].get_x, vlist[foi].get_y)

                            if not is_cross_line and not is_cross_line_2:
                                list_fourangles.append(Fourangle(vlist[fi], vlist[si], vlist[ti], vlist[foi]))
        Task.show_info("\rStatus: completed successfully!\n")
        return list_fourangles, True

    @staticmethod
    def check_cross_lines(x1, y1, x2, y2, x3, y3, x4, y4):
        """Проверка пересечения отрезков"""
        v1 = Task.multiply_vector(x4 - x3, y4 - y3, x1 - x3, y1 - y3)
        v2 = Task.multiply_vector(x4 - x3, y4 - y3, x2 - x3, y2 - y3)
        v3 = Task.multiply_vector(x2 - x1, y2 - y1, x3 - x1, y3 - y1)
        v4 = Task.multiply_vector(x2 - x1, y2 - y1, x4 - x1, y4 - y1)
        return 0 - (v1 * v2) > 0 and 0 - (v3 * v4) > 0

    @staticmethod
    def multiply_vector(ax, ay, bx, by):
        """Умножение векторов"""
        return ax*by - bx*ay

    @staticmethod
    def try_create_figures(vlist, figure,  number_vertex=3):
        """Попытка создать фигуру по принятому обработанному листу вершин"""
        is_correct_length = Task.check_length_list_vertexes(vlist, number_vertex)
        if not is_correct_length:
            return
        combinations = Task.factorial(len(vlist)) / (
                        Task.factorial(number_vertex) * (Task.factorial(len(vlist) - number_vertex)))
        combinations = int(combinations)
        try:
            list_figures, is_create_successful = figure(vlist, combinations)
            return is_create_successful, list_figures
        except ValueError as err:
            return Task.show_error(err, "\nError in process initialization. Wrong Value")
        except TypeError as err:
            return Task.show_error(err, "\nError in initialization. Wrong Type")

    @staticmethod
    def factorial(number):
        """Вычисление факториала числа"""
        factorial = 1
        for i in range(1, number + 1):
            factorial *= i
        return factorial

    @staticmethod
    def return_figure_with_area(lst, option=max):
        """Вычислиние площади среди экземпляров класса"""
        try:
            areas = [figure.area for figure in lst]
            max_area = option(areas)
            figure_with_max_area = lst[areas.index(max_area)]
            return figure_with_max_area
        except AttributeError as err:
            Task.show_error(err, "\nError receiving data")
        except ValueError as err:
            Task.show_error(err, "\nError receiving data")

    @staticmethod
    def check_is_triangle(lst: list):
        """Проверка не лежат ли точки на одной прямой"""
        cp_lst = [_ for _ in lst]
        cp_lst.sort()
        left_x = cp_lst[2][0] - cp_lst[1][0]
        right_x = (cp_lst[1][0] - cp_lst[0][0])
        left_y = abs(cp_lst[2][1] - cp_lst[1][1])
        right_y = abs(cp_lst[1][1] - cp_lst[0][1])
        return left_x * right_y != left_y * right_x


def main():
    """Выполнение программы"""

    # Ввод файла с клавиатуры
    line_list = Task.try_input_filename()
    if line_list is None:
        return

    # Обработка строки и перевод в лист
    line_list = Task.try_process_string(line_list)
    if line_list is None:
        return

    # Создание точек
    points_list = [[line_list[y - 1], line_list[y]] for y in range(1, len(line_list), 2)]

    # Удаление повторяющихся точек
    points_list = Task.pop_repeats(points_list)

    # Создание вершин по принятому листу точек
    vertex_list = [Vertex(point) for point in points_list]

    # Создание треугольников по созданным вершинам
    print()
    print("Triangles:")
    is_completed_successfully_creating_triangles, list_triangles = Task.try_create_figures(vertex_list,
                                                                                           Task.create_triangles)
    if not is_completed_successfully_creating_triangles:
        return

    # Хранение ссылок на треугольники с нужными площадями
    task_max_triangle = Task.return_figure_with_area(list_triangles, max)
    task_min_triangle = Task.return_figure_with_area(list_triangles, min)

    # Если ссылки найдены
    if task_min_triangle is not None and task_max_triangle is not None:

        # Вывод треугольника с максимальной площадью и его площадь
        print(f"\nTriangle with max ({round(task_max_triangle.area, 3)}) area:")
        print(task_max_triangle)

        # Вывод треугольника с минимальной площадью и его площадь
        print(f"\nTriangle with min ({round(task_min_triangle.area, 3)}) area:")
        print(task_min_triangle)

    # Создание тестовых треугольников
    a = Triangle(Vertex([1, 1]), Vertex([1, 2]), Vertex([2, 2]))
    b = Triangle(Vertex([1, 1]), Vertex([1, 2]), Vertex([2, 2]))
    c = Triangle(Vertex([1, 1]), Vertex([1, 200]), Vertex([2, 2]))
    d = [[1, 1], [1, 2], [2, 2]]

    # Вывод площадей тестовых треугольников
    print()
    print("Test triangles 'a', 'b', 'd':", a, "  with areas:",  round(a.area, 3))
    print("Test triangle       'c'     :", c, "with area :", round(c.area, 3))
    print()

    # Визуальные тесты
    print('-' * 70)
    print("eq:", "\t|", "\ta == b :", a == b, "\t|", "\ta == c :", a == c, "\t|", "\ta == d :", a == d, "\t|")
    print("ne:", "\t|", "\ta != b :", a != b, "\t|", "\ta != c :", a != c, "\t|", "\ta != d :", a != d, "\t|")
    print("lt:", "\t|", "\ta <  b :", a < b,  "\t|", "\ta <  c :", a < c,  "\t|", "\ta <  1 :", a < 1,  "\t|")
    print("gt:", "\t|", "\ta >  b :", a > b,  "\t|", "\ta >  c :", a > c,  "\t|", "\ta >  1 :", a > 1,  "\t|")
    print("le:", "\t|", "\ta <= b :", a <= b, "\t|", "\ta <= c :", a <= c, "\t|", "\ta <= 1 :", a <= 1, "\t|")
    print("ge:", "\t|", "\ta >= b :", a >= b, "\t|", "\ta >= c :", a >= c, "\t|", "\ta >= 1 :", a >= 1, "\t|")
    print('-' * 70)

    # Создание четырёхугольников по созданным вершинам
    print()
    print("Fourangles:")
    is_completed_successfully_creating_fourangles, list_fourangles = Task.try_create_figures(vertex_list,
                                                                                             Task.create_fourangles, 4)
    if not is_completed_successfully_creating_fourangles:
        return

    # Хранение ссылок на четырёхугольники с нужными площадями
    task_max_fourangle = Task.return_figure_with_area(list_fourangles, max)
    task_min_fourangle = Task.return_figure_with_area(list_fourangles, min)

    # Если ссылки найдены
    if task_min_fourangle is not None and task_max_fourangle is not None:
        # Вывод четырёхугольника с максимальной площадью и его площадь
        print(f"\nFourangle with max ({round(task_max_fourangle.area, 3)}) area:")
        print(task_max_fourangle)

        # Вывод четырёхугольника с минимальной площадью и его площадь
        print(f"\nFourangle with min ({round(task_min_fourangle.area, 3)}) area:")
        print(task_min_fourangle)

    # Создание тестовых четырёхугольников
    e = Fourangle(Vertex([0, 0]), Vertex([10, 0]), Vertex([10, 10]), Vertex([0, 10]))
    f = Fourangle(Vertex([14, 21]), Vertex([10, 5]), Vertex([17, 0]), Vertex([20, 20]))
    g = Fourangle(Vertex([0, 0]), Vertex([10, 0]), Vertex([7, 20]), Vertex([5, 5]))

    # Вывод площадей тестовых треугольников
    print()
    print("Test fourangle 'e':", e, "with area :", round(e.area, 3))
    print("Test fourangle 'f':", f, "with area :", round(f.area, 3))
    print("Test fourangle 'g':", g, "with area :", round(g.area, 3))
    print()

    # Визуальные тесты
    print('-' * 30)
    print("eq:", "\t|", "\te == f :", e == f, "\t|")
    print("ne:", "\t|", "\te != f :", e != f, "\t|")
    print("lt:", "\t|", "\te <  f :", e < f, "\t|")
    print("gt:", "\t|", "\te >  f :", e > f, "\t|")
    print("le:", "\t|", "\te <= f :", e <= f, "\t|")
    print("ge:", "\t|", "\te >= f :", e >= f, "\t|")
    print('-' * 30)

    # Выход
    input("\n[Enter] for exit\n")


main()
