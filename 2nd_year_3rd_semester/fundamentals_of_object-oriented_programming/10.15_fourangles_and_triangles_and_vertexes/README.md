## Task:

An array of points is given. Find triangles with the smallest and largest area in this array of points. 

Use separate classes for points and triangles. Implement comparison methods for triangles using the operators >, <, =, >=, <=, ==, !=. Use these operators when comparing triangles in a programme.

## Demonstration of work [fourangles and triangles and vertex](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/fundamentals_of_object-oriented_programming/10.15_fourangles_and_triangles_and_vertexes/10.15_fourangles_and_triangles_and_vertexes.py)

![1](1.png "1")
![2](2.png "2")
![3](3.png "3")
