## Task:

Following the OOP paradigm, implement a program that simulates the motion of the planets of the solar system. Display images of the planets on the screen. When pointing at their image (in the legend or the planet itself) display its size, distance from the Sun and its position, for example, take as 0 the vertical from the Sun upwards and display the angle to the vector pointing to the planet.

For graphical representation, use any of the libraries studied last term: DrawSVG, PyGame, Turtle. The sizes of the planets relative to each other are to scale, the orbits and size of the Sun are not to scale. Motion of planets to speed up and specify the Earth day counter in the corner. Orbits can be made circular.

## Demonstration of work [Solar System model](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/fundamentals_of_object-oriented_programming/09.13_Solar_System_model/09.13_Solar_System_model.py)

![1](1.png "1")
![2](2.png "2")
![3](3.png "3")