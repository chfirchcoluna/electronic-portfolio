import pygame
import sys
from math import *

FPS = 60
WIN_WIDTH = 1000
WIN_HEIGHT = 1000
clock = pygame.time.Clock()
screen = pygame.display.set_mode((WIN_WIDTH, WIN_HEIGHT))
pygame.display.set_caption("Solar System model")


class ClassProperty:
    """Get для списка небесных тел"""
    def __init__(self, value):
        self.value = value

    def __get__(self, instance, owner):
        return self.value(owner)


class HeavenlyBody:
    """Небесное тело"""
    __heavenly_bodies = []

    def __init__(self, radius, size, color, speed):
        """Инициализация"""
        self.__radius = radius
        self.__size = size
        self.__color = color
        self.__angle = radians(270)
        self.__speed = speed
        self.__x = 0
        self.__y = 0
        self.__heavenly_bodies.append(self)

    def move(self):
        """Движение по кругу"""
        self.__x = (WIN_WIDTH // 2) + self.__radius * cos(self.__angle)
        self.__y = (WIN_HEIGHT // 2) + self.__radius * sin(self.__angle)
        self.__angle -= self.__speed
        pygame.draw.circle(screen, self.__color, (self.__x, self.__y), self.__size)

    @ClassProperty
    def heavenly_bodies(self):
        return self.__heavenly_bodies


sun = HeavenlyBody(0, 5, "yellow", 0)
mercury = HeavenlyBody(10, 2.439, "gray", 0.04150636363636364*3)  # 88 d
venus = HeavenlyBody(20, 6.050, "orange", 0.0162552736982644*3)  # 224.7 d
earth = HeavenlyBody(45, 6.371, "blue", 0.01*3)  # 0.03 = 3.(3) sec = 365.3 d
mars = HeavenlyBody(60, 3.389, "red", 0.0053166812227074*3)  # 687 d
jupiter = HeavenlyBody(140, 69.911, "brown", 0.0008433525744631725*3)  # 4331 d
saturn = HeavenlyBody(280, 58.232, "orange", 0.0003394446303111408*3)  # 10760.4 d
uranus = HeavenlyBody(370, 25.362, "blue", 0.0001190321161460625*3)  # 30685.5 d
neptune = HeavenlyBody(425, 24.622, "cyan", 0.00006067980197361863*3)  # 60194 d
pluto = HeavenlyBody(465, 1.188, "gray", 0.0000403225753034493*3)  # 90583.5 d
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
    screen.fill("black")
    for planet in HeavenlyBody.heavenly_bodies:
        planet.move()
    pygame.display.update()
    clock.tick(FPS)
