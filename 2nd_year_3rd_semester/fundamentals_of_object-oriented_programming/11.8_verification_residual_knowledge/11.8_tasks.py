import time
# from memory_profiler import profile
from tkinter import *


def show_info(msg=''):
    """Вывод сообщения на экран"""
    print(msg)


def show_time(t, msg=''):
    """Вывод сообщения о времени на экран"""
    print()
    print(msg)
    print(t)


def show_result(res, msg=''):
    """Вывод сообщения о результате на экран"""
    if res is not None:
        print()
        print(msg)
        print(res)


def show_error(err, msg=''):
    """Вывод сообщения об ошибке на экран"""
    print()
    print(msg)
    print(err)


def show_input_data(msg='', *p):
    """Вывод сообщения о исходных данных на экран"""
    print()
    print(msg)
    print(*p)


def show_two_dimensional_list(matrix, msg=''):
    """Вывод двумерного листа на экран"""
    if matrix is not None:
        print(msg)
        for row in matrix:
            for elem in row:
                print(elem, end="\t")
            print()


# @profile
def task_1(pa, pb, pc):
    """
    #Задача 1.
    Cоставить программу обмена значениями трех переменных а, b, c:
    так, чтобы b присвоилось значение c, с присвоить значение а,
    a присвоить значение b.
    Сколько времени и памяти затратилось на выполнение программы?
    :param pa: Any
    :param pb: Any
    :param pc: Any
    :return: pb, pc, pa
    """
    return pb, pc, pa


def task_1_action():
    """#Задача 1. Действие"""

    # Исходные данные
    fa = 5
    sa = 4
    ta = 3

    # Засекаем начальное время
    start_time = time.time()

    # Пытаемся вычислить результат
    res = try_calculate_result(task_1, fa, sa, ta)

    # Высчитываем прошедшее время
    end_time = time.time() - start_time

    # Показываем результат
    show_result(res, "#Задача 1")

    # Показываем исходные данные
    show_input_data("Исходные данные: ", fa, sa, ta)

    # Показываем затраченное время
    show_time(end_time, "Времени прошло: ")

    # Line #    Mem usage    Increment  Occurences   Line Contents
    # ============================================================
    #     50     45.0 MiB     45.0 MiB           1   @profile
    #     51                                         def task_1(pa, pb, pc):
    #     52                                             """
    #     53                                             #Задача 1.
    #     54                                             Cоставить программу обмена значениями трех переменных а, b, c:
    #     55                                             так, чтобы b присвоилось значение c, с присвоить значение а,
    #     56                                             a присвоить значение b.
    #     57                                             Сколько времени и памяти затратилось на выполнение программы?
    #     58                                             :param pa: Any
    #     59                                             :param pb: Any
    #     60                                             :param pc: Any
    #     61                                             :return: pb, pc, pa
    #     62                                             """
    #     63     45.0 MiB      0.0 MiB           1       return pb, pc, pa


def task_2_1(pa, pb):
    """
    #Задача 2.1
    Консольная программа.
    Пользователь вводит 2 числа. Проверьте, что это именно числа, если это не так,
    то выведите пользователю ошибку и попросите ввести число снова.
    Когда пользователь ввел числа, выведите сумму этих чисел
    :param pa: Any
    :param pb: Any
    :return: pa + pb : float
    """
    fn = convert_str_to_float(pa)
    sn = convert_str_to_float(pb)
    return fn + sn


def task_2_1_action():
    """#Задача 2.1. Действие"""

    # Исходные данные
    first_user_input_number = input_one_number()
    second_user_input_number = input_one_number()

    user_input_is_not_none = first_user_input_number is not None and second_user_input_number is not None

    if user_input_is_not_none:

        # Пытаемся вычислить результат
        res = try_calculate_result(task_2_1, first_user_input_number, second_user_input_number)

        # Показываем результат
        show_result(res, "#Задача 2.1")


def task_2_2(pn: int):
    """
    #Задача 2.2
    Доработайте задачу так, чтобы пользователь мог вводить n разных чисел.
    Предоставьте возможность ввести n самому пользователю
    :param pn: int
    :return: sum of pn numbers
    """

    list_numbers = []

    for number in range(pn):

        while True:
            user_input = input_one_number()
            if user_input is not None:
                list_numbers.append(user_input)
                break

    return sum(list_numbers)


def task_2_2_action():
    """#Задача 2.2. Действие"""

    n_user_input = input_one_number("Введите число: ", int)

    user_input_is_not_none = n_user_input is not None

    if user_input_is_not_none:

        res = try_calculate_result(task_2_2, n_user_input)

        show_result(res, "#Задача 2.2")


# @profile
def task_3_1(px: float):
    """
    #Задача 3.1
    Дано число x, которое принимает значение от 0 до 100.
    Вычислите чему будет равно x^5
    :param px: float
    :return: px ** 5
    """
    if 0 <= px <= 100:
        return px ** 5

    raise_error(ValueError, "x does not match the condition: 0 <= x <= 100")


def task_3_1_action():
    """#Задача 3.1. Действие"""

    x_user_input = input_one_number()

    if x_user_input is not None:

        start_time = time.time()

        res = try_calculate_result(task_3_1, x_user_input)

        end_time = time.time() - start_time

        show_result(res, "#Задача 3.1")

        show_time(end_time, "Времени прошло: ")

    # Line #    Mem usage    Increment  Occurences   Line Contents
    # ============================================================
    #    180     45.0 MiB     45.0 MiB           1   @profile
    #    181                                         def task_3_1(px: float):
    #    182                                             """
    #    183                                             #Задача 3.1
    #    184                                             Дано число x, которое принимает значение от 0 до 100.
    #    185                                             Вычислите чему будет равно x^5
    #    186                                             :param px: float
    #    187                                             :return: px ** 5
    #    188                                             """
    #    189     45.0 MiB      0.0 MiB           1       if 0 <= px <= 100:
    #    190     45.0 MiB      0.0 MiB           1           return px ** 5
    #    191
    #    192                                             raise_error(ValueError, "x doesnt match the conditn:0<=x<=100")


# @profile
def task_3_2(px):
    """
    #Задача 3.2
    Измените задачу так, чтобы для вычисления степени использовалось
    только умножение.
    Посмотрите сколько времени и памяти занимают оба метода.
    Что можно сделать для оптимизации данной задачи?
    :param px: float
    :return: px ** 5 but with * five times
    """

    value = px

    if 0 <= px <= 100:
        for i in range(4):
            value = value * px
        return value

    raise_error(ValueError, "x does not match the condition: 0 <= x <= 100")


def task_3_2_action():
    """#Задача 3.2. Действие"""

    x_user_input = input_one_number()

    if x_user_input is not None:

        start_time = time.time()

        res = try_calculate_result(task_3_2, x_user_input)

        end_time = time.time() - start_time

        show_result(res, "#Задача 3.2")

        show_time(end_time, "Времени прошло: ")

    # Line #    Mem usage    Increment  Occurences   Line Contents
    # ============================================================
    #    229     44.6 MiB     44.6 MiB           1   @profile
    #    230                                         def task_3_2(px):
    #    231                                             """
    #    232                                             #Задача 3.2
    #    233                                             Измените задачу так,чтобы для вычисления степени использовалось
    #    234                                             только умножение.
    #    235                                             Посмотрите сколько времени и памяти занимают оба метода.
    #    236                                             Что можно сделать для оптимизации данной задачи?
    #    237                                             :param px: float
    #    238                                             :return: px ** 5 but with * five times
    #    239                                             """
    #    240
    #    241     44.6 MiB      0.0 MiB           1       value = px
    #    242
    #    243     44.6 MiB      0.0 MiB           1       if 0 <= px <= 100:
    #    244     44.6 MiB      0.0 MiB           5           for i in range(4):
    #    245     44.6 MiB      0.0 MiB           4               value = value * px
    #    246     44.6 MiB      0.0 MiB           1           return value
    #    247
    #    248                                             raise_error(ValueError, "x doesnt match the conditn:0<=x<=100")


def task_4(pn: int):
    """
    Пользователь может вводить число от 0 до 250.
    Проверьте, принадлежит ли введенное число числам фибоначи
    :param pn: int
    :return: fibonacci_list has pn: bool
    """

    fibonacci_list = [0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233]

    if 0 <= pn <= 250:
        return pn in fibonacci_list

    raise_error(ValueError, "n does not match the condition: 0 <= n <= 250")


def task_4_action():
    """#Задача 4. Действие"""

    n_user_input = input_one_number()

    if n_user_input is not None:

        res = try_calculate_result(task_4, n_user_input)

        show_result(res, "#Задача 4")


def task_5(pmonth: str):
    """
    Реализуйте программу двумя способами на определение времени года в зависимости от введенного месяца года.
    :param pmonth: str
    :return: season: str, season: str
    """
    dict_month = {"de": 0, "ja": 0, "fe": 0, "ma": 1, "ap": 1, "may": 1, "june": 2, "july": 2, "au": 2, "sept": 3,
                  "oc": 3,
                  "no": 3}
    season = {0: "Winter", 1: "Spring", 2: "Summer", 3: "Autumn"}
    if dict_month[pmonth] == 0:
        return "Winter", season[dict_month[pmonth]]
    elif dict_month[pmonth] == 1:
        return "Spring", season[dict_month[pmonth]]
    elif dict_month[pmonth] == 2:
        return "Summer", season[dict_month[pmonth]]
    else:
        return "Autumn", season[dict_month[pmonth]]


def task_5_action():
    """#Задача 5. Действие"""

    user_input = input_str()

    res = try_calculate_result(task_5, user_input)

    show_result(res, "#Задача 5")


def task_6(pn: int):
    """
    #Задача 6
    Посчитайте сумму, количество четных и нечетных чисел от 1 до n. N вводит пользователь.
    :param pn: int
    :return: sum until pn: int, counter even: float, counter odd: float
    """

    sum_n = 0

    for i in range(1, pn + 1):
        sum_n += i

    if pn % 2 == 0:
        return sum_n, pn / 2, pn / 2
    return sum_n, (pn - 1) / 2, (pn - 1) / 2 + 1


def task_6_action():
    """#Задача 6. Действие"""

    n_user_input = input_one_number("Введите число: ", int)

    if n_user_input is not None:

        res = try_calculate_result(task_6, n_user_input)

        show_result(res, "#Задача 6")


# @profile(precision=3)
def task_7(pn: int):
    """
    #Задача 7
    Для каждого из чисел от 1 до n,
    где n меньше 250 выведите количество делителей.
    N вводит пользователь.
    Выведите число и через пробел количество его делителей.
    Делителем может быть 1.
    Сколько памяти и времени занимает программа,
    что сделано для оптимизации затрачиваемых ресурсов?
    :param pn: int
    :return: all numbers in n range with counter their divisors: list
    """

    data_list = []

    if pn < 250:
        for number in range(1, pn + 1):
            counter_divisors = 0
            for divisor in range(1, convert_float_to_int((number / 2) + 1)):
                if number % divisor == 0:
                    counter_divisors += 1
            data_list.append([number, counter_divisors + 1])
        return data_list
    raise_error(ValueError, "n does not match the condition: n < 250")


def task_7_action():
    """#Задача 7. Действие"""

    n_user_input = input_one_number("Введите число: ", int)

    if n_user_input is not None:

        start_time = time.time()

        res = try_calculate_result(task_7, n_user_input)

        end_time = time.time() - start_time

        show_result(res, "#Задача 7")

        show_time(end_time, "Времени прошло: ")

    # Line #    Mem usage    Increment  Occurences   Line Contents
    # ============================================================
    #    380   44.898 MiB   44.898 MiB           1 @profile(precision=3)
    #    381                                       def task_7(pn: int):
    #    382                                        """
    #    383                                        #Задача 7
    #    384                                        Для каждого из чисел от 1 до n,
    #    385                                        где n меньше 250 выведите количество делителей.
    #    386                                        N вводит пользователь.
    #    387                                        Выведите число и через пробел количество его делителей.
    #    388                                        Делителем может быть 1.
    #    389                                        Сколько памяти и времени занимает программа,
    #    390                                        что сделано для оптимизации затрачиваемых ресурсов?
    #    391                                        :param pn: int
    #    392                                        :return: all numbers in n range with counter their divisors: list
    #    393                                        """
    #    394
    #    395   44.898 MiB    0.000 MiB           1  data_list = []
    #    396
    #    397   44.898 MiB    0.000 MiB           1  if pn < 250:
    #    398   44.906 MiB    0.004 MiB         250   for number in range(1, pn + 1):
    #    399   44.906 MiB    0.000 MiB         249     counter_divisors = 0
    #    400   44.906 MiB    0.004 MiB       15749     for divisor in range(1, convert_float_to_int((number / 2) + 1)):
    #    401   44.906 MiB    0.000 MiB       15500         if number % divisor == 0:
    #    402   44.906 MiB    0.000 MiB        1164             counter_divisors += 1
    #    403   44.906 MiB    0.000 MiB         249     data_list.append([number, counter_divisors + 1])
    #    404   44.906 MiB    0.000 MiB           1   return data_list
    #    405                                        raise_error(ValueError, "n does not match the condition: n <


def task_8(pn: int, pm: int):
    """
    #Задача 8
    Найти все различные пифагоровы тройки из интервала от N до М.
    :param pn: int
    :param pm: int
    :return: list
    """
    list_pythagorean_triples = []
    for b in range(pn+1, pm + 1):
        for a in range(b-1, 0, -1):
            c = (b - a) * (b - a) + b * b
            if pm >= c ** 0.5 == convert_float_to_int(c ** 0.5):
                list_pythagorean_triples.append([b - a, b, convert_float_to_int(c ** 0.5)])
    return list_pythagorean_triples


def task_8_action():
    """#Задача 8. Действие"""

    n_user_input = input_one_number("Введите число: ", int)
    m_user_input = input_one_number("Введите число: ", int)

    user_input_is_not_none = n_user_input is not None and m_user_input is not None

    if user_input_is_not_none:

        res = try_calculate_result(task_8, n_user_input, m_user_input)

        show_result(res, "#Задача 8")


def task_9(pn, pm):
    """
    #Задача 9
    Найти все целые числа из интервала от N до M, которые делятся на каждую из своих цифр.
    :param pn: int
    :param pm: int
    :return: list
    """
    list_numbers = []

    for number in range(pn, pm+1):

        counter_divisions = 0
        counter_digits_in_number = 0
        abs_number = abs(number)

        while True:
            if abs_number <= 0:
                break

            if abs_number % 10 != 0 and number % (abs_number % 10) == 0:
                counter_divisions += 1

            counter_digits_in_number += 1
            abs_number = convert_float_to_int(abs_number / 10)

        if counter_divisions == counter_digits_in_number and counter_divisions != 0:
            list_numbers.append(number)

    return list_numbers


def task_9_action():
    """#Задача 9. Действие"""

    n_user_input = input_one_number("Введите число: ", int)
    m_user_input = input_one_number("Введите число: ", int)

    user_input_is_not_none = n_user_input is not None and m_user_input is not None

    if user_input_is_not_none:

        res = try_calculate_result(task_9, n_user_input, m_user_input)

        show_result(res, "#Задача 9")


def task_10(pn: int):
    """
    #Задача 10
    Натуральное число называется совершенным, если оно равно сумме всех своих делителей, включая единицу.
    Вывести первые N (N<5) совершенных чисел на экран.
    :param pn: int
    :return: list perfect numbers: list
    """
    list_perfect_numbers = []
    list_divisors = []
    number = 0

    if pn < 5:

        while len(list_perfect_numbers) < pn:

            number += 1
            list_divisors.clear()

            for divisor in range(1, convert_float_to_int(number / 2) + 1):
                if number % divisor == 0:
                    list_divisors.append(divisor)

            if sum(list_divisors) == number:
                list_perfect_numbers.append(number)

        return list_perfect_numbers

    raise_error(ValueError, "n does not match the condition: n < 5")


def task_10_action():
    """#Задача 10. Действие"""

    n_user_input = input_one_number("Введите число: ", int)

    user_input_is_not_none = n_user_input is not None

    if user_input_is_not_none:

        res = try_calculate_result(task_10, n_user_input)

        show_result(res, "#Задача 10")


def task_11():
    """
    #Задача 11
    Задайте одномерный массив в коде и выведите в консоль последний элемент данного массива тремя способами.
    Сравните их по времени выполнения.
    :return: None
    """
    show_info("#Задача 11")

    example_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    show_input_data("Исходные данные: ", example_list)

    functions_list = [task_11_1, task_11_2, task_11_3]
    for function in functions_list:

        start_time = time.time()

        res = function(example_list)

        end_time = time.time() - start_time

        show_result(res, function.__doc__)

        show_time(end_time, "Времени прошло: ")


def task_11_1(lst: list):
    """Первый способ"""
    return lst[-1]


def task_11_2(lst: list):
    """Второй способ"""
    return lst[len(lst)-1]


def task_11_3(lst: list):
    """Третий способ"""
    return lst[::-1][0]


def task_11_action():
    """#Задача 11. Действие"""

    try_calculate_result(task_11)


def task_12(example_list: list):
    """
    #Задача 12
    Задайте одномерный массив в коде и выведите в консоль массив в обратном порядке
    :param example_list: list
    :return: inverse list: list
    """

    return example_list[::-1]


def task_12_action():
    """#Задача 12. Действие"""

    example_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    res = try_calculate_result(task_12, example_list)

    show_result(res, "#Задача 12")

    show_input_data("Исходные данные: ", example_list)


def task_13(example_list: list):
    """
    #Задача 13
    Реализуйте нахождение суммы элементов массива через рекурсию.
    Массив можно задать в коде.
    :param example_list: list
    :return: sum of all numbers in matrix
    """
    return sum(deep_numbers_matrix(example_list, []))


def task_13_action():
    """#Задача 13. Действие"""

    example_list = [[1, 2], [3], [[[4]]], [[5, 6], 7], 8, 9, 10]

    res = try_calculate_result(task_13, example_list)

    show_result(res, "#Задача 13")

    show_input_data("Исходные данные: ", example_list)


def task_14_1():
    """
    #Задача 14.1
    Реализуйте оконное приложение-конвертер рублей в доллары.
    Создайте окно ввода для суммы в рублях
    :return: None
    """
    win = Tk()
    win.title("converter")

    win.protocol("WM_DELETE_WINDOW", win.destroy)
    win.geometry("450x200")

    def click_on_converter():
        """Нажатие на кнопку конвертации"""
        try:
            dollar_exchange_rate = 60.22
            rub = convert_str_to_float(entry_rubles.get())
            result = rub / dollar_exchange_rate
            result = round(result, 3)
            label_result.configure(text=convert_float_to_str(result) + "$")
        except ValueError:
            label_result["text"] = "Ошибка! Проверьте корректность ввода."

    Label(win, text="Введите количество рублей", font="Arial 14")
    entry_rubles = Entry(font="Arial 14")
    Button(text="Конвертировать", command=click_on_converter, font="Arial 14")
    label_result = Label(win, font="Arial 14")

    for tk_obj in win.winfo_children():
        tk_obj.pack(expand=True, fill="both")

    running = True

    while running:
        try:
            win.update()
        except TclError:
            return


def task_14_1_action():
    """#Задача 14.1. Действие"""
    try_calculate_result(task_14_1)


def task_14_2():
    """
    #Задача 14.2
    Доработайте приложение так, чтобы можно было переводить доллары в рубли.
    :return: None
    """
    win = Tk()
    win.title("converter")

    win.protocol("WM_DELETE_WINDOW", win.destroy)
    win.geometry("450x200")

    operation = [1]

    def click_on_converter():
        """Нажатие на кнопку конвертации"""
        try:
            exchange_rate = 60.22
            currency = convert_str_to_float(entry_currency.get())
            if operation[0] == 0:
                result = currency * exchange_rate
                result = round(result, 3)
                label_result.configure(text=convert_float_to_str(result) + "₽")
            else:
                result = currency / exchange_rate
                result = round(result, 3)
                label_result.configure(text=convert_float_to_str(result) + "$")
        except ValueError:
            label_result["text"] = "Ошибка! Проверьте корректность ввода."

    def change_currency_again():
        """Изменение валюты обратно"""
        operation[0] = 1
        button_change_currency["command"] = change_currency
        label_text["text"] = "Введите количество рублей"

    def change_currency():
        """Изменение валюты"""
        operation[0] = 0
        button_change_currency["command"] = change_currency_again
        label_text["text"] = "Введите количество долларов"

    button_change_currency = Button(text="Поменять местами", command=change_currency, font="Arial 14")
    label_text = Label(win, text="Введите количество рублей", font="Arial 14")
    entry_currency = Entry(font="Arial 14")
    Button(text="Конвертировать", command=click_on_converter, font="Arial 14")
    label_result = Label(win, font="Arial 14")

    for tk_obj in win.winfo_children():
        tk_obj.pack(expand=True, fill="both")

    running = True

    while running:
        try:
            win.update()
        except TclError:
            return


def task_14_2_action():
    """#Задача 14.2. Действие"""
    try_calculate_result(task_14_2)


def task_15(pn: int, pm: int):
    """
    #Задача 15
    Реализуйте вывод таблицы умножения в консоль размером n на m которые вводит пользователь,
    но при этом они не могут быть больше 20 и меньше 5
    :param pn: int
    :param pm: int
    :return: list of list
    """
    if 5 <= pn <= 20 and 5 <= pm <= 20:
        return [[(index_column+1)*(index_row+1) for index_column in range(pm)] for index_row in range(pn)]
    raise_error(ValueError, "n or m does not match the condition: 5 <= n <= 20 and 5 <= m <= 20")


def task_15_action():
    """#Задача 15. Действие"""

    n_user_input = input_one_number("Введите число: ", int)
    m_user_input = input_one_number("Введите число: ", int)

    user_input_is_not_none = n_user_input is not None and m_user_input is not None

    if user_input_is_not_none:
        res = try_calculate_result(task_15, n_user_input, m_user_input)

        show_two_dimensional_list(res, "#Задача 15")


def task_16(data_ships: list):
    """
    #Задача 16
    Реализуйте вывод в консоль поле для морского боя с выставленными кораблями.
    Данные о кораблях, можно подгружать из файла или генерировать самостоятельно
    :param data_ships: list
    :return: list
    """
    battlefield = [["~~~" for _ in range(10)] for _ in range(10)]

    for index_ship in range(len(data_ships)):
        battlefield[data_ships[index_ship][0]][data_ships[index_ship][1]] = "|H|"

    battlefield.insert(0, ["", " A", " B", " C", " D", " E", " F", " G", " H", " I", " J"])

    for index_row in range(1, len(battlefield)):
        battlefield[index_row].insert(0, convert_int_to_str(index_row))

    return battlefield


def task_16_action():
    """#Задача 16. Действие"""

    data_ships = try_calculate_result(try_read_file, read_file, "ships.txt")

    if data_ships is not None:
        res = try_calculate_result(task_16, data_ships)

        show_two_dimensional_list(res, "#Задача 16")


def read_file(filename: str):
    """
    Считывает файл и записывает в лист
    :param filename: str
    :return: list
    """
    data_list = []
    with open(filename, "r") as file:

        while True:
            line = file.readline().split()
            if not line:
                break
            line = list(map(int, line))
            data_list.append(line)

    return data_list


def try_read_file(func, *p):
    """
    Отлавливает ошибки при чтении файла
    :param func: def
    :param p: parameters
    :return: return function return
    """
    try:
        return func(*p)
    except OSError as err:
        return show_error(err, "Ошибка в получении данных с устройства")


def deep_numbers_matrix(matrix: list, numbers: list):
    """Нахождение всех чисел в массиве"""
    if not isinstance(matrix, list):
        numbers.append(matrix)
        return matrix
    else:
        [deep_numbers_matrix(elem, numbers) for elem in matrix]
        return numbers


def raise_error(type_error, msg=''):
    """Поднимает ошибку"""
    raise type_error(msg)


def input_one_number(msg="Введите число: ", type_number=float):
    """Ввод одного числа с клавиатуры"""
    try:
        show_info()
        s = input(msg)
        if type_number is int:
            return convert_str_to_int(s)
        return convert_str_to_float(s)
    except ValueError as err:
        return show_error(err, "Ошибка при получении данных")


def input_str(msg="Введите строку: "):
    """Ввод строки с клавиатуры"""
    show_info()
    s = input(msg)
    return s


def convert_str_to_float(param_str):
    """Конвертирует str параметр в float и возвращает"""
    return float(param_str)


def convert_str_to_int(param_str):
    """Конвертирует str параметр в int и возвращает"""
    return int(param_str)


def convert_float_to_int(param_float):
    """Конвертирует float параметр в int и возвращает"""
    return int(param_float)


def convert_int_to_str(param_int):
    """Конвертирует int параметр в str и возвращает"""
    return str(param_int)


def convert_float_to_str(param_float):
    """Конвертирует float параметр в str и возвращает"""
    return str(param_float)


def try_calculate_result(func, *params):
    """Проверка на возможность вычисления данных"""
    try:
        return func(*params)
    except ArithmeticError as err:
        return show_error(err, "Ошибка при вычислении данных. Арифметическая ошибка. Попробуйте ввести снова")
    except IndexError as err:
        return show_error(err, "Ошибка при вычислении данных. Выход за границы. Попробуйте ввести снова")
    except ValueError as err:
        return show_error(err, "Ошибка при вычислении данных. Попробуйте ввести снова")
    except TypeError as err:
        return show_error(err, "Ошибка при вычислении данных. Попробуйте ввести снова")
    except KeyError as err:
        return show_error(err, "Ошибка при вычислении данных. Не найден ключ. Попробуйте ввести снова")


def select_action(functions):
    """Выбор действия из доступных"""
    is_run = True

    while is_run:

        show_info()
        show_info("Доступные операции:")
        for i, item in enumerate(functions):
            print(i, item.__name__)

        try:
            show_info()
            i = int(input("Выберите операцию: "))
        except ValueError as err:
            return show_error(err, "Ошибка при получении данных:")

        if 0 <= i < len(functions):
            function = functions[i]
            function()
        else:
            is_run = False


def get_actions():
    """Получение доступных действий"""
    return [task_1_action,
            task_2_1_action,
            task_2_2_action,
            task_3_1_action,
            task_3_2_action,
            task_4_action,
            task_5_action,
            task_6_action,
            task_7_action,
            task_8_action,
            task_9_action,
            task_10_action,
            task_11_action,
            task_12_action,
            task_13_action,
            task_14_1_action,
            task_14_2_action,
            task_15_action,
            task_16_action]


if __name__ == "__main__":
    select_action(get_actions())
