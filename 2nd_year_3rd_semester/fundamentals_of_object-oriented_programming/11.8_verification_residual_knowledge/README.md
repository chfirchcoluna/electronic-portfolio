## Task:

#Task 1
Write a programme to exchange the values of three variables a, b, c: so that b is assigned the value of c, c is assigned the value of a, and a is assigned the value of b. Spend a minimum number of lines. 

#Task 2.1
Console programme.
The user enters 2 numbers. Check that they are numbers, if they are not, print an error and ask the user to enter the numbers again. When the user has entered the numbers, print the sum of these numbers.

#Task 2.2.
Refine the problem so that the user can enter n different numbers. Allow the user to enter n.

#Task 3.1
Given a number x, which takes a value from 0 to 100. Calculate what will be equal to x^5

#Task 3.2
Modify the problem so that only multiplication is used to calculate the degree.
+0.5 points: see how much time and memory both methods take. What can be done to optimise this task?

#Task 4
A user can enter a number from 0 to 250. Check if the entered number belongs to the Fibonacci numbers.

#Task 5
Implement the programme in two ways to determine the time of the year depending on the month of the year entered.

#Task 6
Calculate the sum, the number of even and odd numbers from 1 to N. N is entered by the user.

#Task 7
For each of the numbers from 1 to N, where N is less than 250, print the number of divisors. N is entered by the user.
Print the number and through a space the number of its divisors. The divisor can be 1.  What can be done to optimise the resources used?

#Task 8
Find all the different Pythagorean triples from the interval from N to M.

#Task 9
Find all integers from the interval from N to M that are divisible by each of their digits.

#Task 10
A natural number is called perfect if it is equal to the sum of all its divisors, including one. Print the first N (N<5) perfect numbers on the screen.

#Task 11
Set a one-dimensional array in code and print the last element of this array to the console in three ways. Compare them in terms of execution time.

#Task 12
Set a one-dimensional array in code and print the array in reverse order to the console.

#Task 13
Implement finding the sum of array elements through recursion. The array can be defined in the code.

#Task 14.1
Implement a window application-converter of rubles to dollars. Create an input window for the amount in rubles. Modify the application so that it is possible to convert dollars into rubles.

#Task 14.2
Develop the application so that you can convert dollars into rubles.

#Task 15
Realise the output of the multiplication table in the console of size n by m that the user enters, but they can not be greater than 20 and less than 5.

#Task 16
Realise the output in the console field for naval combat with exposed ships.
Data about ships can be loaded from a file or generated by yourself.

## Demonstration of work [verification residual knowledge](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/fundamentals_of_object-oriented_programming/11.8_verification_residual_knowledge/11.8_tasks.py)

![1](images/1.png "1")
![2](images/2.png "2")
![3](images/3.png "3")
![4](images/4.png "4")
![5](images/5.png "5")
![6](images/6.png "6")
![7](images/7.png "7")
![8](images/8.png "8")
![9](images/9.png "9")
![10](images/10.png "10")
![11](images/11.png "11")
![12](images/12.png "12")
![13](images/13.png "13")
![14](images/14.png "14")
![15](images/15.png "15")
![16](images/16.png "16")
![17](images/17.png "17")
![18](images/18.png "18")
![19](images/19.png "19")
![20](images/20.png "20")
![21](images/21.png "21")