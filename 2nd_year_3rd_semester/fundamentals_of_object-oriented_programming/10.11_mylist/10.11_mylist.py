class MyList(list):

    def __init__(self, *args, **kwargs):
        list.__init__(self, *args, **kwargs)
        # super(MyList, self).__init__(*args, **kwargs)

    def __sub__(self, other):
        return MyList([x - y for x, y in zip(self, other)])

    def __rsub__(self, other):
        return MyList([x - y for x, y in zip(other, self)])


a = MyList([3, 4, 5])
b = [2, 3]
c = a - b
d = c - a
print(d)
print(b - a)
