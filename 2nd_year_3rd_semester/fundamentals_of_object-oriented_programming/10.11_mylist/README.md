## Task:

Create a class My_list, a descendant of the `list` class. Implement the subtraction and division operators in it, implementing these two operations as you see fit. Also override the `super` method so that it outputs the contents of the object as in the original list, while adding something of its own. Keep in mind that the subtraction operation should not raise exceptions with any data types inside.

## Demonstration of work [mylist](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/fundamentals_of_object-oriented_programming/10.11_mylist/10.11_mylist.py)

output:
[-2, -3]
[-1, -1]