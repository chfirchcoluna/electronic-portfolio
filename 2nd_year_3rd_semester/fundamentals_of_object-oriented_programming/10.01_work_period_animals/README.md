## Task:

The critter should have two scales (satiety and joy) and two methods (feed and play). As time passes (beat length from 1 to 10 seconds of your choice) the critter gets sadder and hungrier. When one of the scales is empty, the game ends. The play action takes points away from the satiety scale.

## Demonstration of work [pet](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/fundamentals_of_object-oriented_programming/10.01_work_period_animals/10.01_work_period_animals.py)

![1](1.png "1")
![2](2.png "2")
![3](3.png "3")
![4](4.png "4")
![5](5.png "5")