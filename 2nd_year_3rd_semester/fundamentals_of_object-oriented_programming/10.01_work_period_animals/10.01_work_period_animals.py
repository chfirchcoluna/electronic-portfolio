import time
from tkinter import *
import sys


class ClassProperty:
    """Свойство для атрибутов"""

    def __init__(self, __function):
        """Инициализация функции"""
        self.__function = __function

    def __get__(self, __instance, __owner):
        """Get для атрибута"""
        return self.__function(__owner)


class FrozenMeta(type):
    """Метакласс"""

    def __setattr__(self, key, value):
        """Запрет на создание атрибутов класса"""
        raise AttributeError("unable to create a new attribute")

    def __delattr__(self, item):
        """Запрет на удаление атрибута класса"""
        raise AttributeError("the attribute cannot be deleted")


class State(metaclass=FrozenMeta):
    """Состояния жизни и/или настроения"""
    __dead = "Dead"
    __angry = "Angry"
    __depressed = "Depressed"
    __sad = "Sad"
    __normal = "Normal"
    __kind = "Kind"
    __happy = "Happy"

    @ClassProperty
    def angry(self):
        return self.__angry

    @ClassProperty
    def happy(self):
        return self.__happy

    @ClassProperty
    def dead(self):
        return self.__dead

    @ClassProperty
    def sad(self):
        return self.__sad

    @ClassProperty
    def depressed(self):
        return self.__depressed

    @ClassProperty
    def normal(self):
        return self.__normal

    @ClassProperty
    def kind(self):
        return self.__kind


class DataElement(metaclass=FrozenMeta):
    """Индекс в списке"""
    __first_interval = 0
    __last_interval = -1
    __left_border = 0
    __right_border = 1
    __background_color = 2
    __state_info = 3

    @ClassProperty
    def first_interval(self):
        return self.__first_interval

    @ClassProperty
    def last_interval(self):
        return self.__last_interval

    @ClassProperty
    def left_border(self):
        return self.__left_border

    @ClassProperty
    def right_border(self):
        return self.__right_border

    @ClassProperty
    def background_color(self):
        return self.__background_color

    @ClassProperty
    def state_info(self):
        return self.__state_info


class Animal(metaclass=FrozenMeta):
    """Зверушка"""
    # Первое число списка отвечает за левую границу проверяемого диапазона (листа в листе),
    # второе - за правую границу,
    # третье - за цвет фона текста,
    # четвёртое - за состояние жизни и/или настроения
    # Дипазоны идут на убывание: нулевой - самый счастливый уровень для питомца
    # Смертью считается выход за верхнюю границу нулевого диапазона и выход за нижнюю границу последнего диапазона
    __list_intervals = [[21, 29, "yellow", State.happy],
                        [11, 20, "green", State.kind],
                        [0, 10, "lightgreen", State.normal],
                        [-10, -1, "grey", State.sad],
                        [-19, -11, "brown", State.depressed],
                        [-29, -20, "red", State.angry]]

    def __init__(self, __life=True, __food=6, __eat_timeout=10, __play_gives_food=5, __subtracting_food=10,
                 __border_for_opportunity_to_play=0, __disable_button_play=False,
                 __background_label_color="lightgreen"):
        """Инициализация"""
        self.__life = __life
        self.__food = __food
        self.__eat_timeout = __eat_timeout
        self.__state = State.normal
        self.__time_global = time.time()
        self.__timer = time.time()
        self.__play_gives_food = __play_gives_food
        self.__subtracting_food = __subtracting_food
        self.__border_for_opportunity_to_play = __border_for_opportunity_to_play
        self.__disable_button_play = __disable_button_play
        self.__background_label_color = __background_label_color

    __slots__ = ["__timer", "__time_global", "__life", "__eat_timeout", "__food", "__disable_button_play",
                 "__background_label_color", "__state", "__play_gives_food", "__subtracting_food",
                 "__border_for_opportunity_to_play"]

    @property
    def food(self):
        return self.__food

    @property
    def life(self):
        return self.__life

    @property
    def state(self):
        return self.__state

    @property
    def eat_timeout(self):
        return self.__eat_timeout

    @property
    def timer(self):
        return self.__timer

    @property
    def time_global(self):
        return self.__time_global

    @property
    def play_gives_food(self):
        return self.__play_gives_food

    @property
    def substracting_food(self):
        return self.__subtracting_food

    @property
    def border_for_opportunity_to_play(self):
        return self.__border_for_opportunity_to_play

    @property
    def disable_button_play(self):
        return self.__disable_button_play

    @property
    def color_label_background(self):
        return self.__background_label_color

    def eat(self):
        """Кормёжка"""
        self.__food += 1

    def play(self):
        """Игра с питомцем"""
        __difference_between_right_border_and_giving_food = \
            self.__list_intervals[DataElement.first_interval][DataElement.right_border] - self.__play_gives_food
        # Проверка на условие, чтобы питомец не умер от игры:
        if self.__food > __difference_between_right_border_and_giving_food:
            self.__food = __difference_between_right_border_and_giving_food
        self.__food += self.__play_gives_food
        self.__disable_button_play = True

    def hunger(self):
        """Голод питомца"""
        if time.time() - self.__time_global >= self.__eat_timeout:  # При прошествии времени выполняется голод
            self.__time_global = time.time()
            self.__food -= self.__subtracting_food
            if self.__food >= self.__border_for_opportunity_to_play:  # Если еды > ..., то играть с питомцем можно
                self.__disable_button_play = False

    def state_life(self):
        """Проверка статуса жизни"""
        if self.__food > self.__list_intervals[DataElement.first_interval][DataElement.right_border] or \
                self.__food < self.__list_intervals[DataElement.last_interval][DataElement.left_border]:
            self.__state = State.dead
            self.__life = False

    def state_by_food(self):
        """Возврат состояния питомца на основе количества еды"""
        for interval in range(len(self.__list_intervals)):
            if self.__list_intervals[interval][DataElement.left_border] <= self.__food \
                    <= self.__list_intervals[interval][DataElement.right_border]:
                self.__background_label_color = self.__list_intervals[interval][DataElement.background_color]
                self.__state = self.__list_intervals[interval][DataElement.state_info]
                # Если еды < ..., то играть нельзя:
                if self.__list_intervals[interval][DataElement.left_border] < self.__border_for_opportunity_to_play:
                    self.__disable_button_play = True
                return self.__state


def close():
    """Безопасный выход из программы"""
    global running
    running = False
    sys.exit()


my_little_animal = Animal()
win = Tk()
win.protocol("WM_DELETE_WINDOW", close)
win.title("my_little_animal")
win.geometry("250x200")
label_time_life = Label(font="Arial 14")
label_status = Label(background="cyan", font="Arial 14", borderwidth=2, relief="raised")
button_eat = Button(text="Покормить", command=my_little_animal.eat, background="purple", font="Arial 14", fg="white")
button_play = Button(text="Поиграть", command=my_little_animal.play, background="purple", font="Arial 14", fg="white")
button_exit = Button(text="Выйти из игры", command=close, background="red", font="Arial 14")
for tk_obj in win.winfo_children():
    tk_obj.pack(expand=True, fill='both')
running = True
while running:
    if my_little_animal.life:
        if my_little_animal.disable_button_play:
            button_play["state"] = "disabled"
        else:
            button_play["state"] = "normal"
        my_little_animal.state_by_food()
        label_status.configure(text=my_little_animal.state)
        my_little_animal.hunger()
        my_little_animal.state_life()
        label_status.configure(background=my_little_animal.color_label_background)
        label_time_life.configure(text=f"Время жизни питомца: {int(time.time() - my_little_animal.timer)}")
    else:
        button_eat["state"] = "disabled"
        button_play["state"] = "disabled"
        label_status.configure(text=my_little_animal.state, background="black", fg="white")
    win.update()
