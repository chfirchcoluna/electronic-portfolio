# Game Project
## Task:

To consolidate the learnt material it is proposed to develop a game using PyGame in a team (5-8 people). During the development use the Scrum technique of Agile methodology (see the presentation in the attachment). Each team should have clearly defined roles, but everyone should participate in the development (it is highly undesirable to have one person writing code and the other only drawing). A coordinator/team leader should also be identified. Tasks of the coordinator: to present the results of the development, to provide information to the customer (teacher).

It is strongly recommended (affect the points) to consider the following aspects:

1. when developing, apply as much as possible the knowledge and skills acquired during the first and second year of study;
2. use the OOP approach;
3. if possible (at the PROJECTING stage, i.e. before writing the code) create UML diagrams. You don't need to document everything down to the last detail. You can try to mark at least the basic concepts, functions, classes and sequences in the diagrams.
4. When writing code, don't forget about PEP8. To make it easier to use it, I recommend you to install and configure some development environment like VS Code.
5. All development (all code and possibly diagrams) should be in git. Not just one commit of the finished project at once, but each stage of design and programming should correspond to a commit to the git. Each team member should have some set of commits for his task.
6. To organise work, use services designed for this purpose, e.g. Trello.

## [presentation](https://gitlab.com/vlad.nikz45/minesweeper-2/-/blob/main/презентация.pdf?ref_type=heads)

## [git link](https://gitlab.com/vlad.nikz45/minesweeper-2)

#
# En:

### Implemented in the project:
1. The system of saves (completely, you can quit the game and everything will be saved). To delete saves, go to the saves folder and delete the file save_player_0.json.
2. A variety of levels with their own visual styles, mood, music and weather. A variety of bombs and varying damage from them.
3. Levels can randomly spawn hearts to restore life points, randomly spawn items to help (fruit, money, magic brush).
4. Weather and wind.
5. Smooth music playback so that music does not abruptly end or start (mixer). Sounds and fonts.
6. Animation when an explosion occurs.
7. Animation of spinning coins, hearts on levels.
8. Right clicking on a number helps to put flags around it.
9. Left click on a number helps to open cells around it.
10. Fruit and berry items increase the number of life points.
11. A shop that sells items for money.
12. A Magic Brush item that opens 3 random cells without bombs
13. Death screen and replay the game from the beginning.
14. End of game screen with credits.
15. Moving the playfield for convenience, and scaling it.
16. Character animation in the bottom panel, it winks ;)
17. Character statistics, inventory in the bottom panel.
18. Level selection menu, showing statistics of each level.
19. Start menu.
20. Basic logic of the Minesweeper game.
21. The code is written on MVC template.

### Authors: students ISU 14222/14224 Vladislav Nikitin, Sergey Vdovin, Alexander Domenenko, Pavel Orlov, Daniil Botvenko, Sergey Suslikov
### Year: 2022


#
# Ru:

### Реализовано в проекте:
1. Система сохранений (полностью, вы можете выйти из игры и всё сохранится). Для удаления сохранений зайдите в папку saves и удалить файл  save_player_0.json.
2. Разнообразные уровни со своими визуальными стилями, настроением, музыкой и погодой. Разнообразным количеством бомб и варьируемым уроном от них.
3.  На уровнях возможен случайный спавн сердец для восстановления очков жизней, случайный спавн предметов для помощи (фрукты, деньги, магическая кисть).
4. Погода и ветер.
5. Плавное проигрывание музыки, чтобы резко не кончалась и не начиналась музыка (микшер). Звуки и шрифты.
6. Анимация при взрыве.
7. Анимация кручения монеток, сердец на уровнях.
8. Клик правой кнопкой мыши на цифру помогает поставить флаги вокруг.
9. Клик левой кнопкой мыши на цифру помогает открыть ячейки вокруг.
10. Предметы-фрукты и ягоды увеличивают количество очков жизней.
11. Магазин, в котором продаются предметы за деньги.
12. Предмет Волшебная кисть, которая открывает 3 случайные ячейки без бомб
13. Экран смерти и повтор игры с самого начала.
14. Экран конца игры с титрами.
15. Перемещение игрового поля для удобства, и его масштабирование.
16. Анимация персонажа в нижней панели, он подмигивает ;)
17. Статистика персонажа, инвентарь в нижней панели.
18. Меню выбора уровня, показ статистик каждого уровня.
19. Стартовое меню.
20. Базовая логика игры Сапёр.
21. Код написан на MVC шаблоне.

### Авторы: студенты ИГУ 14222/14224 Владислав Никитин, Сергей Вдовин, Александр Домненко, Павел Орлов, Даниил Ботвенко, Сергей Сусликов
### Год: 2022

#


## Demonstration of work [minesweeper](https://gitlab.com/vlad.nikz45/minesweeper-2)

![1](images/1.png "1")
![2](images/2.png "2")
![3](images/3.png "3")
![4](images/4.png "4")
![5](images/5.png "5")
![6](images/6.png "6")
![7](images/7.png "7")
![8](images/8.png "8")
![9](images/9.png "9")
![10](images/10.png "10")
![11](images/11.png "11")
![12](images/12.png "12")
![13](images/13.png "13")
![14](images/14.png "14")
![15](images/15.png "15")
![16](images/16.png "16")
![17](images/17.png "17")
![18](images/18.png "18")
![19](images/19.png "19")
![20](images/20.png "20")
![21](images/21.png "21")
![22](images/22.png "22")
![23](images/23.png "23")
![24](images/24.png "24")
![25](images/25.png "25")
![26](images/26.png "26")
![27](images/27.png "27")
![28](images/28.png "28")
![29](images/29.png "29")
![30](images/30.png "30")
![31](images/31.png "31")
![32](images/32.png "32")
![33](images/33.png "33")