<h1 align="center">Fundamentals of Object-Oriented Programming</h1>

### About subject:
<p align="left">
Goals:

1. To form a complex of theoretical knowledge of practical
skills, sufficient for independent development of algorithms and programmes in object-oriented programming methodology.
object-oriented programming methodology. To develop skills of thinking in
style of the object-oriented approach paradigm;
2. Gain practical experience of working with compilable programming languages that support the basic principles of OO.
programming languages that support the basic principles of OOP.

Objectives:
1. Gain skills and experience of working with classes, structures, objects, master the
basic OOP principles: encapsulation, inheritance, polymorphism;
2. To get skills and experience in developing own data types, work with
basic form controls;
3. Master the work with associative arrays, iterators, timers, smart
pointers, connection to the server;
4. Gain skills and experience in using parameterised classes,
functors, lambda functions.
</p>

<h2></h2>

<h2><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester\fundamentals_of_object-oriented_programming\09.13_Solar_System_model">TASK Solar System model 00</a></h2>

<h2><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester\fundamentals_of_object-oriented_programming\10.01_work_period_animals">TASK working with animal 01</a></h2>

<h2><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester\fundamentals_of_object-oriented_programming\10.11_four_in_a_row">TASK four in a row 02</a></h2>

<h2><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester\fundamentals_of_object-oriented_programming\10.11_mylist">TASK mylist 03</a></h2>

<h2><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester\fundamentals_of_object-oriented_programming\10.15_fourangles_and_triangles_and_vertexes">TASK fourangles and triangles and vertexes 04</a></h2>

<h2><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester\fundamentals_of_object-oriented_programming\11.8_verification_residual_knowledge">TASK verification residual knowledge 05</a></h2>

<h2><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester\fundamentals_of_object-oriented_programming\12.20_game_project">TASK GAME PROJECT MINESWEEPER 06</a></h2>