## Task:

Implement your own game or a 4-in-a-row game using the rules below.

Realise a four-in-a-row game in a man vs. man variant.
Rules of the game
1. 6x6 rectangular field is used.
2. Red and yellow players take turns.
3. The move consists in adding a chip of their colour to one of the columns
of the field, thus filling the lowest of the free cells of this column.
column.
4. The player who has four pieces of his colour in a row horizontally, vertically or diagonally wins the game
5. If the field is full and no one wins - a draw is declared.

## Demonstration of work [four in a row](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/fundamentals_of_object-oriented_programming/10.11_four_in_a_row/10.11_four_in_a_row.py)

![diagrams](diagrams.svg "diagrams")
![1](images/1.png "1")
![2](images/2.png "2")
![3](images/3.png "3")
![4](images/4.png "4")
![5](images/5.png "5")
![6](images/6.png "6")
![7](images/7.png "7")
![8](images/8.png "8")
![9](images/9.png "9")
![10](images/10.png "10")
![11](images/11.png "11")