import time
import pygame
from pygame.locals import (KEYDOWN, K_ESCAPE, K_q, K_r)

# Цвета
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
YELLOW = (255, 237, 0)
BLUE = (0, 0, 255)
BLACK = (0, 0, 0)
CYAN = (0, 183, 235)
ORANGE = (255, 165, 0)
VINOUS = (155, 45, 48)


def data_read_from_file():
    """Открытие файла на чтение и запись данных из файла в data"""
    data_list = []
    try:
        filename = "settings/settings.txt"
        with open(filename) as file:
            for line in file:
                line = line.split()
                data_list.append([line[0], int(line[1])])
        return data_list
    except IndexError as err_index:
        print(err_index, '\nБудут установлены значения по умолчанию')
    except ValueError as err_value:
        print(err_value, '\nБудут установлены значения по умолчанию')
    except OSError as err_os:
        print(err_os, '\nБудут установлены значения по умолчанию')


# Запись данных из файла
data = data_read_from_file()


class DataElement:
    """Индекс в списке"""
    rectangle = 0
    empty_player = 0
    radius = 0
    color = 1
    sub_color = 2
    clicked = 1
    LMB = 1
    first_player = 1
    value = 1
    second_player = 2
    number_columns = 0
    number_rows = 1
    value_for_victory = 2
    multiply_number = 3


class Game:
    """Игра"""

    def __init__(self, column=None, row=None):
        """Инициализация"""
        if column is None and row is None:
            self.columns, self.rows = self.__get_data()
        else:
            self.columns, self.rows = column, row
        self.__players = [[DataElement.empty_player, WHITE, WHITE],
                          [DataElement.first_player, ORANGE, YELLOW],
                          [DataElement.second_player, VINOUS, RED]]
        self.__game_over = False
        self.__winner = 0
        self.__player_now = 1
        self.__need_show_result_game = False
        if self.columns < 1:
            self.columns = 7
            print("Количество столбцов было задано меньше одного. Установлено значение по умолчанию")
        if self.rows < 1:
            self.rows = 6
            print("Количество строк было задано меньше одного. Установлено значение по умолчанию")
        self._NUMBER_COLUMNS = self.columns
        self._NUMBER_ROWS = self.rows

    @staticmethod
    def __get_data():
        """Чтение количества столбцов и строк из файла"""
        try:
            column = data[DataElement.number_columns][DataElement.value]
            row = data[DataElement.number_rows][DataElement.value]
            return column, row
        except Exception as err_data_read:
            print(err_data_read,
                  "\nОшибка при инициализации файла. Количество столбцов и строк установлено по умолчанию")
            column = 7
            row = 6
            return column, row

    @property
    def get_number_columns(self):
        """Получить количество колонок"""
        return self._NUMBER_COLUMNS

    @property
    def get_number_rows(self):
        """Получить количество колонок"""
        return self._NUMBER_ROWS

    def reset_game(self):
        """Пересоздание игры"""
        self.__init__()

    def next_turn(self, player_turn):
        """Передача хода другому игроку"""
        self.__player_now = player_turn % 2 + 1

    @property
    def player_now(self):
        """Возврат ходящего сейчас игрока"""
        return self.__player_now

    @property
    def game_over(self):
        """Конец игры"""
        return self.__game_over

    @game_over.setter
    def game_over(self, value):
        """Установка значения конца игры"""
        self.__game_over = value

    def show_result_game(self):
        """Показ результата игры"""
        is_game_over = self.game_over
        if is_game_over:
            self.__need_show_result_game = False

    @property
    def need_show_result_game(self):
        """Проверка на необходимость показа результата игры"""
        return self.__need_show_result_game

    @need_show_result_game.setter
    def need_show_result_game(self, value):
        """Установка значения для необходимости показа результата игры"""
        self.__need_show_result_game = value

    @property
    def winner(self):
        """Возврат победителя"""
        return self.__winner

    @winner.setter
    def winner(self, value):
        """Установка победителя"""
        self.__winner = value

    def change_color_token(self, value=WHITE, player=0):
        """Изменение цвета токена для игрока"""
        self.__players[player][DataElement.color] = value

    def change_sub_color_token(self, value=WHITE, player=0):
        """Изменение цвета токена для игрока"""
        self.__players[player][DataElement.sub_color] = value

    @property
    def players(self):
        """Возврат игроков"""
        return self.__players

    @players.setter
    def players(self, value):
        """Установка игроков"""
        self.__players = value


class ScreenPygame(Game):
    """Класс создания окна на pygame"""
    def __init__(self, column=None, row=None, multiply_number=None):
        """Инициализация"""

        # Коэффициент для динамического разрешения
        super().__init__(column, row)
        if multiply_number is None:
            self.__MULTIPLY_NUMBER = self.__get_data()
        else:
            self.__MULTIPLY_NUMBER = multiply_number

        self.__SCREEN_WIDTH = self.get_number_columns * self.__MULTIPLY_NUMBER

        # На самом деле высота больше на self.__MULTIPLY_NUMBER * 3 / 2
        self.__SCREEN_HEIGHT = self.get_number_rows * self.__MULTIPLY_NUMBER

        # Создание экрана
        self.__scr = self.create_pygame_screen()
        try:
            self.__sound_token = pygame.mixer.Sound("audio/token.mp3")
            self.__sound_token.set_volume(0.3)
        except Exception as err_sound:
            print(err_sound, "\nОшибка при инициализации файла звука")
        self.__FPS = 10
        self.__button_restart = pygame.rect.Rect(0, 0,
                                                 self.__SCREEN_WIDTH / self.get_number_columns,
                                                 self.__MULTIPLY_NUMBER / 2)

        # Лист активных колонок для нажатия на них
        self.__rectangles_list = [
            [pygame.rect.Rect(self.__SCREEN_WIDTH / self.get_number_columns * column, self.__MULTIPLY_NUMBER * 3 / 2,
                              self.__SCREEN_WIDTH / self.get_number_columns,
                              self.__SCREEN_HEIGHT)] for column in range(self.get_number_columns)]

        self.__clock = pygame.time.Clock()
        self.__f1 = pygame.font.Font(None, int(self.__MULTIPLY_NUMBER / 3))
        self.__text = "game is running"
        self.__text_button = "reset"
        self.__render_font = self.__f1.render(self.__text, False, BLACK)
        self.__render_font_button = self.__f1.render(self.__text_button, False, BLACK)

    @staticmethod
    def __get_data():
        """Чтение коэффициента динамического разрешения из файла"""
        try:
            multiply_number = data[DataElement.multiply_number][DataElement.value]
            return multiply_number
        except Exception as err_data_read:
            print(err_data_read,
                  "\nОшибка при инициализации файла. Коэффициент динамического разрешения установлен по умолчанию")
            multiply_number = 120
            return multiply_number

    @property
    def sound_token(self):
        """Возврат звука падения фишки"""
        return self.__sound_token

    @property
    def button_restart(self):
        """Возврат созданной кнопки"""
        return self.__button_restart

    @property
    def scr(self):
        """Возврат созданного экрана"""
        return self.__scr

    @property
    def rectangles_list(self):
        """Возврат листа столбцов активных зон"""
        return self.__rectangles_list

    @property
    def multiply_number(self):
        """Возврат числа умножения для динамического разрешения экрана"""
        return self.__MULTIPLY_NUMBER

    @property
    def screen_width(self):
        """Возврат ширины экрана"""
        return self.__SCREEN_WIDTH

    @property
    def screen_height(self):
        """Возврат высоты экрана"""
        return self.__SCREEN_HEIGHT

    def create_pygame_screen(self):
        """Создание игрового окна"""
        pygame.init()
        return pygame.display.set_mode((self.__SCREEN_WIDTH, self.__SCREEN_HEIGHT + self.__MULTIPLY_NUMBER * 3 / 2))

    def settings(self):
        """Настройки для окна"""
        pygame.display.set_caption("Four in a row")
        try:
            pygame.mixer.music.load("audio/Rocky_Balboa.mp3")
            pygame.mixer.music.play(-1)  # Бесконечное проигрывание музыки
            pygame.mixer.music.set_volume(0.05)
        except Exception as err_music:
            print(err_music, "\nОшибка при инициализации файла музыки")
        self.__scr.fill(BLUE)
        self.__clock.tick(self.__FPS)

    @property
    def render_font(self):
        """Возврат текста статуса игры"""
        return self.__render_font

    @render_font.setter
    def render_font(self, value):
        """Установка текста статуса игры"""
        self.__render_font = self.__f1.render(value, False, BLACK)

    def show_text(self):
        """Показ текста на экран"""
        self.__scr.blit(self.__render_font,
                        (self.__SCREEN_WIDTH - self.__MULTIPLY_NUMBER * 2, self.__MULTIPLY_NUMBER / 10))

    def show_button_text(self):
        """Показ текста кнопки на экран"""
        self.__scr.blit(self.__render_font_button,
                        (self.__MULTIPLY_NUMBER / 8, self.__MULTIPLY_NUMBER / 10))


class DrawBoard(ScreenPygame):
    """Отрисовка игровой доски"""

    def __init__(self, column=None, row=None, multiply_number=None):
        """Инициализация"""
        super().__init__(column, row, multiply_number)

    def draw_token(self, column, row, player=0):
        """Отрисовка поставленной фишки"""

        # Лист с отступом радиуса и цветом
        colors_and_radiuses = [[self.multiply_number / 30, CYAN],
                               [self.multiply_number / 10, self.players[player][DataElement.color]],
                               [self.multiply_number / 5, self.players[player][DataElement.sub_color]]]

        # Центр будущей фишки
        center_x = self.screen_width / self.get_number_columns * column + (
                self.screen_width / self.get_number_columns) / 2
        center_y = self.screen_height / self.get_number_rows * row + (
                          self.screen_height / self.get_number_rows) / 2 + self.multiply_number * 3 / 2
        center = (center_x, center_y)

        # Отрисовка поставленной фишки
        for index in range(len(colors_and_radiuses)):
            color = colors_and_radiuses[index][DataElement.color]
            radius = self.multiply_number / 2 - colors_and_radiuses[index][DataElement.radius]
            pygame.display.update(pygame.draw.circle(self.scr, color, center, radius))

    def draw_board(self):
        """Отрисовка игровой доски"""
        for column in range(self.get_number_columns):
            for row in range(self.get_number_rows):
                self.draw_token(column, row)

    def draw_button_restart(self):
        """Отрисовка кнопки рестарта"""
        pygame.display.update(pygame.draw.rect(self.scr, RED, self.button_restart))
        pygame.display.update(pygame.draw.rect(self.scr, BLACK, self.button_restart,
                                               int(self.multiply_number / 10)))

    def draw_token_player_before_turn(self, column, player=0):
        """Отрисовка фишки над столбцом при движении мыши"""
        center_x = self.screen_width / self.get_number_columns * column + (
                self.screen_width / self.get_number_columns) / 2
        center_y = self.multiply_number
        center = (center_x, center_y)
        pygame.display.update(pygame.draw.circle(self.scr, self.players[player][DataElement.color],
                                                 center, self.multiply_number * 0.45))
        pygame.display.update(pygame.draw.circle(self.scr, self.players[player][DataElement.sub_color],
                                                 center, self.multiply_number * 0.3))

    def clear_draw_above(self):
        """Очистить поле над игровым полем"""
        pygame.display.update(pygame.draw.rect(self.scr, self.players[DataElement.empty_player][DataElement.color],
                                               pygame.rect.Rect(0,
                                                                0,
                                                                self.screen_width,
                                                                self.multiply_number * 3 / 2)))

    def clear_drawed_token_before_turn(self, column):
        """Очистка прошлых позиций фишки над столбцом при движении мыши"""
        pygame.display.update(pygame.draw.rect(self.scr, self.players[DataElement.empty_player][DataElement.color],
                                               pygame.rect.Rect(self.screen_width / self.get_number_columns * column,
                                                                self.multiply_number / 2,
                                                                self.screen_width,
                                                                self.multiply_number)))


class Board(DrawBoard):
    """Игровая доска"""

    def __init__(self, column=None, row=None, value_for_victory=None, multiply_number=None):
        """Инициализация"""
        super().__init__(column, row, multiply_number)
        self.__game_board = [[0 for _ in range(self._NUMBER_COLUMNS)] for _ in range(self._NUMBER_ROWS)]
        self.__number_turn = 0  # Ход по счёту
        if value_for_victory is None:
            self.__value_for_victory = self.__get_data()
        else:
            self.__value_for_victory = value_for_victory

    @staticmethod
    def __get_data():
        """Чтение количества очков для победы из файла"""
        try:
            value_for_victory = data[DataElement.value_for_victory][DataElement.value]
            return value_for_victory
        except Exception as err_data_read:
            print(err_data_read,
                  "\nОшибка при инициализации файла. Количество очков для победы установено по умолчанию")
            value_for_victory = 4
            return value_for_victory

    def reset_all(self):
        return

    def reset_game_board(self, column=None, row=None, value_for_victory=None, multiply_number=None):
        """Пересоздание игрового окна игры"""
        self.__init__(column, row, value_for_victory, multiply_number)

    def fill_column(self, column, player):
        """Заполнение столбца фишкой игрока, возврат координаты установленной фишки"""
        last_position = None
        for row in range(self._NUMBER_ROWS):
            if self.__game_board[row][column] == DataElement.empty_player:
                if row > 0:
                    self.__game_board[row - 1][column] = DataElement.empty_player
                    self.draw_token(column, row - 1)
                self.__game_board[row][column] = player
                self.draw_token(column, row, player)
                time.sleep(0.035)
                last_position = (row, column)
        return last_position

    def click_column(self, column, player):
        """Нажатие на столбец"""
        token_position = self.fill_column(column, player)
        if token_position is not None:
            self.processing_player_move(token_position, player)
        is_game_over = self.game_over
        if not is_game_over:
            self.check_draw()

    def processing_player_move(self, token_position, player):
        """Основная функция обработки хода"""
        self.__number_turn += 1
        self.check_win(token_position, player)
        self.next_turn(player)

    def check_win(self, token_position, player):
        """Проверка победы"""
        is_have_winner = self.check_player_victory(token_position, player)
        self.result(is_have_winner, str(player) + " player")

    def check_draw(self):
        """Проверка ничьи"""
        is_draw = self.__number_turn == self._NUMBER_ROWS * self._NUMBER_COLUMNS
        self.result(is_draw, "draw, no")

    def result(self, condition, msg):
        """Результата игры от условия"""
        if condition:
            self.need_show_result_game = True
            self.game_over = True
            self.winner = msg

    def calculate_amount(self, amount, x, y, player, coordinates):
        """Просчёт суммы и запоминание координат"""
        copy_coordinates = [x for x in coordinates]
        if self.__game_board[x][y] == player:
            copy_coordinates.append([x, y])
            return amount + 1, copy_coordinates
        copy_coordinates.clear()
        return 0, copy_coordinates

    def check_horizontal_and_vertical(self, range_values, a, b, player):
        """Проверка горизонтали и диагонали на self.__value_for_victory (4 по стандарту) фишки"""
        amount = 0
        x, y = a, b
        coordinates = []
        for i in range(range_values):
            x, y, it_was_none = self.from_none_to_i(x, y, i)
            amount, coordinates = self.calculate_amount(amount, x, y, player, coordinates)
            if amount == self.__value_for_victory:
                self.change_color_token(GREEN)
                self.change_sub_color_token(self.players[player][DataElement.sub_color])
                for point in range(len(coordinates)):
                    self.draw_token(coordinates[point][1], coordinates[point][0])
                self.change_color_token()
                self.change_sub_color_token()
                return True
            x, y = self.from_i_to_none(x, y, it_was_none)

    @staticmethod
    def from_none_to_i(x, y, i):
        """Перевод из None в значение цикла"""
        if x is None:
            return i, y, 0
        return x, i, 1

    @staticmethod
    def from_i_to_none(x, y, it_was_none):
        """Перевести обратно в None"""
        if it_was_none == 0:
            return None, y
        return x, None

    def prepare_for_back_diagonal(self, x, y):
        """Нахождение стартовых координат для прохода по побочной диагонали"""
        a = x
        b = y
        while b < self.get_number_columns - 1 and a > 0:
            a, b = a - 1, b + 1
        return a, b

    def check_player_victory(self, pos, player):
        """Проверка победы игрока после хода"""

        # Проверка горизонтали
        is_horizontal_full = self.check_horizontal_and_vertical(self._NUMBER_COLUMNS, pos[0], None, player)
        if is_horizontal_full:
            return True

        # Проверка вертикали
        is_vertical_full = self.check_horizontal_and_vertical(self._NUMBER_ROWS, None, pos[1], player)
        if is_vertical_full:
            return True

        # Проверка главной диагонали
        is_diagonal_full = self.check_diagonal(pos, player)
        if is_diagonal_full:
            return True

        # Проверка побочной диагонали
        is_back_diagonal_full = self.check_back_diagonal(pos, player)
        if is_back_diagonal_full:
            return True

    def check_diagonal(self, pos, player):
        """Проверка диагонали на self.__value_for_victory (4 по стандарту) фишки"""
        amount = 0
        coordinates = []
        for i in range(self._NUMBER_ROWS):
            amount, coordinates = self.calculate_amount(amount, pos[0] - min(pos) + i, pos[1] - min(pos) + i, player,
                                                        coordinates)
            if amount == self.__value_for_victory:
                self.change_color_token(GREEN)
                self.change_sub_color_token(self.players[player][DataElement.sub_color])
                for point in range(len(coordinates)):
                    self.draw_token(coordinates[point][1], coordinates[point][0])
                self.change_color_token()
                self.change_sub_color_token()
                return True
            if not (pos[0] - min(pos) + i < self._NUMBER_ROWS - 1 and pos[1] - min(
                    pos) + i < self._NUMBER_COLUMNS - 1):
                break

    def check_back_diagonal(self, pos, player):
        """Проверка побочной диагонали на self.__value_for_victory (4 по стандарту) фишки"""
        amount = 0
        coordinates = []
        x, y = self.prepare_for_back_diagonal(pos[0], pos[1])
        for i in range(self._NUMBER_ROWS):
            amount, coordinates = self.calculate_amount(amount, x + i, y - i, player, coordinates)
            if amount == self.__value_for_victory:
                self.change_color_token(GREEN)
                self.change_sub_color_token(self.players[player][DataElement.sub_color])
                for point in range(len(coordinates)):
                    self.draw_token(coordinates[point][1], coordinates[point][0])
                self.change_color_token()
                self.change_sub_color_token()
                return True
            if not (x + i + 1 < self._NUMBER_ROWS and y - i > 0):
                break


# Создание экземлпяров классов
try:
    board = Board()
except Exception as err:
    board = Board(7, 6, 4, 120)
    print(err, "Ошибка при инициализации атрибутов. Установлены значения по умолчанию")


def main():
    """Выполнение программы"""
    global data

    # Настройка экрана
    board.settings()

    # Отрисовка "решета" игровой доски путём отрисовки пустых фишек, а не наоборот
    board.draw_board()

    # Заливка цветом пространства над игровым полем
    board.clear_draw_above()

    # Отрисовка кнопки рестарта
    board.draw_button_restart()

    # Отрисовка текста кнопки рестарта
    board.show_button_text()

    # Отрисовка текста статуса игры
    board.show_text()

    # Выполнение отрисовки каждым кадром, пока running == True
    running = True

    # Основной цикл отрисовки
    while running:

        # Показ результата игры
        need_show_game_result = board.need_show_result_game
        if need_show_game_result:
            board.clear_draw_above()
            board.show_result_game()
            board.render_font = str(board.winner) + " winner"
            board.show_text()
            board.draw_button_restart()
            board.show_button_text()

        # Отлов событий
        for event in pygame.event.get():

            # Закрытие окна нажатием на крестик
            if event.type == pygame.QUIT:
                running = False

            # Считывание с клавиатуры пользователя
            if event.type == KEYDOWN:
                # Закрытие окна нажатием ESC или Q
                if event.key == K_ESCAPE or event.key == K_q:
                    running = False
                # Перезапуск игры нажатием R
                if event.key == K_r:
                    data = data_read_from_file()
                    try:
                        board.reset_game_board()
                    except Exception as err_reset:
                        board.reset_game_board(7, 6, 4, 120)
                        print(err_reset, "Ошибка при инициализации атрибутов. Установлены значения по умолчанию")
                    main()

            # Нажатие на кнопку мыши
            if event.type == pygame.MOUSEBUTTONDOWN:
                # Событие связанное с левой кнопкой мыши
                if event.button == DataElement.LMB:
                    is_game_over = board.game_over
                    if not is_game_over:
                        for rect in range(len(board.rectangles_list)):
                            if board.rectangles_list[rect][DataElement.rectangle].collidepoint(event.pos):

                                # Нажатие на определённый столбец игроком
                                board.click_column(rect, board.player_now)

                                try:
                                    board.sound_token.play()
                                except AttributeError as err_attribute:
                                    print(err_attribute, "\nНевозможно воспроизвести звук")

                                # Переотрисовка фишки над столбцом
                                board.draw_token_player_before_turn(rect, board.player_now)

                    # Рестарт игры по нажатию на кнопку рестарта
                    if board.button_restart.collidepoint(event.pos):
                        data = data_read_from_file()
                        try:
                            board.reset_game_board()
                        except Exception as err_reset:
                            board.reset_game_board(7, 6, 4, 120)
                            print(err_reset, "Ошибка при инициализации атрибутов. Установлены значения по умолчанию")
                        main()

            # Передвижение мыши
            if event.type == pygame.MOUSEMOTION:
                is_game_over = board.game_over
                if not is_game_over:
                    for rect in range(len(board.rectangles_list)):
                        # Отрисовка фишки над наведённым мышью столбцом, иначе очистка
                        if board.rectangles_list[rect][DataElement.rectangle].collidepoint(event.pos):
                            board.draw_token_player_before_turn(rect, board.player_now)
                        else:
                            board.clear_drawed_token_before_turn(rect)

        # Обновление экран
        pygame.display.update()

    # Выход из pygame при окончании основного цикла
    pygame.quit()
    exit()


# Выполнение программы
main()
