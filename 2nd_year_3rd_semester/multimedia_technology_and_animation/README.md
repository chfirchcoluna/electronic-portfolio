<h1 align="center">Multimedia Technology and Animation</h1>

### About subject:
<p align="left">
Goals: to form a set of theoretical knowledge and practical skills sufficient to independently develop interactive content of modern visual media.
To form a set of theoretical knowledge and practical skills sufficient for independent development of interactive content of modern visual media.
Objectives:
- Formation of the students of the discipline of the idea of the possibilities of
multimedia technologies and animation, their functional purpose and role in the interactive visual content.
interactive visual content, advertising, game and educational materials;
- study of tools for creating multimedia objects and animation,
means and ways of embedding them into a variety of content;
- study of tools and principles of building interactive visual content; study of tools and principles of creating interactive visual
content;
- formation of students' creative thinking, the ability to
independent problem solving, the ability to interactively use tool-technological means and to work effectively in a small team.
</p>

<h2></h2>

<h1 align="left"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester/multimedia_technology_and_animation/game_project">Game Project</a></h1>