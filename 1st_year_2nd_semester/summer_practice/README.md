<h1 align="center">Summer Practice</h1>

### About subject:
<p align="left">
The main purpose of practice is the application of theoretical knowledge in practical activity and development of professional skills and abilities.
activity and development of professional skills and abilities in creation, implementation,
analyses and support of professionally oriented information technologies and information system shells,
technologies and information systems shells. 

Objectives:
- Formation of students' design thinking, which consists in the ability to
to use creative methods in solving engineering problems;
- development of understanding of social significance of professional activity through
motivation and aspiration for its fulfilment;
- development and consolidation of methods and techniques of self-study for improvement of professional qualification and mastery; development of social significance of professional activity through motivation and aspiration for its fulfilment.
to improve professional qualification and skills;
- study of methods and techniques of using modern information and communication technologies in professional activity;
- study of methods and techniques of solving standard problems of professional activity on the basis of information and bibliographical information
activity on the basis of information and bibliographic culture with the use of information and communication technologies and with the use of modern information and communication technologies in professional activity
application of information and communication technologies and taking into account
basic requirements of information security;
- Practical training and consolidation of the previously learnt methods and techniques of search and independent study of literature and electronic and electronic technologies
and independent study of literature and electronic educational resources
in the sphere of professional activity;
- Formation of skills and abilities to develop, operate and maintain
information systems and services of enterprises;
- formation of practical skills of participation in the realisation of professional
professional communications within project groups (teamwork);
- practical development of skills and abilities to prepare and conduct a presentation of project results and initial user training.
project results and initial training of users
information systems;

</p>

<h2></h2>

## [DEMONSTRATION OF SUMMER PRACTICE](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/1st_year_2nd_semester/summer_practice/summer_practice_1.pdf)