from itertools import permutations
from math import inf, sqrt, radians, sin, cos
import pygame
from pygame.locals import KEYDOWN
import time
import random


filename = input("Введите имя файла с расширением: ")
if filename == "":
    filename = "msm.txt"
massive = []
try:
    with open(filename, "r") as file:
        length = len(file.readline().split())
except FileNotFoundError:
    print("Файл не найден")
    exit()
with open(filename, "r") as file:
    while True:
        line = file.readline()
        if not line:
            break
        line = line.rstrip()
        massive.append(list(line.split()))
print("Введённый двумерный массив: ")
for i in range(0, length):
    for j in range(0, length):
        print(massive[i][j], ' ',  end='')
    print()
massive.remove(massive[0])
for i in range(length-1):
    massive[i].remove(massive[i][0])
length -= 1
all_summa = []
all_ways = []
counter = 0
end_point = ""
start_point = ""
str_way = ""
str_way_max = ""
list_d = []
list_b = []
edge = []


def program():
    global all_summa
    global counter
    global end_point
    global start_point
    global list_b
    global edge
    vertex = length
    list_a = []
    list_c = []
    summa = []
    summa_element = 0
    global list_d
    global str_way
    global str_way_max
    str_way = ""
    str_way_max = ""
    dict = {"A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "F": 6, "G": 7, "H": 8, "I": 9, "J": 10, "K": 11, "L": 12, "M": 13,
            "N": 14, "O": 15, "P": 16, "Q": 17, "R": 18, "S": 19, "T": 20, "U": 21, "V": 22, "W": 23, "X": 24, "Y": 25,
            "Z": 26}
    dict_r = {1: "A", 2: "B", 3: "C", 4: "D", 5: "E", 6: "F", 7: "G", 8: "H", 9: "I", 10: "J", 11: "K", 12: "L",
              13: "M",
              14: "N", 15: "O", 16: "P", 17: "Q", 18: "R", 19: "S", 20: "T", 21: "U", 22: "V", 23: "W", 24: "X",
              25: "Y",
              26: "Z"}
    try:
        if counter == 0:
            a = input("Пункт отправления(буква): ")
            b = input("Пункт назначения(буква): ")
            end_point = b
            start_point = a
            counter += 1
        elif counter <= length-2:
            a = str(dict_r[list_b[counter-1]])
            b = end_point
            counter += 1
        while vertex > 0:
            list_a.append(vertex)
            vertex -= 1
        if counter == 1:
            list_a.reverse()
            seq_2 = permutations(list_a, 2)
            for p in list(seq_2):
                list_d.append(list(p))
            list_a.reverse()
            for i in list_d:
                edge.append((massive[i[0]-1][i[1]-1]))
            #  print(edge)
        #  print(list_d)
        list_a.remove(int(dict[a]))
        if dict[a] != dict[b]:
            list_a.remove(int(dict[b]))
        if counter == 1:
            list_b = list_a
    except ValueError:
        print("Неверный ввод данных")
        exit()
    except KeyError:
        print("Неверный ввод данных")
        exit()
    if a == b:
        print("Пункт назначения равен пункту отправления")
        exit()
    for i in range(length):
        seq = permutations(list_a, i)
        for p in list(seq):
            list_c.append(list(p))
    res = list(map(lambda x: [int(dict[a])] + x + [int(dict[b])], list_c))
    for i in res:
        last = 1
        try:
            for j in range(0, len(i)):
                if float(eval(massive[i[j] - 1][i[j+1] - 1])) != 0:
                    summa_element += float(eval(massive[i[j] - 1][i[j+1] - 1]))
                else:
                    last = 0
                    summa_element = 0
            if summa_element != 0 and last != 0:
                summa.append(summa_element)
            else:
                summa.append(inf)
            summa_element = 0
        except IndexError:
            if summa_element != 0 and last != 0:
                summa.append(summa_element)
            else:
                summa.append(inf)
            summa_element = 0

    if dict[a] != dict[b] and min(summa) != inf:
        way = res[summa.index(min(summa))]
        for i in way:
            str_way += str(dict_r[i])
        if a == start_point:
            print("Самый короткий путь от", a, "до", b, "=", min(summa), "по маршруту:", str_way)
        else:
            all_summa.append(min(summa))
            all_ways.append(str_way)
            if counter <= length-2:
                program()
            else:
                #print(all_summa)
                print("Самый короткий путь это", all_ways[all_summa.index(min(all_summa))][0], "до",
                      all_ways[all_summa.index(min(all_summa))][-1], "=", min(all_summa), "по маршруту:",
                      all_ways[all_summa.index(min(all_summa))])
                print("Самый длинный путь это", all_ways[all_summa.index(max(all_summa))][0], "до",
                      all_ways[all_summa.index(max(all_summa))][-1], "=", max(all_summa), "по маршруту:",
                      all_ways[all_summa.index(max(all_summa))])
                #print(all_ways)
        for i in range(len(summa)):
            if summa[i] == inf:
                summa[i] = 0
        way = res[summa.index(max(summa))]
        for i in way:
            str_way_max += str(dict_r[i])
        if a == start_point:
            print("Самый длинный путь от", a, "до", b, "=", max(summa), "по маршруту:", str_way_max)
    elif counter <= length-2:
        program()
    else:
        print("В точку невозможно добраться никаким способом")


program()


points = []
start, end = 500, 500
list_x = []
#radius = [3, 4, 5, 3, 5, 4, 4, 5, 3, 5, 4, 3]
for i in range(len(massive[0])-1):
    points.append([start, end])
    #  print("ok")
for i in range(len(edge)):
    edge[i] = float(eval(edge[i]))
found_coordinate = [[start, end], [(start), (end - edge[0] * 20)]]


def coordinates():
    global list_x
    angle = 0
    list_x.clear()
    for i in range(len(points)):
        proxid = True
        list_x.append([])
        while proxid:
            angle_rad = radians(angle)
            pt_x = int(points[i][0] + edge[i] * 20 * sin(angle_rad))
            pt_y = int(points[i][1] - edge[i] * 20 * cos(angle_rad))
            list_x[i].append([pt_x, pt_y])
            angle += 1
            if angle >= 360:
                proxid = False
                angle = 0
        #  print(list_x[i])


coordinates()
broken = False
#print(list_x)
found = False
for nomer in range(1, len(points)):
    flag = True
    for i in list_x[nomer]:
        try:
            # print("f_c", found_coordinate)
            # print(i, found_coordinate[nomer], nomer, radius[wayes.index([2 + nomer - 1, 2 + nomer])]*10)
            if broken == False and flag == True and sqrt((i[0] - found_coordinate[nomer][0]) ** 2 +
                                                         (i[1] - found_coordinate[nomer][1]) ** 2) == \
                    edge[list_d.index([2 + nomer - 1, 2 + nomer])] * 20:
                nomer_2 = 2 + nomer
                #print(found_coordinate, "f")
                while 2 + nomer - 1 != 2:
                    #print(nomer, nomer_2)
                    for j in list_x[nomer]:
                        if flag == True and sqrt(
                                (j[0] - found_coordinate[nomer-1][0]) ** 2 +
                                (j[1] - found_coordinate[nomer-1][1]) ** 2) == \
                                edge[list_d.index([nomer, nomer_2])] * 20:
                            # print(j, found_coordinate[nomer-1],  edge[list_d.index([nomer, nomer_2])], nomer, nomer_2)
                            found = True
                            found_coordinate.append([int(j[0]), int(j[1])])
                    nomer -= 1
                if not found:
                    found_coordinate.append([int(i[0]), int(i[1])])
                else:
                    found = False
                flag = False
        except IndexError:
            print("Невозможно нарисовать граф, возможно он ориентированный или вес(а) указаны неверно")
            print("Тогда возьмём произвольные координаты:")
            broken = True


# print(found_coordinate)
SCREEN_WIDTH = 1000
SCREEN_HEIGHT = 1000
if not broken:
    points = found_coordinate
    print("Координаты:")
    print(points)
else:
    randy = 50
    points.clear()
    for i in range(len(massive[0])):
        #  print(SCREEN_WIDTH/int(len(massive[0])))
        randy += int((SCREEN_WIDTH/int(len(massive[0]))))-20
        points.append([randy, random.randint(50, 950)])
    # points = [(50, 40), (180, 300), (320, 120), (450, 600), (650, 200), (750, 750), (900, 700), (950, 900)]
    print(points)
print("Зёлёным будет вырисовываться самый длинный путь, голубым - минимальный")
name_points = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
               "U", "W", "X", "Y"]
pygame.display.set_caption("Graph")
list_p = []
list_e = []
pygame.init()
list_ways = []
list_ways_max = []
for i in range(len(str_way)-1):
    list_ways.append([points[name_points.index(str_way[i])], points[name_points.index(str_way[i+1])]])
for i in range(len(str_way_max)-1):
    list_ways_max.append([points[name_points.index(str_way_max[i])], points[name_points.index(str_way_max[i+1])]])
screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])
clock = pygame.time.Clock()
seq_3 = permutations(points, 2)
for p in list(seq_3):
    list_p.append(p)
draw = False


def drawing():
    for i in range(len(list_p)):
        if (edge[list_p.index(list_p[i])]) != 0:
            if list_d[i][0] < list_d[i][1]:
                if abs(list_p[i][0][0] - list_p[i][1][0]) > 50:
                    pygame.draw.line(screen, "black", ((list_p[i][0][0] + list_p[i][1][0] - 20) / 2,
                                                       (list_p[i][0][1] + list_p[i][1][1] - 20) / 2),
                                     ((list_p[i][0][0] + list_p[i][1][0] + 20) / 2,
                                      (list_p[i][0][1] + list_p[i][1][1] - 20) / 2), 5)
                    pygame.draw.circle(screen, "black", ((list_p[i][0][0] + list_p[i][1][0] + 20) / 2,
                                                         (list_p[i][0][1] + list_p[i][1][1] - 20) / 2), 10, 0)
                else:
                    pygame.draw.line(screen, "black", ((list_p[i][0][0] + list_p[i][1][0] + 40) / 2,
                                                       (list_p[i][0][1] + list_p[i][1][1] - 20) / 2),
                                     ((list_p[i][0][0] + list_p[i][1][0] + 40) / 2,
                                      (list_p[i][0][1] + list_p[i][1][1] + 20) / 2), 5)
                    pygame.draw.circle(screen, "black", ((list_p[i][0][0] + list_p[i][1][0] + 40) / 2,
                                                         (list_p[i][0][1] + list_p[i][1][1] + 20) / 2), 10, 0)
            if list_d[i][0] > list_d[i][1]:
                if abs(list_p[i][0][0] - list_p[i][1][0]) > 50:
                    pygame.draw.line(screen, "red", ((list_p[i][0][0] + list_p[i][1][0] - 20) / 2,
                                                     (list_p[i][0][1] + list_p[i][1][1] + 20) / 2),
                                     ((list_p[i][0][0] + list_p[i][1][0] + 20) / 2,
                                      (list_p[i][0][1] + list_p[i][1][1] + 20) / 2), 5)
                    pygame.draw.circle(screen, "red", ((list_p[i][0][0] + list_p[i][1][0] - 20) / 2,
                                                       (list_p[i][0][1] + list_p[i][1][1] + 20) / 2), 10, 0)
                else:
                    pygame.draw.line(screen, "red", ((list_p[i][0][0] + list_p[i][1][0] - 30) / 2,
                                                     (list_p[i][0][1] + list_p[i][1][1] - 20) / 2),
                                     ((list_p[i][0][0] + list_p[i][1][0] - 30) / 2,
                                      (list_p[i][0][1] + list_p[i][1][1] + 20) / 2), 5)
                    pygame.draw.circle(screen, "red", ((list_p[i][0][0] + list_p[i][1][0] - 30) / 2,
                                                       (list_p[i][0][1] + list_p[i][1][1] - 20) / 2), 10, 0)
                f = pygame.font.Font(None, 36)
                text = f.render(str(int(edge[list_p.index(list_p[i])])), False, "red")
                screen.blit(text, ((abs(list_p[i][1][0]) + abs(list_p[i][0][0])) / 2,
                                   (abs(list_p[i][1][1]) + abs(list_p[i][0][1])) / 2 - 20))
    for i in range(len(list_p)):
         if (edge[list_p.index(list_p[i])]) != 0:
            if list_d[i][0] < list_d[i][1]:
                f = pygame.font.Font(None, 36)
                text = f.render(str(int(edge[list_p.index(list_p[i])])), False, "black")
                screen.blit(text, (((abs(list_p[i][1][0]) + abs(list_p[i][0][0])) - 20) / 2,
                                   (abs(list_p[i][1][1]) + abs(list_p[i][0][1]) + 20) / 2))
    for i in range(len(list_p) - 1):
        f = pygame.font.Font(None, 72)
        text = f.render(name_points[points.index(list_p[i][0])], False, "orange")
        screen.blit(text, list_p[i][0])


while True:
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            exit()
    if not draw:
        screen.fill("white")
        for i in range(len(list_p)):
            if (edge[list_p.index(list_p[i])]) != 0:
                if list_d[i][0] < list_d[i][1]:
                    pygame.draw.line(screen, "black", (list_p[i][0][0]+5, list_p[i][0][1]+5), (list_p[i][1][0]+5,
                                                                                             list_p[i][1][1]+5), 8)
                if list_d[i][0] > list_d[i][1]:
                    pygame.draw.line(screen, "blue", list_p[i][0], list_p[i][1], 8)
        drawing()
        time.sleep(1)
        for i in range(len(list_ways_max)):
            pygame.draw.line(screen, "green", list_ways_max[i][0], list_ways_max[i][1], 8)
            time.sleep(1)
            pygame.display.flip()
        pygame.draw.circle(screen, "green", list_ways_max[i][-1], 20)
        pygame.display.flip()
        for i in range(len(list_ways)):
            pygame.draw.line(screen, "cyan", list_ways[i][0], list_ways[i][1], 8)
            time.sleep(1)
            pygame.display.flip()
        pygame.draw.circle(screen, "cyan", list_ways[i][-1], 10)
        pygame.display.flip()
        draw = True
    drawing()
    pygame.display.flip()
    clock.tick(1)
