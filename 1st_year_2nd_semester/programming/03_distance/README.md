<h1 align="center">Distance</h1>

There are settlements with names A, B, C, D, E, F, G, H. The roads between them are one-way and of different lengths. In the attached file the distances between the settlements are given by a matrix. The main diagonal of the matrix is given by zeros. On the left are the points of departure and on the top are the points of destination. Thus, the distance from A to B is 13 km and from B to A is 25 km. The value 0 means that there is no direct route in this direction between these settlements (for example, from E to C).

Task: find the shortest distance between the settlements given by the user, output it and the route.

Complication (+1 point): if the path does not exist, find the nearest point, output the distance to it and the route. The nearest point is a point that is connected to the destination, but the direction of the connection is reversed.

Challenge 2 (+1 point): visualise the problem using PyGame or Tk.

Complication 2.1 (+1 point): add animation.

Challenge 2.2 (+1 point): keep scale when visualising.

## [Distance.py](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/1st_year_2nd_semester/programming/03_distance/src/distance.py)

# Demonstration:

![1](images/1.png "1")
![2](images/2.png "2")
