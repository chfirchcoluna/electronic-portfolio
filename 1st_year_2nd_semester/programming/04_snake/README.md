<h1 align="center">Snake</h1>

The field must be at least 100x100. The snake should eat apples with sound and grow a tail. When colliding with a wall, a sound signal and a message (with the help of Tk library) about the end of the game should be given out. During the game there should be unobtrusive background music. The rules of the game should be formatted as a help in the menu at the top of the window (as an option, you can design a beautiful game menu using PyGame tools).
* second snake with AI (conditional)

## [Snake.py](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/1st_year_2nd_semester/programming/04_snake/src/snake.py)

# Demonstration:

![1](images/1.png "1")
![2](images/2.png "2")
![3](images/3.png "3")
![4](images/4.png "4")
![5](images/5.png "5")
![6](images/6.png "6")
![7](images/7.png "7")
![8](images/8.png "8")
![9](images/9.png "9")
![10](images/10.png "10")
![11](images/11.png "11")
![12](images/12.png "12")
![13](images/13.png "13")
![14](images/14.png "14")
![15](images/15.png "15")
![16](images/16.png "16")
![17](images/17.png "17")
![18](images/18.png "18")
![19](images/19.png "19")
![20](images/20.png "20")
![21](images/21.png "21")
![22](images/22.png "22")
![23](images/23.png "23")