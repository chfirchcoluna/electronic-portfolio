import pygame
from pygame.locals import (KEYDOWN, K_ESCAPE, K_UP, K_DOWN, K_LEFT, K_RIGHT, K_q)
import random
import pygame_menu


pygame.init()
SCREEN_WIDTH = 1000  # (изменяемое)
SCREEN_HEIGHT = 1000  # (изменяемое)
q = 100  # количество ячеек по высоте и ширине (изменяемое)
screen = pygame.display.set_mode([SCREEN_WIDTH + 1, SCREEN_HEIGHT + 1])
game_map = []
score = 4
score_enemy = 4
last = []
enemy_last = []
button = 0
score_last = 1
speed = 20
pygame.init()
pygame.mixer.init()
tuturu = pygame.mixer.Sound("sounds/tuturu_1.mp3")
tuturu.set_volume(0.2)
death = pygame.mixer.Sound("sounds/death.mp3")
length = 4  # длина начальной змейки (изменяемое)
death_count = False


def death_window():
    global death_count
    pygame.mixer.music.pause()
    death_count = True
    death.play(0)
    message_box()


def move_update(pressed_keys):
    global last
    global score
    global button
    global length
    if (pressed_keys[K_UP]) and not pressed_keys[K_LEFT] and button != 1:
        button = 0
    if (pressed_keys[K_DOWN]) and not pressed_keys[K_RIGHT] and button != 0:
        button = 1
    if (pressed_keys[K_RIGHT]) and not pressed_keys[K_UP] and button != 3:
        button = 2
    if (pressed_keys[K_LEFT]) and not pressed_keys[K_DOWN] and button != 2:
        button = 3
    if button == 0:
        last = [here_you_are[0][0], here_you_are[0][1]]
        here_you_are.insert(1, ([last[0], last[1]]))
        while len(here_you_are) > score+length:
            here_you_are.pop()
        if here_you_are[0][0] > 0:
            here_you_are[0][0] -= 1
        else:
             here_you_are[0][0] = q - 1
        game_map[here_you_are[0][0]][here_you_are[0][1]] = 1
    elif button == 1:
        last = [here_you_are[0][0], here_you_are[0][1]]
        here_you_are.insert(1, ([last[0], last[1]]))
        while len(here_you_are) > score+length:
            here_you_are.pop()
        if here_you_are[0][0] < (q - 1):
            here_you_are[0][0] += 1
        else:
             here_you_are[0][0] = 0
        game_map[here_you_are[0][0]][here_you_are[0][1]] = 1
    elif button == 2:
        last = [here_you_are[0][0], here_you_are[0][1]]
        here_you_are.insert(1, ([last[0], last[1]]))
        while len(here_you_are) > score+length:
            here_you_are.pop()
        if here_you_are[0][1] < (q - 1):
            here_you_are[0][1] += 1
        else:
             here_you_are[0][1] = 0
        game_map[here_you_are[0][0]][here_you_are[0][1]] = 1
    elif button == 3:
        last = [here_you_are[0][0], here_you_are[0][1]]
        here_you_are.insert(1, ([last[0], last[1]]))
        while len(here_you_are) > score+length:
            here_you_are.pop()
        if here_you_are[0][1] > 0:
            here_you_are[0][1] -= 1
        else:
             here_you_are[0][1] = q - 1
        game_map[here_you_are[0][0]][here_you_are[0][1]] = 1


duration = 4
cicle = 0
death_enemy = False
eat = False
copy_list = []


def enemy_update():
    global here_enemy_are
    global apple_i
    global apple_j
    global enemy_last
    global speed
    global q
    global death_enemy
    global eat
    global copy_list
    if death_enemy:
        if not eat:
            for i in here_enemy_are:
                copy_list.append(i)
            for i in range(len(here_enemy_are)):
                game_map[here_enemy_are[i][0]][here_enemy_are[i][1]] = 9
                pygame.draw.rect(screen, (0, 255, 0), pygame.Rect(here_enemy_are[i][1] * SCREEN_WIDTH // q + 1,
                                                                here_enemy_are[i][0] * SCREEN_HEIGHT // q + 1,
                                                                SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
            while len(here_enemy_are) > 1:
                here_enemy_are.pop()
            here_enemy_are[0] = [q, q]
            eat = True
        return 0
    enemy_last = [here_enemy_are[0][0], here_enemy_are[0][1]]
    here_enemy_are.insert(1, ([enemy_last[0], enemy_last[1]]))
    while len(here_enemy_are) > score_enemy+1:
        here_enemy_are.pop()

    def repeat():
        global speed
        global death_enemy
        global apple_i
        global apple_j
        global here_you_are
        try:
            draw_a = True
            if here_enemy_are[0][0] < 0:
                draw_a = False
                here_enemy_are[0][0] = q - 2
                here_enemy_are[1][0] = q - 1
                game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                game_map[here_enemy_are[1][0]][here_enemy_are[1][1]] = 2
                here_enemy_are.insert(0, ([here_enemy_are[0][0], here_enemy_are[0][1]]))
            elif here_enemy_are[0][1] < 0:
                draw_a = False
                here_enemy_are[0][1] = q - 2
                here_enemy_are[1][1] = q - 1
                game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                game_map[here_enemy_are[1][0]][here_enemy_are[1][1]] = 2
                here_enemy_are.insert(0, ([here_enemy_are[0][0], here_enemy_are[0][1]]))
            if not draw_a:
                here_enemy_are.pop(-1)
                for i in range(0, len(game_map[0])):
                    for j in range(0, len(game_map[0])):
                        pygame.draw.rect(screen, (0, 0, 0),
                                         pygame.Rect(j * SCREEN_WIDTH // q + 1, i * SCREEN_HEIGHT // q + 1,
                                                     SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
                pygame.draw.rect(screen, (0, 255, 0),
                                 pygame.Rect(apple_j * SCREEN_WIDTH // q + 1, apple_i * SCREEN_HEIGHT // q + 1,
                                             SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
                draw_a = True
            global duration
            global cicle
            if apple_j < here_enemy_are[0][1] and duration != 1\
                    and [here_enemy_are[0][0], here_enemy_are[0][1] - 1] not in here_enemy_are \
                    and [here_enemy_are[0][0], here_enemy_are[0][1] - 1] not in here_you_are:
                here_enemy_are[0][1] -= 1
                duration = 0
                game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
            elif apple_j > here_enemy_are[0][1] and duration != 0\
                    and [here_enemy_are[0][0], here_enemy_are[0][1] + 1] not in here_enemy_are \
                    and [here_enemy_are[0][0], here_enemy_are[0][1] + 1] not in here_you_are:
                here_enemy_are[0][1] += 1
                duration = 1
                game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
            elif apple_i > here_enemy_are[0][0] and duration != 3\
                    and [here_enemy_are[0][0] + 1, here_enemy_are[0][1]] not in here_enemy_are \
                    and [here_enemy_are[0][0] + 1, here_enemy_are[0][1]] not in here_you_are:
                here_enemy_are[0][0] += 1
                duration = 2
                game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
            elif apple_i < here_enemy_are[0][0] and duration != 2\
                    and [here_enemy_are[0][0] - 1, here_enemy_are[0][1]] not in here_enemy_are \
                    and [here_enemy_are[0][0] - 1, here_enemy_are[0][1]] not in here_you_are:
                here_enemy_are[0][0] -= 1
                duration = 3
                game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
            else:
                if duration == 0:
                    if [here_enemy_are[0][0] - 1, here_enemy_are[0][1]] not in here_enemy_are \
                            and [here_enemy_are[0][0] - 1, here_enemy_are[0][1]] not in here_you_are:
                        here_enemy_are[0][0] -= 1
                        duration = 3
                        game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                    elif [here_enemy_are[0][0] + 1, here_enemy_are[0][1]] not in here_enemy_are \
                            and [here_enemy_are[0][0] + 1, here_enemy_are[0][1]] not in here_you_are:
                        here_enemy_are[0][0] += 1
                        duration = 2
                        game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                    elif [here_enemy_are[0][0], here_enemy_are[0][1] + 1] not in here_enemy_are \
                            and [here_enemy_are[0][0], here_enemy_are[0][1] + 1] not in here_you_are:
                        here_enemy_are[0][1] += 1
                        duration = 1
                        game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                    elif cicle <= 5:
                        cicle += 1
                        repeat()
                    else:
                        cicle = 0
                        if [here_enemy_are[0][0], here_enemy_are[0][1] - 1] not in here_enemy_are \
                                and [here_enemy_are[0][0], here_enemy_are[0][1] - 1] not in here_you_are:
                            here_enemy_are[0][1] -= 1
                            duration = 0
                            game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                        else:
                            death_enemy = True
                elif duration == 1:
                    if [here_enemy_are[0][0] - 1, here_enemy_are[0][1]] not in here_enemy_are \
                            and [here_enemy_are[0][0] - 1, here_enemy_are[0][1]] not in here_you_are:
                        here_enemy_are[0][0] -= 1
                        duration = 3
                        game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                    elif [here_enemy_are[0][0] + 1, here_enemy_are[0][1]] not in here_enemy_are \
                            and [here_enemy_are[0][0] + 1, here_enemy_are[0][1]] not in here_you_are:
                        here_enemy_are[0][0] += 1
                        duration = 2
                        game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                    elif [here_enemy_are[0][0], here_enemy_are[0][1] - 1] not in here_enemy_are \
                            and [here_enemy_are[0][0], here_enemy_are[0][1] - 1] not in here_you_are:
                        here_enemy_are[0][1] -= 1
                        duration = 0
                        game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                    elif cicle <= 5:
                        cicle += 1
                        repeat()
                    else:
                        cicle = 0
                        if [here_enemy_are[0][0], here_enemy_are[0][1] + 1] not in here_enemy_are \
                                and [here_enemy_are[0][0], here_enemy_are[0][1] + 1] not in here_you_are:
                            here_enemy_are[0][1] += 1
                            duration = 1
                            game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                        else:
                            death_enemy = True
                elif duration == 2:
                    if [here_enemy_are[0][0], here_enemy_are[0][1] + 1] not in here_enemy_are \
                            and [here_enemy_are[0][0], here_enemy_are[0][1] + 1] not in here_you_are:
                        here_enemy_are[0][1] += 1
                        duration = 1
                        game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                    elif [here_enemy_are[0][0], here_enemy_are[0][1] - 1] not in here_enemy_are \
                            and [here_enemy_are[0][0], here_enemy_are[0][1] - 1] not in here_you_are:
                        here_enemy_are[0][1] -= 1
                        duration = 0
                        game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                    elif [here_enemy_are[0][0] - 1, here_enemy_are[0][1]] not in here_enemy_are \
                            and [here_enemy_are[0][0] - 1, here_enemy_are[0][1]] not in here_you_are:
                        here_enemy_are[0][0] -= 1
                        duration = 3
                        game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                    elif cicle <= 5:
                        cicle += 1
                        repeat()
                    else:
                        cicle = 0
                        if [here_enemy_are[0][0] + 1, here_enemy_are[0][1]] not in here_enemy_are \
                                and [here_enemy_are[0][0] + 1, here_enemy_are[0][1]] not in here_you_are:
                            here_enemy_are[0][0] += 1
                            duration = 2
                            game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                        else:
                            death_enemy = True
                elif duration == 3:
                    if [here_enemy_are[0][0], here_enemy_are[0][1] + 1] not in here_enemy_are \
                            and [here_enemy_are[0][0], here_enemy_are[0][1] + 1] not in here_you_are:
                        here_enemy_are[0][1] += 1
                        duration = 1
                        game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                    elif [here_enemy_are[0][0], here_enemy_are[0][1] - 1] not in here_enemy_are \
                            and [here_enemy_are[0][0], here_enemy_are[0][1] - 1] not in here_you_are:
                        here_enemy_are[0][1] -= 1
                        duration = 0
                        game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                    elif [here_enemy_are[0][0] + 1, here_enemy_are[0][1]] not in here_enemy_are \
                            and [here_enemy_are[0][0] + 1, here_enemy_are[0][1]] not in here_you_are:
                        here_enemy_are[0][0] += 1
                        duration = 2
                        game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                    elif cicle <= 5:
                        cicle += 1
                        repeat()
                    else:
                        cicle = 0
                        if [here_enemy_are[0][0] - 1, here_enemy_are[0][1]] not in here_enemy_are \
                                and [here_enemy_are[0][0] - 1, here_enemy_are[0][1]] not in here_you_are:
                            here_enemy_are[0][0] -= 1
                            duration = 3
                            game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                        else:
                            death_enemy = True
        except IndexError:
            if duration == 2:
                if here_enemy_are[0][0] < 0:
                    here_enemy_are[0][0] = q - 1
                    game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                if here_enemy_are[0][0] > (q - 1):
                    here_enemy_are[0][0] = 0
                    game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                if here_enemy_are[0][1] > (q - 1):
                    here_enemy_are[0][1] = 0
                    game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                if here_enemy_are[0][1] < 0:
                    here_enemy_are[0][1] = q - 1
                    game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
            if duration == 1:
                if here_enemy_are[0][0] < 0:
                    here_enemy_are[0][0] = q - 1
                    game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                if here_enemy_are[0][0] > (q - 1):
                    here_enemy_are[0][0] = 0
                    game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                if here_enemy_are[0][1] > (q - 1):
                    here_enemy_are[0][1] = 0
                    game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                if here_enemy_are[0][1] < 0:
                    here_enemy_are[0][1] = q - 1
                    game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
            if duration == 0:
                if here_enemy_are[0][0] > (q - 1):
                    here_enemy_are[0][1] = 0
                    game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                if here_enemy_are[0][0] < 0:
                    here_enemy_are[0][0] = q - 1
                    game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                if here_enemy_are[0][1] > (q - 1):
                    here_enemy_are[0][1] = 0
                    game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
                if here_enemy_are[0][1] < 0:
                    here_enemy_are[0][1] = q - 1
                    game_map[here_enemy_are[0][0]][here_enemy_are[0][1]] = 2
    repeat()


def efg():
    global running
    global screen_tkinter
    screen_tkinter.quit()
    screen_tkinter.destroy()
    running = False


def message_box():
    global mark
    global score
    global documentation
    global parameters
    global quantity
    global w_d
    global h_d
    global length_m
    global length
    global length_e
    global score_enemy
    global fps
    global speed
    global SCREEN_WIDTH
    global SCREEN_HEIGHT
    global death_count
    if (int(score) - 1) > (int(score_enemy) - int(length_e.get_value())):
        menu = pygame_menu.Menu('Смерть', SCREEN_WIDTH, SCREEN_HEIGHT, theme=pygame_menu.themes.THEME_ORANGE)
        menu.add.label('Вы набрали ' + str(int(score) - 1))
        menu.add.label('Враг набрал ' + str(int(score_enemy) - int(length_e.get_value())))
        menu.add.label('Вы победили!')
    elif (int(score) - 1) < (int(score_enemy) - int(length_e.get_value())):
        menu = pygame_menu.Menu('Смерть', SCREEN_WIDTH, SCREEN_HEIGHT, theme=pygame_menu.themes.THEME_DARK)
        menu.add.label('Вы набрали ' + str(int(score) - 1))
        menu.add.label('Враг набрал ' + str(int(score_enemy) - int(length_e.get_value())))
        menu.add.label('Вы проиграли!')
    else:
        menu = pygame_menu.Menu('Смерть', SCREEN_WIDTH, SCREEN_HEIGHT, theme=pygame_menu.themes.THEME_DEFAULT)
        menu.add.label('Вы набрали ' + str(int(score) - 1))
        menu.add.label('Враг набрал ' + str(int(score_enemy) - int(length_e.get_value())))
        menu.add.label('Ничья!')
    menu.add.button('Повторить', start_the_game)
    parameters = pygame_menu.Menu('Настройки', SCREEN_WIDTH, SCREEN_HEIGHT, theme=pygame_menu.themes.THEME_BLUE)
    quantity = parameters.add.text_input('Количество ячеек по ширине и высоте: ', default=int(q), maxchar=3,
                                         textinput_id='q',
                                         input_type=pygame_menu.locals.INPUT_INT)
    w_d = parameters.add.text_input('Разрешение по ширине: ', default=int(SCREEN_WIDTH), maxchar=4, textinput_id='w',
                                    input_type=pygame_menu.locals.INPUT_INT)
    h_d = parameters.add.text_input('Разрешение по высоте: ', default=int(SCREEN_HEIGHT), maxchar=4, textinput_id='h',
                                    input_type=pygame_menu.locals.INPUT_INT)
    length_e = parameters.add.text_input('Длина врага: ', default=int(default_score_enemy), maxchar=4, textinput_id='length_e',
                                         input_type=pygame_menu.locals.INPUT_INT)
    length_m = parameters.add.text_input('Ваша длина: ', default=int(default_score), maxchar=2, textinput_id='length_m',
                                         input_type=pygame_menu.locals.INPUT_INT)
    fps = parameters.add.text_input('FPS: ', default=int(speed), maxchar=4, textinput_id='fps',
                                    input_type=pygame_menu.locals.INPUT_INT)
    documentation = pygame_menu.Menu('Правила', SCREEN_WIDTH, SCREEN_HEIGHT, theme=pygame_menu.themes.THEME_BLUE)
    documentation.add.label('Поедайте яблоки, \n чтобы набрать больше очков \n чем вражеская змейка, \n '
                            ' не нападайте сами на себя \n и не врезайтесь во вражескую змейку!')
    menu.add.button('Правила', documentation)
    menu.add.button('Настройки', parameters)
    menu.add.button('Выход', pygame_menu.events.EXIT)
    menu.mainloop(screen)


here_you_are = []
here_enemy_are = []
clock = pygame.time.Clock()
draw = False
running = True
counter = 0
mark = "Счёт: " + str(score - 1)


def start_the_game():
    global draw
    global running
    global counter
    global here_you_are
    global clock
    global screen_tkinter
    global score
    global last
    global enemy_last
    global button
    global score_last
    global speed
    global SCREEN_WIDTH
    global SCREEN_HEIGHT
    global game_map
    global q
    global length
    global here_enemy_are
    global apple_i
    global apple_j
    global copy_list
    global score_enemy
    global screen
    global w_d
    global h_d
    global length_m
    global length_e
    global death_enemy
    global fps
    global death_count
    global eat
    eat = False
    q = quantity.get_value()
    length = length_m.get_value()
    score_enemy = length_e.get_value()
    if length < 1:
        length = 1
    if score_enemy < 1:
        score_enemy = 1
    if q > 500:
        q = 500
    if q < 15:
        q = 15
    SCREEN_WIDTH = w_d.get_value()
    if SCREEN_WIDTH > 1920:
        SCREEN_WIDTH = 1920
    if SCREEN_WIDTH < 300:
        SCREEN_WIDTH = 300
    SCREEN_HEIGHT = h_d.get_value()
    if SCREEN_HEIGHT > 1920:
        SCREEN_HEIGHT = 1920
    if SCREEN_HEIGHT < 300:
        SCREEN_HEIGHT = 300
    speed = fps.get_value()
    copy_list.clear()
    death_enemy = False
    if speed < 0:
        speed = 1
    screen = pygame.display.set_mode([SCREEN_WIDTH + 1, SCREEN_HEIGHT + 1])
    pygame.mixer.music.load("sounds/ost.mp3")
    pygame.mixer.music.set_volume(0.2)
    pygame.mixer.music.play(-1)
    game_map.clear()
    for i in range(q):
        game_map.append([0]*q)
    for i in range(len(game_map[0])):
        for j in range(len(game_map[0])):
            if i == (q//2) and j == (q//2):
                game_map[i][j] = 1
            if i == (q//2) and j == (q-5):
                game_map[i][j] = 2
    score = 1
    last.clear()
    last = []
    enemy_last.clear()
    here_enemy_are.clear()
    here_enemy_are = []
    enemy_last = []
    button = 0
    score_last = 1
    here_you_are.clear()
    here_you_are = []
    clock = pygame.time.Clock()
    draw = False
    running = True
    counter = 0
    mark = "Счёт: " + str(score - 1)
    while running:
        mark = "Счёт: " + str(score - 1)
        pygame.display.set_caption("Snake " + mark)
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE or event.key == K_q:
                    # print(here_enemy_are)
                    exit()
        if not draw:
            screen.fill((115, 115, 115))
            for i in range(0, len(game_map[0])):
                for j in range(0, len(game_map[0])):
                    if game_map[i][j] == 0:
                        pygame.draw.rect(screen, (0, 0, 0),
                                         pygame.Rect(j * SCREEN_WIDTH // q + 1, i * SCREEN_HEIGHT // q + 1,
                                                     SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
                    elif game_map[i][j] == 1:
                        here_you_are.append([i, j])
                        pygame.draw.rect(screen, (255, 0, 0),
                                         pygame.Rect(j * SCREEN_WIDTH // q + 1, i * SCREEN_HEIGHT // q + 1,
                                                     SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
                    elif game_map[i][j] == 2:
                        here_enemy_are.append([i, j])
                        pygame.draw.rect(screen, (0, 0, 255),
                                         pygame.Rect(j * SCREEN_WIDTH // q + 1, i * SCREEN_HEIGHT // q + 1,
                                                     SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
                    elif game_map[i][j] == 9:
                        pygame.draw.rect(screen, (0, 255, 0),
                                         pygame.Rect(j * SCREEN_WIDTH // q + 1, i * SCREEN_HEIGHT // q + 1,
                                                     SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
            flag = True
            while flag:
                apple_i = random.randint(0, (q-1))
                apple_j = random.randint(0, (q-1))
                if game_map[apple_i][apple_j] != 1 and game_map[apple_i][apple_j] != 2:
                    flag = False
            game_map[apple_i][apple_j] = 9
            pygame.draw.rect(screen, (0, 255, 0),
                             pygame.Rect(apple_j * SCREEN_WIDTH // q + 1, apple_i * SCREEN_HEIGHT // q + 1,
                                         SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
            draw = True
        pressed_keys = pygame.key.get_pressed()
        move_update(pressed_keys)
        enemy_update()
        if here_you_are[0][0] == apple_i and here_you_are[0][1] == apple_j:
            tuturu.play(0)
            flag = True
            while flag:
                apple_i = random.randint(0, (q-1))
                apple_j = random.randint(0, (q-1))
                if game_map[apple_i][apple_j] != 1 and game_map[apple_i][apple_j] != 2:
                    flag = False
            game_map[apple_i][apple_j] = 9
            pygame.draw.rect(screen, (0, 255, 0),
                                         pygame.Rect(apple_j * SCREEN_WIDTH // q + 1, apple_i * SCREEN_HEIGHT // q + 1,
                                                     SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
            score += 1
            score_last = score - 1
        if here_enemy_are[0][0] == apple_i and here_enemy_are[0][1] == apple_j:
            tuturu.play(0)
            flag = True
            while flag:
                apple_i = random.randint(0, (q-1))
                apple_j = random.randint(0, (q-1))
                if game_map[apple_i][apple_j] != 1 and game_map[apple_i][apple_j] != 2:
                    flag = False
            game_map[apple_i][apple_j] = 9
            pygame.draw.rect(screen, (0, 255, 0),
                                         pygame.Rect(apple_j * SCREEN_WIDTH // q + 1, apple_i * SCREEN_HEIGHT // q + 1,
                                                     SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
            score_enemy += 1
        counter = 0
        for i in range(len(here_you_are)):
            if i == (len(here_you_are) - 1) and here_you_are[0] != here_you_are[i]:
                game_map[here_you_are[i][0]][here_you_are[i][1]] = 0
                pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(here_you_are[i][1] * SCREEN_WIDTH // q + 1,
                                                                  here_you_are[i][0] * SCREEN_HEIGHT // q + 1,
                                                                  SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
            else:
                pygame.draw.rect(screen, (255, 0, 0), pygame.Rect(here_you_are[i][1] * SCREEN_WIDTH // q + 1,
                                                                  here_you_are[i][0] * SCREEN_HEIGHT // q + 1,
                                                                  SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
        for i in range(len(here_enemy_are)):
            if i == (len(here_enemy_are) - 1) and here_enemy_are[0] != here_enemy_are[i]:
                game_map[here_enemy_are[i][0]][here_enemy_are[i][1]] = 0
                pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(here_enemy_are[i][1] * SCREEN_WIDTH // q + 1,
                                                                  here_enemy_are[i][0] * SCREEN_HEIGHT // q + 1,
                                                                  SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
            else:
                pygame.draw.rect(screen, (0, 0, 255), pygame.Rect(here_enemy_are[i][1] * SCREEN_WIDTH // q + 1,
                                                                      here_enemy_are[i][0] * SCREEN_HEIGHT // q + 1,
                                                                      SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
        if not death_enemy:
            pygame.draw.rect(screen, (255, 25, 155), pygame.Rect(here_enemy_are[0][1] * SCREEN_WIDTH // q + 1,
                                                                 here_enemy_are[0][0] * SCREEN_HEIGHT // q + 1,
                                                                 SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))
        pygame.draw.rect(screen, (255, 155, 25), pygame.Rect(here_you_are[0][1] * SCREEN_WIDTH // q + 1,
                                                             here_you_are[0][0] * SCREEN_HEIGHT // q + 1,
                                                             SCREEN_WIDTH // q - 1, SCREEN_HEIGHT // q - 1))

        count = 0
        # if score != score_last:
        #     speed += 0.3
        #     score_last = score
        for element in here_you_are:
            if here_you_are[0] == element and element != here_you_are[-1]:
                count += 1
        if count == 2:
            death_window()
        if here_you_are[0] in here_enemy_are and not death_enemy:
            death_window()
        if death_enemy:
            if here_you_are[0] in copy_list:
                tuturu.play(0)
                score += 1
                score_last = score - 1
                copy_list.remove(here_you_are[0])
        pygame.display.update()
        clock.tick(speed)


menu = pygame_menu.Menu('Змейка', SCREEN_WIDTH, SCREEN_HEIGHT, theme=pygame_menu.themes.THEME_GREEN)
menu.add.button('Играть', start_the_game)
parameters = pygame_menu.Menu('Настройки',  SCREEN_WIDTH, SCREEN_HEIGHT, theme=pygame_menu.themes.THEME_BLUE)
quantity = parameters.add.text_input('Количество ячеек по ширине и высоте: ', default=int(q), maxchar=3, textinput_id='q',
                               input_type=pygame_menu.locals.INPUT_INT)
w_d = parameters.add.text_input('Разрешение по ширине: ', default=int(SCREEN_WIDTH), maxchar=4, textinput_id='w',
                               input_type=pygame_menu.locals.INPUT_INT)
h_d = parameters.add.text_input('Разрешение по высоте: ', default=int(SCREEN_HEIGHT), maxchar=4, textinput_id='h',
                               input_type=pygame_menu.locals.INPUT_INT)
length_e = parameters.add.text_input('Длина врага: ', default=int(score_enemy), maxchar=4, textinput_id='length_e',
                               input_type=pygame_menu.locals.INPUT_INT)
default_score_enemy = score_enemy
length_m = parameters.add.text_input('Ваша длина: ', default=int(length), maxchar=2, textinput_id='length_m',
                               input_type=pygame_menu.locals.INPUT_INT)
default_score = length
fps = parameters.add.text_input('FPS: ', default=int(speed), maxchar=4, textinput_id='fps',
                               input_type=pygame_menu.locals.INPUT_INT)
documentation = pygame_menu.Menu('Правила',  SCREEN_WIDTH, SCREEN_HEIGHT, theme=pygame_menu.themes.THEME_BLUE)
documentation.add.label('Поедайте яблоки, \n чтобы набрать больше очков \n чем вражеская змейка, \n '
                        ' не нападайте сами на себя \n и не врезайтесь во вражескую змейку!')
menu.add.button('Правила', documentation)
menu.add.button('Настройки', parameters)
menu.add.button('Выход', pygame_menu.events.EXIT)
menu.mainloop(screen)
