<h1 align="center">Flowchart</h1>

Come up with and implement an interactive animation of a programming topic (for example, about the work of loops...).
In feedback you can offer your variants.

Options:

An interactive programme for creating graphs, which allows you to arrange nodes, create links between them and create a graph's adjacency matrix based on the drawing (you can use the programme from drawSvg site as a basis);
an interactive programme for creating flowcharts (a bit more complicated in that you need to draw some set of template elements in advance) with saving the result in SVG.

## [Flowchart.ipynb](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/1st_year_2nd_semester/programming/05_flowchart/04.29_flowchart.ipynb)

# Demonstration:

![1](images/1.png "1")
![2](images/2.png "2")
![3](images/3.png "3")
![4](images/4.png "4")
![5](images/5.png "5")
![6](images/6.png "6")
![7](images/7.png "7")