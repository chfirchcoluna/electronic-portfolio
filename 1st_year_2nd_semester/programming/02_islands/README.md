<h1 align="center">Islands</h1>

A text file is given (generate it yourself and pour it into the git with the programme), which contains a two-dimensional array of digits from 0 to 9. Zeros in the array denote emptiness, they are not taken into account. Groups of identical digits form islands. The formation of islands is possible only by identical digits standing next to each other vertically or horizontally. Diagonal neighbourhood does not form an island. Single standing digits form a separate island. We need to count the number of islands in the array. Example:

0 0 0 0 0 0 0
0 1 1 2 0 0 0
1 1 2 2 0 0 0
0 0 0 0 2 0 0
0 0 0 0 2 2 0
3 0 0 0 0 0 0
0 0 0 0 0 0 0

The array shown above contains 4 islands:

1 1 2 2 3
1 1 2 2 2 2

## [Islands.py](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/1st_year_2nd_semester/programming/02_islands/src/islands.py)

# Demonstration:

![1](images/1.png "1")
