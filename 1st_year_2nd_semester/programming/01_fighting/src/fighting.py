import pygame
import random
import tkinter
from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    K_z,
    K_SPACE,
    RLEACCEL
)


def player_init():
    global player_sprite
    player_sprite.image = pygame.image.load("player_right.png").convert()
    player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
    player_sprite.rect = player_sprite.image.get_rect(center=(SCREEN_WIDTH/2, SCREEN_HEIGHT/2))


def attack_init():
    global attack_sprite
    attack_sprite.image = pygame.Surface((75, 75))
    attack_sprite.image.fill((255, 255, 255))
    attack_sprite.rect = attack_sprite.image.get_rect(center=(SCREEN_WIDTH + 100, SCREEN_HEIGHT + 100))


a = -2
b = 2
c = -2
d = 2
flag = 0
stopping = 0
counter = [0, 0, 0, 0, 0]
duration = -1
energy = 10000


def player_update(pressed_keys):
    global player_sprite
    global flag
    global counter
    global a
    global b
    global c
    global d
    global stopping
    global duration
    global energy
    if energy < 100:
        energy += 0.1
    # print(energy)
    if pressed_keys[K_UP] and (not pressed_keys[K_LEFT]) and (not pressed_keys[K_RIGHT]) and (
            not pressed_keys[K_SPACE]):
        counter[3] = 0
        if (flag == 1) and (not pressed_keys[K_DOWN]):
            stopping = 0
            counter[flag] += 0.06
            player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
            player_sprite.rect.move_ip(0, a + 1)
            if counter[flag] > 1.2:
                player_sprite.image = pygame.image.load("player_right_down.png").convert()
                player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                if counter[flag] > 1.8:
                    player_sprite.image = pygame.image.load("player_right.png").convert()
                    player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                    if counter[flag] > 3.4:
                        player_sprite.image = pygame.image.load("player_right_up.png").convert()
                        player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                        if counter[flag] > 5:
                            player_sprite.image = pygame.image.load("player_up.png").convert()
                            player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                            counter[flag] = 0
                            flag = 0
        else:
            if counter[flag + 1] > 0:
                flag = 1
                stopping += 0.06
                if stopping > 2:
                    player_sprite.rect.move_ip(0, a + 1)
                if stopping > 5:
                    counter[flag] = 0
                    stopping = 0
                    counter[flag + 1] = 0
                    flag = 0
            else:
                player_sprite.image = pygame.image.load("player_up.png").convert()
                player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                player_sprite.rect.move_ip(0, a)
                counter[flag] = 0
                counter[flag + 1] = 0
                stopping = 0
                flag = 0
    if pressed_keys[K_DOWN] and (not pressed_keys[K_LEFT]) and (not pressed_keys[K_RIGHT]) and (
            not pressed_keys[K_SPACE]):
        counter[3] = 0
        if (flag == 0) and (not pressed_keys[K_UP]):
            stopping = 0
            counter[flag + 1] += 0.06
            player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
            player_sprite.rect.move_ip(0, b - 1)
            if counter[flag + 1] > 1.2:
                player_sprite.image = pygame.image.load("player_right_up.png").convert()
                player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                if counter[flag + 1] > 1.8:
                    player_sprite.image = pygame.image.load("player_right.png").convert()
                    player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                    if counter[flag + 1] > 3.4:
                        player_sprite.image = pygame.image.load("player_right_down.png").convert()
                        player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                        if counter[flag + 1] > 5:
                            player_sprite.image = pygame.image.load("player_down.png").convert()
                            player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                            counter[flag + 1] = 0
                            flag = 1
        else:
            if counter[flag] > 0:
                flag = 0
                stopping += 0.06
                if stopping > 2:
                    player_sprite.rect.move_ip(0, b - 1)
                if stopping > 5:
                    counter[flag + 1] = 0
                    stopping = 0
                    counter[flag] = 0
                    flag = 1
            else:
                player_sprite.image = pygame.image.load("player_down.png").convert()
                player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                player_sprite.rect.move_ip(0, b)
                counter[flag] = 0
                counter[flag + 1] = 0
                stopping = 0
                flag = 1
    if pressed_keys[K_LEFT] and (not pressed_keys[K_UP]) and (not pressed_keys[K_DOWN]) and (not pressed_keys[K_SPACE]):
        counter[1] = 0
        if (flag == 3) and (not pressed_keys[K_RIGHT]):
            stopping = 0
            counter[flag] += 0.06
            player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
            player_sprite.rect.move_ip(c + 1, 0)
            if counter[flag] > 1.2:
                player_sprite.image = pygame.image.load("player_right_up.png").convert()
                player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                if counter[flag] > 1.8:
                    player_sprite.image = pygame.image.load("player_up.png").convert()
                    player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                    if counter[flag] > 3.4:
                        player_sprite.image = pygame.image.load("player_left_up.png").convert()
                        player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                        if counter[flag] > 5:
                            player_sprite.image = pygame.image.load("player_left.png").convert()
                            player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                            counter[flag] = 0
                            flag = 2
        else:
            if counter[flag + 1] > 0:
                flag = 3
                stopping += 0.06
                if stopping > 2:
                    player_sprite.rect.move_ip(c + 1, 0)
                if stopping > 5:
                    counter[flag + 1] = 0
                    stopping = 0
                    counter[flag] = 0
                    flag = 2
            else:
                player_sprite.image = pygame.image.load("player_left.png").convert()
                player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                player_sprite.rect.move_ip(c, 0)
                counter[flag] = 0
                counter[flag - 1] = 0
                stopping = 0
                flag = 2
    if (pressed_keys[K_RIGHT]) and (not pressed_keys[K_UP]) and (not pressed_keys[K_DOWN]) and (
            not pressed_keys[K_SPACE]):
        counter[1] = 0
        if (flag == 2) and (not pressed_keys[K_LEFT]):
            stopping = 0
            counter[flag + 1] += 0.06
            player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
            player_sprite.rect.move_ip(d - 1, 0)
            if counter[flag + 1] > 1.2:
                player_sprite.image = pygame.image.load("player_left_up.png").convert()
                player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                if counter[flag + 1] > 1.8:
                    player_sprite.image = pygame.image.load("player_up.png").convert()
                    player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                    if counter[flag + 1] > 3.4:
                        player_sprite.image = pygame.image.load("player_right_up.png").convert()
                        player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                        if counter[flag + 1] > 5:
                            player_sprite.image = pygame.image.load("player_right.png").convert()
                            player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                            counter[flag + 1] = 0
                            flag = 3
        else:
            if counter[flag] > 0:
                flag = 2
                stopping += 0.06
                if stopping > 2:
                    player_sprite.rect.move_ip(d - 1, 0)
                if stopping > 5:
                    counter[flag + 1] = 0
                    stopping = 0
                    counter[flag] = 0
                    flag = 3
            else:
                player_sprite.image = pygame.image.load("player_right.png").convert()
                player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
                player_sprite.rect.move_ip(d, 0)
                counter[flag] = 0
                counter[flag - 1] = 0
                stopping = 0
                flag = 3
    if pressed_keys[K_RIGHT] and pressed_keys[K_DOWN]:
        player_sprite.rect.move_ip(d - 1, b - 1)
        player_sprite.image = pygame.image.load("player_right_down.png").convert()
        player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
        duration = 0
    if pressed_keys[K_RIGHT] and pressed_keys[K_UP]:
        player_sprite.rect.move_ip(d - 1, a + 1)
        player_sprite.image = pygame.image.load("player_right_up.png").convert()
        player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
        duration = 1
    if pressed_keys[K_LEFT] and pressed_keys[K_DOWN]:
        player_sprite.rect.move_ip(c + 1, b - 1)
        player_sprite.image = pygame.image.load("player_left_down.png").convert()
        player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
        duration = 2
    if pressed_keys[K_LEFT] and pressed_keys[K_UP]:
        player_sprite.rect.move_ip(c + 1, a + 1)
        player_sprite.image = pygame.image.load("player_left_up.png").convert()
        player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
        duration = 3
    if pressed_keys[K_RIGHT] and pressed_keys[K_SPACE] and not pressed_keys[K_UP] and not pressed_keys[K_DOWN] and (
            energy > 0):
        energy -= 2
        player_sprite.rect.move_ip(d + 1, 0)
        player_sprite.image = pygame.image.load("player_right.png").convert()
        player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
    if pressed_keys[K_LEFT] and pressed_keys[K_SPACE] and not pressed_keys[K_UP] and not pressed_keys[K_DOWN] and (
            energy > 0):
        energy -= 2
        player_sprite.rect.move_ip(c - 1, 0)
        player_sprite.image = pygame.image.load("player_left.png").convert()
        player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
    if pressed_keys[K_UP] and pressed_keys[K_SPACE] and not pressed_keys[K_LEFT] and not pressed_keys[K_RIGHT] and (
            energy > 0):
        energy -= 2
        player_sprite.rect.move_ip(0, a - 1)
        player_sprite.image = pygame.image.load("player_up.png").convert()
        player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
    if pressed_keys[K_DOWN] and pressed_keys[K_SPACE] and not pressed_keys[K_LEFT] and not pressed_keys[K_RIGHT] and (
            energy > 0):
        energy -= 2
        player_sprite.rect.move_ip(0, b + 1)
        player_sprite.image = pygame.image.load("player_down.png").convert()
        player_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
    if player_sprite.rect.left < 0:
        player_sprite.rect.left = 0
    if player_sprite.rect.right > SCREEN_WIDTH:
        player_sprite.rect.right = SCREEN_WIDTH
    if player_sprite.rect.top <= 0:
        player_sprite.rect.top = 0
    if player_sprite.rect.bottom >= SCREEN_HEIGHT:
        player_sprite.rect.bottom = SCREEN_HEIGHT


v = -1
e = 1
f = -1
t = 1
hp = 3


def drawText(text, font, surface, x, y):
    textobj = font.render(text, 1, (0, 0, 0))
    textrect = textobj.get_rect()
    textrect.topleft = (x, y)
    surface.blit(textobj, textrect)


def attack_update(pressed_keys):
    global flag
    global energy
    if (not pressed_keys[K_z]) or (energy < 10):
        attack_sprite.rect[0] = SCREEN_WIDTH + 100
        attack_sprite.rect[1] = SCREEN_HEIGHT + 100
    elif (pressed_keys[K_z]) and (flag == 0) and (energy > 10):
        energy -= 10
        attack_sprite.rect[0] = player_sprite.rect[0]
        attack_sprite.rect[1] = player_sprite.rect[1] - 70
    elif (pressed_keys[K_z]) and (flag == 1) and (energy > 10):
        energy -= 10
        attack_sprite.rect[0] = player_sprite.rect[0]
        attack_sprite.rect[1] = player_sprite.rect[1] + 50
    elif (pressed_keys[K_z]) and (flag == 2) and (energy > 10):
        energy -= 10
        attack_sprite.rect[0] = player_sprite.rect[0] - 70
        attack_sprite.rect[1] = player_sprite.rect[1]
    elif (pressed_keys[K_z]) and (flag == 3) and (energy > 10):
        energy -= 10
        attack_sprite.rect[0] = player_sprite.rect[0] + 50
        attack_sprite.rect[1] = player_sprite.rect[1]


spawned = 0


def enemy_update():
    global enemy_sprite
    global flag
    global hp
    global spawned
    global enemies
    global quantity
    for number in enemies:
        if number["hp"] <= 0:
            enemies.append(new_enemy_copy)
            while (number["rect"][0] > SCREEN_WIDTH or number["rect"][0] < SCREEN_WIDTH) and (number["rect"][1] > SCREEN_HEIGHT or number["rect"][1] < SCREEN_HEIGHT):
                number["rect"][0] = random.randint(-300, SCREEN_WIDTH + 100)
                number["rect"][1] = random.randint(-300, SCREEN_HEIGHT + 100)
            spawned += 1
            quantity += 1
            number["hp"] = 5
        elif attack_sprite.rect.colliderect(number["rect"]):
            if (flag == 3) and (number["hp"] > 0):
                if number["hp"] > 1:
                    number["rect"].move_ip(100, 0)
                number["hp"] -= 1
            elif (flag == 2) and (number["hp"] > 0):
                if number["hp"] > 1:
                    number["rect"].move_ip(-100, 0)
                number["hp"] -= 1
            elif (flag == 1) and (number["hp"] > 0):
                if number["hp"] > 1:
                    number["rect"].move_ip(0, 100)
                number["hp"] -= 1
            elif (flag == 0) and (number["hp"] > 0):
                if number["hp"] > 1:
                    number["rect"].move_ip(0, -100)
                number["hp"] -= 1
        else:
            if (player_sprite.rect[0] < (number["rect"])[0]) and (player_sprite.rect[1] < (number["rect"])[1]):
                number["rect"].move_ip(v, f)
                number["surface"] = pygame.image.load("player_left_up.png").convert()
                number["surface"].set_colorkey((255, 255, 255), RLEACCEL)
            if (player_sprite.rect[0] < (number["rect"])[0]) and (player_sprite.rect[1] > (number["rect"])[1]):
                number["rect"].move_ip(v, t)
                number["surface"] = pygame.image.load("player_left_down.png").convert()
                number["surface"].set_colorkey((255, 255, 255), RLEACCEL)
            if (player_sprite.rect[0] > (number["rect"])[0]) and (player_sprite.rect[1] < (number["rect"])[1]):
                number["rect"].move_ip(e, f)
                number["surface"] = pygame.image.load("player_right_up.png").convert()
                number["surface"].set_colorkey((255, 255, 255), RLEACCEL)
            if (player_sprite.rect[0] > (number["rect"])[0]) and (player_sprite.rect[1] > (number["rect"])[1]):
                number["rect"].move_ip(e, t)
                number["surface"] = pygame.image.load("player_right_down.png").convert()
                number["surface"].set_colorkey((255, 255, 255), RLEACCEL)
            if (player_sprite.rect[0] < (number["rect"])[0]) and (player_sprite.rect[1] == (number["rect"])[1]):
                number["rect"].move_ip(v - 1, 0)
                number["surface"] = pygame.image.load("player_left.png").convert()
                number["surface"].set_colorkey((255, 255, 255), RLEACCEL)
            if (player_sprite.rect[0] > (number["rect"])[0]) and (player_sprite.rect[1] == (number["rect"])[1]):
                number["rect"].move_ip(e + 1, 0)
                number["surface"] = pygame.image.load("player_right.png").convert()
                number["surface"].set_colorkey((255, 255, 255), RLEACCEL)
            if (player_sprite.rect[1] < (number["rect"])[1]) and (player_sprite.rect[0] == (number["rect"])[0]):
                number["rect"].move_ip(0, f - 1)
                number["surface"] = pygame.image.load("player_up.png").convert()
                number["surface"].set_colorkey((255, 255, 255), RLEACCEL)
            if (player_sprite.rect[1] > (number["rect"])[1]) and (player_sprite.rect[0] == (number["rect"])[0]):
                number["rect"].move_ip(0, t + 1)
                number["surface"] = pygame.image.load("player_down.png").convert()
                number["surface"].set_colorkey((255, 255, 255), RLEACCEL)


def starting():
    global running
    global screen_tkinter
    global Esc
    screen_tkinter.quit()
    screen_tkinter.destroy()
    Esc = False
    running = True


def efg():
    global running
    screen_tkinter.quit()
    screen_tkinter.destroy()
    running = False


SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen_tkinter = tkinter.Tk()
screen_tkinter.title("Начало")
screen_tkinter.geometry(str(SCREEN_WIDTH) + "x" + str(SCREEN_HEIGHT))
running = False
start = tkinter.Button(screen_tkinter, text="Начать", command=starting).pack()
exit_from_game = tkinter.Button(screen_tkinter, text="Выход", command=efg).pack()
Header = tkinter.Label(screen_tkinter, text="Управление:").pack()
control = tkinter.Label(screen_tkinter, text="Хождение - стрелочки\nАтака - z").pack()
screen_tkinter.mainloop()

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
pygame.init()
player_sprite = pygame.sprite.Sprite()
attack_sprite = pygame.sprite.Sprite()
screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])
pygame.display.set_caption("Fighting")
clock = pygame.time.Clock()
player_init()
enemy_sprite = pygame.sprite.Sprite()
enemy_sprite.image = pygame.image.load("player_left.png").convert()
enemy_sprite.rect = enemy_sprite.image.get_rect()
enemy_original = {
             "rect": enemy_sprite.image.get_rect(center=(random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100), random.randint(0, SCREEN_HEIGHT))),
             "speed:": random.randint(5, 10),
             "surface": enemy_sprite.image,
             "color": enemy_sprite.image.set_colorkey((255, 255, 255), RLEACCEL),
             "hp": 5,
             }
enemies = []
quantity = 1
font = pygame.font.SysFont("Arial", 48)
enemies.append(enemy_original)
attack_init()
attack_sprite.image = pygame.image.load("attack.png").convert()
attack_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
player_sprites = pygame.sprite.Group()
player_sprites.add(player_sprite)
stones_sprites = pygame.sprite.Group()
stone_1_sprite = pygame.sprite.Sprite()
stone_1_sprite.image = pygame.image.load("stone_1.png").convert()
stone_1_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
stone_1_sprite.rect = stone_1_sprite.image.get_rect(center=(random.randint(SCREEN_WIDTH - 200, SCREEN_WIDTH - 100), random.randint(0, SCREEN_HEIGHT)))
stone_2_sprite = pygame.sprite.Sprite()
stone_2_sprite.image = pygame.image.load("stone_2.png").convert()
stone_2_sprite.image.set_colorkey((255, 255, 255), RLEACCEL)
stone_2_sprite.rect = stone_2_sprite.image.get_rect(center=(random.randint(SCREEN_WIDTH - 200, SCREEN_WIDTH - 100), random.randint(0, SCREEN_HEIGHT)))
stones_sprites.add(stone_1_sprite)
stones_sprites.add(stone_2_sprite)
background = pygame.sprite.Sprite()
background.image = pygame.image.load("background.png").convert()
Esc = False
while running:
    new_enemy_copy = {
        "rect": enemy_sprite.image.get_rect(
            center=(random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100), random.randint(0, SCREEN_HEIGHT))),
        "speed:": random.randint(5, 10),
        "surface": enemy_sprite.image,
        "color": enemy_sprite.image.set_colorkey((255, 255, 255), RLEACCEL),
        "hp": 5
    }
    screen.fill((55, 255, 255))
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE and Esc == False:
                screen_tkinter = tkinter.Tk()
                screen_tkinter.title("ESC")
                screen_tkinter.geometry(str(SCREEN_WIDTH) + "x" + str(SCREEN_HEIGHT))
                start = tkinter.Button(screen_tkinter, text="Продолжить", command=starting).pack()
                exit_from_game = tkinter.Button(screen_tkinter, text="Выход", command=efg).pack()
                Header = tkinter.Label(screen_tkinter, text="Управление:").pack()
                control = tkinter.Label(screen_tkinter, text="Хождение - стрелочки\nАтака - z").pack()
                Esc = True
                screen_tkinter.mainloop()
    pressed_keys = pygame.key.get_pressed()
    player_update(pressed_keys)
    attack_update(pressed_keys)
    enemy_update()
    screen.blit(background.image, (0, 0))
    screen.blit(stone_1_sprite.image, stone_1_sprite.rect)
    screen.blit(stone_2_sprite.image, stone_2_sprite.rect)
    screen.blit(player_sprite.image, player_sprite.rect)
    for number in enemies:
        screen.blit(number["surface"], number["rect"])
    for number in enemies:
        if player_sprite.rect.colliderect(number["rect"]):
            enemy = enemies[0]
            enemies.clear()
            quantity = 1
            energy = 10000
            enemies.append(enemy)
            enemies[0]["rect"] = enemy_sprite.image.get_rect(
            center=(random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100), random.randint(0, SCREEN_HEIGHT)))
            player_init()
            screen_tkinter = tkinter.Tk()
            screen_tkinter.title("Вы погибли")
            screen_tkinter.geometry(str(SCREEN_WIDTH) + "x" + str(SCREEN_HEIGHT))
            running = False
            start = tkinter.Button(screen_tkinter, text="Попробовать ещё раз", command=starting).pack()
            exit_from_game = tkinter.Button(screen_tkinter, text="Сдаться", command=efg).pack()
            Header = tkinter.Label(screen_tkinter, text="Управление:").pack()
            control = tkinter.Label(screen_tkinter, text="Хождение - стрелочки\nАтака - z").pack()
            screen_tkinter.mainloop()
    screen.blit(attack_sprite.image, attack_sprite.rect)
    if pygame.sprite.spritecollideany(player_sprite, stones_sprites):
        print("Я нахожусь на камне")
    drawText("Врагов повержено: " + str(quantity-1), font, screen, 10, 0)
    pygame.display.flip()
    clock.tick(60)
pygame.quit()
