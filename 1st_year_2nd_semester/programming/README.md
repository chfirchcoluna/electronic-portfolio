<h1 align="center">Programming</h1>

Objectives: To provide students with a set of theoretical knowledge and practical skills sufficient to develop applications on a compilable
practical skills sufficient to develop applications in compilable programming language (C++), interpreted programming language Python.
programming language (C++), interpreted programming language Python
To develop skills of algorithmic thinking, to master effective class algorithms and principles of writing rational programme code.
Objectives:
- Gain practical skills and experience in working with basic data types,
arrays, conditional and cyclic operators, recursion.
- Gain practical skills and experience with functions and subroutines,
functional decomposition of voluminous tasks, master modular and structural programming.
programming.
- Gain practical skills and experience in debugging programmes, writing stable and fast-acting programmes.
writing stable and fast-acting programmes.

## [Demonstration of Fighting](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/1st_year_2nd_semester/programming/01_fighting)

## [Demonstration of Islands](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/1st_year_2nd_semester/programming/02_islands)

## [Demonstration of Distance](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/1st_year_2nd_semester/programming/03_distance)

## [Demonstration of Snake](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/1st_year_2nd_semester/programming/04_snake)

## [Demonstration of Flowchart](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/1st_year_2nd_semester/programming/05_flowchart)