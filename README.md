<h1 align="center">Electronic Portfolio</h1>

# Резюме (Ru):

ОБРАЗОВАНИЕ:
- студент ИГУ ФБКИ направления Прикладная Информатика в дизайне,
- Детская Художественная Школа г. Саянск.

ПРОФЕССИОНАЛЬНЫЕ НАВЫКИ:
- Владение иностранным языком (английский язык),
- умение работать в команде,
- стремление к профессиональному росту,
- коммуникабельность,
- сверхответственное отношение к работе.

ЗНАНИЕ КОМПЬЮТЕРНЫХ ПРОГРАММ:
- Adobe Photoshop,
- Figma,
- Illustrator,
- CoreI Draw,
- Animate,
- программ профессионального пакета Microsoft Office 2019,
- DaVinci Resolve,
- TouchDesigner,
- Unity.

ЗНАНИЕ ЯЗЫКОВ ПРОГРАМИРОВАНИЯ:
- Свободное владение Python, 
- базовые знания синтаксиса и стандартных операций:
    - C#, 
    - C++, 
    - JavaScript, 
    - HTML&CSS, 
    - PHP, 
    - SQL, 
    - QML, 
    - Kotlin (Android Studio).

# Resume (En):

EDUCATION: 
- ISU FBCI student of Applied Informatics in Design,
- Children's Art School of Sayansk.

PROFESSIONAL SKILLS: 
- Foreign language skills (English), 
- ability to work in a team, 
- desire for professional growth, 
- communication skills, 
- responsible attitude to work.

KNOWLEDGE OF COMPUTER PROGRAMMES:
- Adobe Photoshop, 
- Figma, 
- Illustrator, 
- CoreI Draw, 
- Animate, 
- Microsoft Office 2019 professional package programmes, 
- DaVinci Resolve, 
- TouchDesigner, 
- Unity.

KNOWLEDGE OF PROGRAMMING LANGUAGES:
- Fluent in Python, 
- basic knowledge of syntax and standard operations of: 
    - C#, 
    - C++, 
    - JavaScript, 
    - HTML&CSS, 
    - PHP, 
    - SQL, 
    - QML, 
    - Kotlin (Android Studio).

# Ссылки/Links:

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/summer_practice/summer_practice.pdf">DEMONSTRATION OF SUMMER PRACTICE 2nd year</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/1st_year_2nd_semester/summer_practice/summer_practice_1.pdf">DEMONSTRATION OF SUMMER PRACTICE 1st year</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/3rd_year_5th_semester/design_of_graphical_user_interfaces">Design of Graphical User Interfaces</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/3rd_year_5th_semester/publishing_and_design">Publishing and Design</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_4th_semester/visual_identity">Visual Identity</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_4th_semester/web_application_design">Web Application Design</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_4th_semester/server-side_programmingn">Server-side Programming</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_4th_semester/request_game">Request Game</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_4th_semester/photography_technique_and_technology">Photography Technique and Technology</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester/multimedia_technology_and_animation">Multimedia Technology and Animation</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester/client-side_programming">Client-side Programming</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester/fundamentals_of_object-oriented_programming">Fundamentals of Object-Oriented Programming</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester/workshop_on_computer_mathematics">Computer Mathematics</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_3rd_semester/cave_of_time">Cave of Time Novel Game</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/1st_year_2nd_semester/programming">Programming 1st year</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/1st_year_2nd_semester/graphic_design/computer_graphic">Computer graphic</a></h1>

<h1 align="center"><a href="https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/1st_year_2nd_semester/graphic_design/website_prototype">Website prototype</a></h1>