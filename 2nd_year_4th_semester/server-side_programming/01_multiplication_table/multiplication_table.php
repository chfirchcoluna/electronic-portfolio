<?php
echo '<table style="border: 2px solid black; border-collapse: collapse; text-align: center;">';
for ($i = 0; $i <= 10; $i++) {
    echo '<tr>';
    for ($j = 0; $j <= 10; $j++) {
        if ($i === 0 && $j === 0) {
                echo '<td style="border: 2px solid black; min-width: 2em; background-color: #800000;"></td>';
        } else if ($i === 0 && $j !== 0) {
                echo '<td style="border: 2px solid black; min-width: 2em; font-weight: bold; background-color: #FECA9A;">'.$j.'</td>';
        } else if ($j === 0 && $i !== 0) {
                echo '<td style="border: 2px solid black; min-width: 2em; font-weight: bold; background-color: #FECA9A;">'.$i.'</td>';
        } else if ($i !== 0 && $j !== 0 && $i === $j) {
                echo '<td style="border: 1px solid black; min-width: 2em; background-color: #FFFD98;">'.$i * $j.'</td>';
        } else if ($i !== 0 && $j !== 0) {
                echo '<td style="border: 1px solid black; min-width: 2em;">'.$i * $j.'</td>';
        }
    }
    echo '</tr>';
}
echo '</table>';
?>