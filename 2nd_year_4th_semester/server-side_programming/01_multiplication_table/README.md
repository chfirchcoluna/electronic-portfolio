## 01 Task:

Using two for loops (one nested in the other), recreate the multiplication table familiar from school.

Use <table>, <tr>, <td> tags.

## Demonstration of work [multiplication_table.html](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/server-side_programming/01_multiplication_table/multiplication_table.html):

![1](1.png "1")