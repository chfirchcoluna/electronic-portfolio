<?php

$register = function($classname)
{
    $path = 'Classes/';
    $filename = $path.$classname.'.php';
    $filename = str_replace('\\', '/', $filename);
    require_once($filename);
};

spl_autoload_register($register);
