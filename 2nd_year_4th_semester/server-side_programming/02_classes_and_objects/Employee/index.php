<?php namespace App;

// register
require_once('Register/Register.php');

// logic
$emp0 = (new Employee('Sergey','Suslikov', 19, 'M'));
$emp1 = new Employee('Vladislav', 'Nikitin', 19, 'M');
$emp2 = (new Employee('Alexandr', 'Domnenko', 19, 'M'))->getInfo();

print_r($emp0);
$emp0->setAge(43);
print_r($emp0);
echo "\n";
$emp0->sayHelloUser();
echo "\n";

echo '---------------------------------------'."\n";

print_r($emp1);
$emp1 = $emp1->getInfo();
print_r($emp1);

echo '---------------------------------------'."\n";

print_r($emp2);
echo "\n";
echo $emp2->age.' years'."\n";
