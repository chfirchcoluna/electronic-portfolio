<?php namespace App;

class Human
{
    private $id;
    protected $gender;
    protected $age;
    public function __construct($gender, $age)
    {
        $this->id = uniqid();
        $this->gender = $gender;
        $this->age = $age;
    }

    public function setAge($value)
    {
        if ( 1 <= $value && $value <= 100 ) {
            $this->age = $value;
        }
    }
}
