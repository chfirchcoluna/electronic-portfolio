<?php namespace App;

class Employee extends Human implements AppUserInterface
{
    use HelloUserTrait;
    private $firstName;
    private $lastName;

    public function __construct($firstName, $lastName, $age, $gender)
    {
        parent::__construct($gender, $age);
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getInfo()
    {
        $myValues = new CurrentOldValuesEmployee();
        $myValues->firstName = $this->firstName;
        $myValues->lastName = $this->lastName;
        $myValues->age = $this->age;
        $myValues->gender = $this->gender;
        return $myValues;
    }
}
