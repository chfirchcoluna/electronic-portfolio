<?php namespace App;

trait HelloUserTrait
{
    public function sayHelloUser()
    {
        echo 'Hello, '.$this->firstName.'!';
    }
}
