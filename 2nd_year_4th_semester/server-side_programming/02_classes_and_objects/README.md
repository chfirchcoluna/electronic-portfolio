## 02 Task:

1. Create a base class on the example of a life situation, define its properties (including protected and private), define methods (in particular for working with private properties).
2. Create an inherited class, define its properties and methods.
3. Create an interface, implement it in your class.
4. Create trait(s), add it (them) to the class.
5. Create instances of the created classes and show how they work.

## Demonstration of work [index.php](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/server-side_programming/02_classes_and_objects/Employee/index.php):

App\Employee Object ( [id:App\Human:private] => 6563f9e8a9aad [gender:protected] => M [age:protected] => 19 [firstName:App\Employee:private] => Sergey [lastName:App\Employee:private] => Suslikov )

App\Employee Object ( [id:App\Human:private] => 6563f9e8a9aad [gender:protected] => M [age:protected] => 43 [firstName:App\Employee:private] => Sergey [lastName:App\Employee:private] => Suslikov ) Hello, Sergey! 

--------------------------------------- 

App\Employee Object ( [id:App\Human:private] => 6563f9e8a9aba [gender:protected] => M [age:protected] => 19 [firstName:App\Employee:private] => Vladislav [lastName:App\Employee:private] => Nikitin ) 

App\CurrentOldValuesEmployee Object ( [firstName] => Vladislav [lastName] => Nikitin [age] => 19 [gender] => M )

--------------------------------------- 

App\CurrentOldValuesEmployee Object ( [firstName] => Alexandr [lastName] => Domnenko [age] => 19 [gender] => M ) 19 years