## 04 Task:

### Exercise 1.
Create regular expressions to match the following types of data:
1. integer
2. A set of letters and digits (Latin)
3. a set of letters and digits (Latin + Cyrillic)
4. Domain ( google.com )
5. Username (limited to 3-25 characters, which can be letters and numbers, the first character must be a letter)
6. Password (lowercase and uppercase Latin letters, digits)
7. Password (lowercase and uppercase Latin letters, digits, special characters, minimum length - 8 characters)
8. date in the format YYYY-MM-DD
9. date in DD/MM/YYYY format
10. Date in DD.MM.YYYY format
11. Time in the format
12. Time in the format nn:mm
13. URL (http://yandex.ru/)
14. E-mail (user@maildomain.com)
15. Pv4 (94.137.192.81)
16. lPv6 (2001:0:9d38:6abd:c70:2d3c:a176:3398)
17. Mac-address (ec:23:3d:1b:7a:e7)
18. Russian cell phone number (+79021234567)
19. Credit card number (4048 4323 9889 3301)
20. TIN (3808753981 or 380870115601)
21. Zip code (664000)
22. Price in rubles (2546,10 руб.)
23. price in dollars ($39.99)

### Exercise 2.
Create functions using regular expressions:
1. From a file name (e.g., picture.jpg ), get its extension (e.g., jpg )
2. From the file name, check if it corresponds to: a) an archive, b) an audio file, c) a video file, d) a picture.
3. In an arbitrary NTMb code, find the string enclosed in tags <title></title>
4. In an arbitrary NTMbcode, find all links in <a> tags (hpef attribute).
5. In arbitrary NTM[-code find all references to pictures in tags <img> (attribute src)
6. In arbitrary text, find and highlight a given line using a tag <strong>
7. In arbitrary text, find a given set of text emoticons :), ;), :( to their corresponding pictures <img src="smile.png" alt=":)">, <img src="wink.png" alt=";)">, <img src="sad.png" alt=":(">
8. In a given string, get rid of random repeated spaces.


## Demonstration of work [regular.php](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/server-side_programming/04_regular_expressions/regular.php):

![1](1.png "1")