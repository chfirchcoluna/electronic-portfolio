<?php


function fitsPattern($pattern, $line)
{
    if (preg_match_all($pattern, $line)) {
        return 'True';
    }
    return 'False';
}


function isIntegerNumber($stringNumber)
{
    $pattern = '/^-?\d+$/';
    return fitsPattern($pattern, $stringNumber);
}


function isLatinAlphabetAndNumbers($lineLatinAndNumbers)
{
        $pattern = '/^[a-zA-Z0-9]*$/';
        return fitsPattern($pattern, $lineLatinAndNumbers);
}


echo 'Упражнение 1. ';
echo '<br>';
echo '1. Целое число ';
echo '1/2: ' . isIntegerNumber('1/2') . ' ';
echo '15: ' . isIntegerNumber('15') . ' ';
echo '5.5: ' . isIntegerNumber('5.5') . ' ';
echo '-15: ' . isIntegerNumber('-15') . ' ';
echo '<br>';

echo '2. Набор из букв и цифр (латиница) ';
echo '12test: ' . isLatinAlphabetAndNumbers('12test');
echo '<br>';

echo '3. Набор из букв и цифр (латиница + кириллица) ';
echo '1ёЁbBиИ: ' . fitsPattern('/^[a-zA-Zа-ЯА-Я\dёЁ]+$/', '1ёЁbBиИ');
echo '<br>';

echo '4. Домен (google.com): ';
echo fitsPattern('/^(([a-zA-Z\d][a-zA-Z\d\-]{0,61})?[a-zA-Z\d]\.)+[A-Za-z\d]{2,63}$/', 'google.com');
echo '<br>';

echo '5. Имя пользователя (с ограничением 3-25 символов, которыми могут быть буквы и цифры, первый символ обязательно буква) ';
echo 'Dima: ' . fitsPattern('/^[a-zA-Z][a-zA-Z\d]{2,24}$/', 'Dima');
echo '<br>';

echo '6. Пароль (строчные и прописные латинские буквы, цифры) ';
echo '1aA: ' . fitsPattern('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{3,}$/', '1aA');
echo '<br>';

echo '7. Пароль (строчные и прописные латинские буквы, цифры, спецсимволы, минимальная длина - 8 символов) ';
echo '1aA@aaaa: ' . fitsPattern('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$&_*^#!])[a-zA-Z\d@$&_*^#!]{8,}$/', '1aA@aaaa');
echo '<br>';

echo '8. Дата в формате YYYY-MM-DD ';
echo '1992-12-21: ' . fitsPattern('/^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|30|31)$/', '1992-12-21');
echo '<br>';

echo '9. Дата в формате DD/MM/YYYY ';
echo '21/12/1992: ' . fitsPattern('/^(0[1-9]|[12]\d|30|31)\/(0[1-9]|1[0-2])\/\d{4}$/', '21/12/1992');
echo '<br>';

echo '10. Дата в формате DD.MM.YYYY ';
echo '21.12.1992: ' . fitsPattern('/^(0[1-9]|[12]\d|30|31)\.(0[1-9]|1[0-2])\.\d{4}$/', '21.12.1992');
echo '<br>';

echo '11. Время в формате HH:MM:SS ';
echo '12:35:15: ' . fitsPattern('/^([01]\d|2[0-3])(:[0-5]\d){2}$/', '12:35:15');
echo '<br>';

echo '12. Время в формате HH:MM ';
echo '12:35: ' . fitsPattern('/^([01]\d|2[0-3])(:[0-5]\d)$/', '12:35');
echo '<br>';

echo '13. URL (http://yandex.ru/): ';
echo fitsPattern('/^(http)s?:\/\/(([a-zA-Z\d][a-zA-Z\d\-]{0,61})?[a-zA-Z\d]\.)+[A-Za-z\d]{2,63}\/([a-zA-Z\d]*\/)*([a-zA-Z\d]*\.)*[a-zA-Z\d]*$/', 'http://yandex.ru/');
echo '<br>';

echo '14. E-mail (user@maildomain.com): ';
echo fitsPattern('/^[a-zA-Z\d\.]{4,}@[a-zA-Z\d]+\.[a-zA-Z]{2,}$/', 'user@maildomain.com');
echo '<br>';

echo '15. IPv4 (94.137.192.81): ';
echo fitsPattern('/^((2[0-5][0-5]|1\d\d|[1-9]?\d)(\.|$)){4}/', '94.137.192.81');
echo '<br>';

echo '16. IPv6 (2001:0:9d38:6abd:c70:2d3c:a176:3398): ';
echo fitsPattern('/^(([a-f\d]|[a-f1-9][a-f\d]|[a-f1-9][a-f\d]{2}|[a-f1-9][a-f\d]{3})(:|$)){8}/', '2001:0:9d38:6abd:c70:2d3c:a176:3398');
echo '<br>';

echo '17. Mac-адрес (ec:23:3d:1b:7a:e7): ';
echo fitsPattern('/^(([a-f\d]{2})([:-]|$)){6}/', 'ec:23:3d:1b:7a:e7');
echo '<br>';

echo '18. Российский номер мобильного телефона (+79021234567): ';
echo fitsPattern('/^(\+7|8)\d{10}$/', '+79021234567');
echo '<br>';

echo '19. Номер кредитной карты (4048 4323 9889 3301): ';
echo fitsPattern('/^(\d{4}\s?){4}$/', '4048 4323 9889 3301');
echo '<br>';

echo '20. ИНН (3808753981 или 380870115601) ';
echo '3808753981: ' . fitsPattern('/^\d{10}|\d{12}$/', '3808753981') . ' ';
echo '380870115601: ' . fitsPattern('/^\d{10}|\d{12}$/', '380870115601');
echo '<br>';

echo '21. Почтовый индекс (664000): ';
echo fitsPattern('/^\d{6}$/', '664000');
echo '<br>';

echo '22. Цена в рублях (2546,10 руб.): ';
echo fitsPattern('/^([1-9]\d*|\d)(,\d+)?\sруб\.$/', '2546,10 руб.');
echo '<br>';

echo '23. Цена в долларах ($39.99): ';
echo fitsPattern('/^\$([1-9]\d*|\d)(.\d+)?$/', '$39.99');
echo '<br>';


echo 'Упражнение 2. ';
echo '<br>';


function extensionFile($file_name)
{
    $pattern = '/(?<=.)\w+$/';
    if (preg_match_all($pattern, $file_name, $matches)) {
        return $matches[0][0];
    }
    return 'False';
}


echo '1. Из имени файла (например, picture.jpg) получите его расширение (например, jpg) ';
echo '<br>';
echo 'picture.jpg: ';
echo '<br>';
echo extensionFile('picture.jpg');
echo '<br>';


function checkTypeFile($file_name)
{
    $extension = extensionFile($file_name);
    $extension = strtolower($extension);
    if (in_array($extension, array('bmp', 'ecw', 'gif', 'ico', 'ilbm', 'jpeg', 'jpg', 'pcx', 'png', 'psd', 'tga', 'tiff', 'jiff', 'webp', 'xbm', 'xps', 'rla', 'rpf', 'rnm'))) {
        return 'Picture';
    }
    else if (in_array($extension, array('mp3','avi','wma', 'cd', 'ac3', 'dts', 'aac', 'ogg', 'wav'))) {
        return 'Audio';
    }
    else if (in_array($extension, array('webm', 'mkv', 'avi', 'mp4', 'flv', 'wob', 'mov', 'vmw', 'amv', 'viv', 'asf', 'rm', 'qt'))) {
        return 'Video';
    }
    else if (in_array($extension, array('zip', 'rar', '7z', 'arj', 'tar', 'bin', 'gzip', 'jar', 'pak', 'pkg', 'tar-gz'))) {
        return 'Archive';
    }
    return 'None of them';
}


echo '2. По имени файла проверьте соответствует ли оно: а) архиву, б) аудиофайлу, в) видеофайлу, г) картинке ';
echo '<br>';
echo 'picture.JPG: ';
echo '<br>';
echo checkTypeFile('picture.JPG');
echo '<br>';


function findTitle($lineCode)
{
    $pattern = '/<title>(.*)<\/title>/';
    if (preg_match_all($pattern, $lineCode, $matches)) {
        return $matches[1][0];
    } else {
        return 'False';
    }
}


echo '3. В произвольном HTML-коде найдите строку, заключенную в теги <title></title> ';
echo '<br>';
echo 'tj293 jf9@%@e wfoaKsok1#R#OR@R @@frpl<title>Hello World!</title>fawa.#%#@^f afawa: ';
echo '<br>';
echo findTitle('tj293 jf9@%@e wfoaKsok1#R#OR@R @@frpl<title>Hello World!</title>fawa.#%#@^f afawa ');
echo '<br>';


function findHref($lineCode)
{
    $pattern = '/<a\s+[^>]*href=["\']([^"\'>]+)["\'][^>]*>/';
    if (preg_match_all($pattern, $lineCode, $matches)) {
        return implode(' ', $matches[1]);
    }
    return 'False';
}


echo '4. В произвольном HTML-коде найдите все ссылки в тегах <a> (атрибут href) ';
echo '<br>';
echo 'g8h89hf@8fh8safs<a href="http://vk.com">vk</a> WHFA faw8hf<a href="ya.ru">ya.ru</a>fwaF 2@d @ff i&$#&@$* f aw2: ';
echo '<br>';
echo findHref('g8h89hf@8fh8safs<a href="http://vk.com">vk</a> WHFA faw8hf<a href="ya.ru">ya.ru</a>fwaF 2@d @ff i&$#&@$* f aw2');
echo '<br>';


function findImgSrc($lineCode)
{
    $pattern = '/<img\s+[^>]*src=["\']([^"\'>]+)["\'][^>]*>/';
    if (preg_match_all($pattern, $lineCode, $matches)) {
        return implode(' ', $matches[1]);
    }
    return 'False';
}


echo '5. В произвольном HTML-коде найдите все ссылки на картинки в тегах <img> (атрибут src) ';
echo '<br>';
echo 'g8h89hf@8fh8safsfwaF <img src="smile.png" alt=":)">,faf3@Rf <img src="wink.png" alt=";)">2@d @ff i&$#&@$* f aw2: ';
echo '<br>';
echo findImgSrc('g8h89hf@8fh8safsfwaF <img src="smile.png" alt=":)">,faf3@Rf <img src="wink.png" alt=";)">2@d @ff i&$#&@$* f aw2');
echo '<br>';


function findUserString($someText, $userString)
{
    $pattern = '/' . $userString . '/';
    $replacement = '<strong>' . $userString . '</strong>';
    return preg_replace($pattern, $replacement, $someText);
}


echo '6. В произвольном тексте найдите и подсветите с помощью тега strong заданную строку ';
echo '<br>';
echo 'TryToFindIt: ';
echo '<br>';
echo findUserString('oofkOW@(@f kgok@T#(%$% !@%TryToFindItoeg koe3(TKFTryToFindIt#KTF', 'TryToFindIt');
echo '<br>';


function findEmojiAndReplaceToImage($inputText)
{
    $patterns = array(
        '/:\)/',
        '/;\)/',
        '/:\(/',
    );
    $replacements = array(
        '<img src="smile.png" alt=":)">',
        '<img src="wink.png" alt=";)">',
        '<img src="sad.png" alt=":(">',
    );
    return preg_replace($patterns, $replacements, $inputText);
}


echo '7. В произвольном тексте найдите определенный набор текстовых смайликов :), ;), :(на соответствующие им картинки img src="smile.png" alt=":)", img src="wink.png" alt=";)", img src="sad.png" alt=":(" ';
echo '<br>';
echo ':) ;) :(: ';
echo '<br>';
echo findEmojiAndReplaceToImage(':) ;) :(');
echo '<br>';


function replaceRandomSpaces($inputText)
{
    $pattern = '/\s+/';
    return preg_replace($pattern, ' ', $inputText);
}


echo '8. В заданной строке избавьтесь от случайных повторяющихся пробелов. ';
echo '<br>';
echo '1       2               3        4      5: ';
echo '<br>';
echo replaceRandomSpaces('1       2               3        4      5');
echo '<br>';

?>
