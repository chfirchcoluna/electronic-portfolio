## 05 Task:

### Exercise 1
Finalize the conference registration form (from the previous exercise).

1. Modify the script so that applications are saved not in separate files, but in one single file in the following format: one application per line with the application data listed with a delimiter.
2. As a delimiter you can use: |, ||, *, **.
3. The order of the data should be the same in all lines (determine in advance in what order the data will go). Imagine that you are saving the data as a table with columns and rows. Each row should end with \n or PHP_EOL.
4. Incoming data should definitely be checked for your delimiter and avoid it.
5. Don't forget to include the date of submission and the visitor's IP address in the data of each submission ($_SERVER['REMOTE_ADDR'] or getenv('REMOTE_ADDR') will come in handy).

### Exercise 2
1. Modify the admin so that all requests are read from one file, according to the format defined in the beginning, and presented as a table with an option to delete.
2. When deleting an application, do not delete it, just mark it as deleted. This will probably be an additional field in the data where the status of the application is placed. Requests marked as deleted should not appear in the list of requests.

## 06 Task:

1. Organize the counting of visitors to the applications page:
unique by IP,
users by session,
page loads (hits)
2. Close the administrative page for the conference application form with the administrator login and password.
3. Provide a logout (unlogging) function.
4. Use inactivity time to unlog a user automatically (e.g. after 5 minutes of inactivity).


## Demonstration of work [index.php](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/server-side_programming/05_&_06_string_functions_&_working_with_sessions/Form2/indexphp.php):

![1](1.png "1")
![2](2.png "2")
![3](3.png "3")