<?php

if ($_POST) {

    $username = htmlentities($_POST['username']);
    $surname = htmlentities($_POST['surname']);
    $email = htmlentities($_POST['email']);
    $phone = htmlentities($_POST['phone']);
    $interest = htmlentities($_POST['interest']);
    $paymentMethod = htmlentities($_POST['paymentMethod']);
    $receiveMailing = isset(($_POST['receiveMailing'])) ? 'Да' : 'Нет';
    $receiveTime = date('Y-m-d-H-i-s');

    if (empty($username) || empty($surname) || empty($email) || empty($phone) || empty($interest) || empty($paymentMethod)) {
        $message = 'Пожалуйста, заполните все обязательные поля!';
    } else {

        if (strlen($username) > 85 || strlen($surname) > 85 || strlen($email) > 85 || strlen($phone) > 85 || strlen($interest) > 85 || strlen($paymentMethod) > 85) {
            $message = 'Пожалуйста, не пишите больше чем 85 символов в ячейке!';
        } else {
            $filename = 'requests/requests.txt';
          
            if (!file_exists($filename)) {
                touch($filename);
            }
          
          	$ip = $_SERVER['REMOTE_ADDR'];

            $file = fopen($filename, 'a+');
          	fwrite($file, uniqid() . '|');
            fwrite($file, "$username|");
            fwrite($file, "$surname|");
            fwrite($file, "$email|");
          	fwrite($file, "$ip|");
            fwrite($file, "$phone|");
            fwrite($file, "$interest|");
            fwrite($file, "$paymentMethod|");
            fwrite($file, "$receiveMailing|");
            fwrite($file, "$receiveTime\n");
            fclose($file);
            $message = 'Ваша заявка успешно отправлена!';
        }
    }
    echo '<h2>' . $message . '</h2>';
}
?>