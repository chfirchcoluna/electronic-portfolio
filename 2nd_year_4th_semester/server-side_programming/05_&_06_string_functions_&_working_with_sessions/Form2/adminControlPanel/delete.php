<?php


function touchFileIfNotExists($filename) {
    if (!file_exists($filename)) {
        touch($filename);
    }
}


$lifeTimeSession = 300;
$adminFilename = 'adminLog.txt';
touchFileIfNotExists($adminFilename);
$datefile = file($adminFilename, FILE_IGNORE_NEW_LINES);
if ($datefile[0] !== '') {
    if (strtotime(date('Y-m-d H:i:s', time())) - strtotime($datefile[0]) > $lifeTimeSession) {
				$_SESSION['authentification'] = false;
      	header("Location: logoutAdmin.php");
      	exit;
    }
}

if (isset($_POST['delete'])) {
    $to_delete = $_POST['delete'];
  
    $file = '../requests/requests.txt';

    $lines = file($file, FILE_IGNORE_NEW_LINES);

    for ($i = 0; $i < count($lines) ; $i++) {
        $data = explode('|', $lines[$i]);
        $id = $data[0];
        if (in_array($id, $to_delete)) {
            $lines[$i] = $id . '|' . implode('|', array_slice($data, 1)) . '|deleted';
        }
    }

    file_put_contents($file, [implode(PHP_EOL, array_filter($lines)), "\n"]);
}
header("Location: admin.php");
?>