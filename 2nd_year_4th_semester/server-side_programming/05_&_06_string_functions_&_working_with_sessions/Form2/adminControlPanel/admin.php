<?php
session_start();


function touchFileIfNotExists($filename) {
    if (!file_exists($filename)) {
        touch($filename);
    }
}


if (!isset($_SESSION['authentification'])) {
    header('Location: logoutAdmin.php');
    exit;
}


$lifeTimeSession = 300;
$adminFilename = 'adminLog.txt';
touchFileIfNotExists($adminFilename);
$datefile = file($adminFilename, FILE_IGNORE_NEW_LINES);
if ($datefile[0] !== '') {
    if (strtotime(date('Y-m-d H:i:s', time())) - strtotime($datefile[0]) > $lifeTimeSession) {
				$_SESSION['authentification'] = false;
      	header("Location: logoutAdmin.php");
      	exit;
    }
}
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Страница администратора</title>
</head>
<body>

<h1>Список заявок</h1>

<form action="delete.php" method="POST">
    <table>
        <tr>
            <th>ID</th>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Email</th>
          	<th>IP</th>
            <th>Телефон</th>
            <th>Тематика</th>
            <th>Метод оплаты</th>
            <th>Рассылка о конференции</th>
            <th>Время отправки заявки</th>
            <th>Удалить</th>
        </tr>
        <?php
        $filename = '../requests/requests.txt';
				$lines = file($filename, FILE_IGNORE_NEW_LINES);
        foreach ($lines as $line) {
            $data = explode("|", $line);
            if (end($data) !== 'deleted') {
            		echo '<tr>';
            		for ($i=0; $i < count($data); $i++) {
                		echo '<td>' . $data[$i] . '</td>';
            		}
            		echo '<td><input type="checkbox" name="delete[]" value="' . $data[0] . '"></td>';
            		echo '</tr>';
        		}
        }
        ?>
    </table>
    <br>
    <input type="submit" name="submit" value="Удалить">
</form>
<form action="logoutAdmin.php">
		<input type="submit" value="Выход">
</form>
</body>
</html>