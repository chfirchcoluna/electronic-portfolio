## 00 Task:

1. Create your first hosted website, coming up with a domain name for it.
2. Check access to the site from your browser.
3. Upload additional files of different types to the server: images, documents, etc., and then upload them to the server.
4. Check access to these files using the developer panel, examine HTTP request and response headers.
5. Use the developer panel to examine header information about other sites on the Internet.
6. Make a report about HTTP headers: what they are in requests and responses, what values they take (in general and in particular).

## Demonstration of work [headers.html](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/server-side_programming/00_getting_to_know_HTTP/headers.html):

![1](1.png "1")