## 07 Task:

1. Update the conference registration form by adding saving data to MySQL database instead of a plain text file.
2. Also fix the admin area by adding reading and deleting requests from MySQL database.

## Demonstration of work [index.php](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/server-side_programming/07_working_with_the_database/Form3/indexphp.php):

![1](1.png "1")
![2](2.png "2")
![3](3.png "3")
![4](4.png "4")