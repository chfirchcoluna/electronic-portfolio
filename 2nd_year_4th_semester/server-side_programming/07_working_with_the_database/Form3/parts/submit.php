<?php
$servername = "127.0.0.1";
$username = "chfirchco19";
$password = "zdbmsW8h";
$dbname = "chfirchco19";

if ($_POST) {
  
  	try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(PDOException $e) {
        $message = 'Ошибка подключения: ' . $e->getMessage();
    }

    $username = htmlentities($_POST['username']);
    $surname = htmlentities($_POST['surname']);
    $email = htmlentities($_POST['email']);
    $phone = htmlentities($_POST['phone']);
    $subject_id = htmlentities($_POST['interest']);
    $payment_id = htmlentities($_POST['paymentMethod']);
    $mailing = isset(($_POST['receiveMailing'])) ? 1 : 0;
  
  	$ip = $_SERVER['REMOTE_ADDR'];
  
  	/**
     * Доступные темы конференции
     * @var Array
     */
  	$subjects = array();
    $subjects['Бизнес'] = 1;
  	$subjects['Технологии'] = 2;
  	$subjects['Реклама и маркетинг'] = 3;
  	$subjects['Проектирование'] = 4;

  	$subject_id = $subjects[$subject_id];

    /**
     * Доступные методы оплаты
     * @var Array
     */
  	$payments = array();
    $payments['WebMoney'] = 1;
    $payments['Яндекс.Деньги'] = 2;
    $payments['PayPal'] = 3;
    $payments['Кредитная карта'] = 4;
    $payments['Робокасса'] = 5;

  	$payment_id = $payments[$payment_id];
  
  	if (empty($username) || empty($surname) || empty($email) || empty($phone) || empty($subject_id) || empty($payment_id)) {
        $message = 'Пожалуйста, заполните все обязательные поля!';
    } else {
  			if (strlen($username) > 255 || strlen($surname) > 255 || strlen($email) > 255 || strlen($phone) > 255 || strlen($subject_id) > 255 || strlen($payment_id) > 255) {
            $message = 'Пожалуйста, не пишите больше чем 255 символов в ячейке!';
        } else {
            $sql = "INSERT INTO participants (username, surname, email, ip, phone, subject_id, payment_id, mailing, created_at, updated_at)
                    VALUES (:username, :surname, :email, :ip, :phone, :subject_id, :payment_id, :mailing, NOW(), NOW())";
            try {
                $pstmt = $conn->prepare($sql);
                $pstmt->bindParam(':username', $username);
                $pstmt->bindParam(':surname', $surname);
                $pstmt->bindParam(':email', $email);
              	$pstmt->bindParam(':ip', $ip);
                $pstmt->bindParam(':phone', $phone);
                $pstmt->bindParam(':subject_id', $subject_id);
                $pstmt->bindParam(':payment_id', $payment_id);
                $pstmt->bindParam(':mailing', $mailing);
                $pstmt->execute();

                $message = 'Ваша заявка успешно отправлена!';
            } catch(PDOException $e) {
                $message = 'Ошибка: ' . $e->getMessage();
            }
        }
    }
  	echo '<h2>' . $message . '</h2>';
}
$conn = null;
?>