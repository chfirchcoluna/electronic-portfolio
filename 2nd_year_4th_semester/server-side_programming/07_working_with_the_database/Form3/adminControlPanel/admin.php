<?php
session_start();


function touchFileIfNotExists($filename) {
    if (!file_exists($filename)) {
        touch($filename);
    }
}


if (!isset($_SESSION['authentification'])) {
    header('Location: logoutAdmin.php');
    exit;
}


$lifeTimeSession = 300;
$adminFilename = 'adminLog.txt';
touchFileIfNotExists($adminFilename);
$datefile = file($adminFilename, FILE_IGNORE_NEW_LINES);
if ($datefile[0] !== '') {
    if (strtotime(date('Y-m-d H:i:s', time())) - strtotime($datefile[0]) > $lifeTimeSession) {
				$_SESSION['authentification'] = false;
      	header("Location: logoutAdmin.php");
      	exit;
    }
}
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Страница администратора</title>
</head>
<body>

<h1>Список заявок</h1>

<form action="delete.php" method="POST">
    <table>
        <tr>
            <th>ID</th>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Email</th>
          	<th>IP</th>
            <th>Телефон</th>
            <th>Тематика</th>
            <th>Метод оплаты</th>
            <th>Рассылка о конференции</th>
            <th>Время отправки заявки</th>
          	<th>Время обновления</th>
            <th>Удалить</th>
        </tr>
        <?php
				$servername = "127.0.0.1";
        $username = "chfirchco19";
        $password = "zdbmsW8h";
        $dbname = "chfirchco19";

				try {
            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            $message = 'Ошибка подключения: ' . $e->getMessage();
        }

        $sql = "SELECT * FROM participants";
        $qstmt = $conn->query($sql);

        foreach ($qstmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
          	if (is_null($row['deleted_at'])) {
                echo '<tr>';
                echo '<td>' . $row['id'] . '</td>';
                echo '<td>' . $row['username'] . '</td>';
                echo '<td>' . $row['surname'] . '</td>';
                echo '<td>' . $row['email'] . '</td>';
                echo '<td>' . $row['ip'] . '</td>';
                echo '<td>' . $row['phone'] . '</td>';
                echo '<td>' . $row['subject_id'] . '</td>';
                echo '<td>' . $row['payment_id'] . '</td>';
                echo '<td>' . ($row['mailing'] == 1 ? 'Да' : 'Нет') . '</td>';
                echo '<td>' . $row['created_at'] . '</td>';
                echo '<td>' . $row['updated_at'] . '</td>';
                echo '<td><input type="checkbox" name="delete[]" value="' .  $row['id'] . '"></td>';
                echo '</tr>';
            }
        }
				/*
        $filename = '../requests/requests.txt';
				$lines = file($filename, FILE_IGNORE_NEW_LINES);
        foreach ($lines as $line) {
            $data = explode("|", $line);
            if (end($data) !== 'deleted') {
            		echo '<tr>';
            		for ($i=0; $i < count($data); $i++) {
                		echo '<td>' . $data[$i] . '</td>';
            		}
            		echo '<td><input type="checkbox" name="delete[]" value="' . $data[0] . '"></td>';
            		echo '</tr>';
        		}
        }
        */
        ?>
    </table>
    <br>
    <input type="submit" name="submit" value="Удалить">
</form>
<form action="logoutAdmin.php">
		<input type="submit" value="Выход">
</form>
</body>
</html>