<?php


function touchFileIfNotExists($filename) {
    if (!file_exists($filename)) {
        touch($filename);
    }
}


function countHits() {
  	$hitsFilename = 'hits.txt';
    if (!file_exists($hitsFilename)) {
        touch($hitsFilename);
        $hits = 0;
    }
    else {
        $hits = file_get_contents($hitsFilename);
    }
    $hits++;
    file_put_contents($hitsFilename, $hits);
  	return $hits;
}


function showUniqueIPs($guestsFilename) {
  	$uniqueIps = array();
    $lines = file($guestsFilename, FILE_IGNORE_NEW_LINES);
    echo 'Уникальные посетители по IP: ';
    for ($i = 0; $i < count($lines); $i++) {
        $data = explode('|', $lines[$i]);
        $uniqueIps[] = $data[0];
    }
    $uniqueIps = array_unique($uniqueIps);
    for ($i = 0; $i < count($uniqueIps); $i++) {
        echo $uniqueIps[$i] . ' ';
		}
}


function countNumberActiveSessions($guestsFilename) {
    $lines = file($guestsFilename, FILE_IGNORE_NEW_LINES);
    $numberActiveSessions = 0;
    for ($i = 0; $i < count($lines); $i++) {
        $data = explode('|', $lines[$i]);
        if (end($data) !== 'expired') {
            $numberActiveSessions++;
        }
    }
  	return $numberActiveSessions;
}


function checkExpiredSessions($lifeTimeSession, $guestsFilename) {
    $current_date = date('Y-m-d H:i:s', time());
    $isHaveToPut = false;
    $lines = file($guestsFilename, FILE_IGNORE_NEW_LINES);
    for ($i = 0; $i < count($lines); $i++) {
        $data = explode('|', $lines[$i]);
        if (strtotime($current_date) - strtotime($data[2]) > $lifeTimeSession && end($data) !== 'expired') {
            $lines[$i] = $lines[$i] . '|' . 'expired';
            $isHaveToPut = true;
        }
    }

    if ($isHaveToPut) {
      file_put_contents($guestsFilename, implode(PHP_EOL, $lines) . PHP_EOL, LOCK_EX);
    }  
}


function enterNewInfoAboutSessions($guestsFilename) {
  	$ip = $_SERVER['REMOTE_ADDR'];
		$sid = session_id();
    $lines = file($guestsFilename, FILE_IGNORE_NEW_LINES);
    $isHaveToPut = true;
    for ($i = 0; $i < count($lines); $i++) {
        $data = explode('|', $lines[$i]);
        if ($sid === $data[1]) {
            $lines[$i] = $ip . '|' . $sid . '|' . date('Y-m-d H:i:s', time());
            file_put_contents($guestsFilename, implode(PHP_EOL, $lines) . PHP_EOL, LOCK_EX);
            $isHaveToPut = false;
            break;
        }
    }

    if ($isHaveToPut) {
        file_put_contents($guestsFilename, $ip . '|' . $sid . '|' . date('Y-m-d H:i:s', time()) . PHP_EOL, FILE_APPEND | LOCK_EX);	
    }  
}


function body() {
    // seconds
    $lifeTimeSession = 300;

    session_start();
    $guestsFilename = 'guests.txt';
    touchFileIfNotExists($guestsFilename);
    enterNewInfoAboutSessions($guestsFilename);
    checkExpiredSessions($lifeTimeSession, $guestsFilename);

    echo 'Количество загрузок страницы: ' . countHits() . '<br>';
    echo 'Активных сессий: ' . countNumberActiveSessions($guestsFilename) . '<br>';
    showUniqueIPs($guestsFilename);
}
  

body();

?>