<?php


function touchFileIfNotExists($filename) {
    if (!file_exists($filename)) {
        touch($filename);
    }
}


$lifeTimeSession = 300;
$adminFilename = 'adminLog.txt';
touchFileIfNotExists($adminFilename);
$datefile = file($adminFilename, FILE_IGNORE_NEW_LINES);
if ($datefile[0] !== '') {
    if (strtotime(date('Y-m-d H:i:s', time())) - strtotime($datefile[0]) > $lifeTimeSession) {
				$_SESSION['authentification'] = false;
      	header("Location: logoutAdmin.php");
      	exit;
    }
}

if (isset($_POST['delete'])) {
    $to_delete = $_POST['delete'];
  
  	$servername = "127.0.0.1";
    $username = "chfirchco19";
    $password = "zdbmsW8h";
    $dbname = "chfirchco19";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(PDOException $e) {
        $message = 'Ошибка подключения: ' . $e->getMessage();
    }
  
  	$sql = "UPDATE `participants` SET deleted_at = NOW(), updated_at = NOW() WHERE id IN (:to_delete)";
  	
      	
    try {
    		$pstmt = $conn->prepare($sql);
        $pstmt->bindParam(':to_delete', implode(', ', $to_delete));
        $pstmt->execute();

    } catch(PDOException $e) {
        $message = 'Ошибка: ' . $e->getMessage();
    }

		$conn = null;
}
header("Location: admin.php");
?>