<?php


function touchFileIfNotExists($filename) {
    if (!file_exists($filename)) {
        touch($filename);
    }
}


session_start();

if (isset($_SESSION['authentification'])) {
    header('Location: admin.php');
    exit;
}

$login = 'admin';
$password = 'admin';

if (isset($_POST['submit'])) {

    $login_input = $_POST['login'];
    $password_input = $_POST['password'];
    if ($login_input == $login && $password_input == $password) {
        $_SESSION['authentification'] = true;
      	$adminFilename = 'adminLog.txt';
      	touchFileIfNotExists($adminFilename);
      	file_put_contents($adminFilename, date('Y-m-d H:i:s', time()) . PHP_EOL, LOCK_EX);
        header('Location: admin.php');
        exit;
    } else {
        $error = 'Неверный логин или пароль';
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Авторизация администратора</title>
</head>
<body>
    <h2>Авторизация администратора</h2>
    <?php if (isset($error)): ?>
        <p style="color: red;"><?php echo $error; ?></p>
    <?php endif; ?>
    <form method="post">
        <label>Логин:</label><br>
        <input type="text" name="login"><br>
        <label>Пароль:</label><br>
        <input type="password" name="password"><br><br>
        <input type="submit" name="submit" value="Войти">
    </form>
</body>
</html>