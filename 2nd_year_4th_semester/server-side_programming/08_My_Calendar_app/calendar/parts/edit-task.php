<?php

namespace app;

session_start();

if (!isset($_SESSION['username'])) {
    header("Location: authorization.php");
    exit;
}

require_once('../register/register-in.php');
require_once('../header/header-in.php');

$controller = new Controller();
$view = new View();
$controller->setView($view);
if (isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    $id = null;
}

$controller->connectToDatabase('config.php');
$task = $controller->getEditTask($id);
?>

<h1>Мой календарь</h1>

<h2>Редактировать задачу</h2>

<body>
    <form name="edit_task" method="POST">
        <label for="theme">Тема:*</label>
        <input type="text" id="theme" name="theme" value="<?= ($task['theme'] ?? '') ?>" required><br>

        <label for="category">Тип:*</label>
        <select id="category" name="category">
            <option value="Встреча" <?php if (($_SESSION['category'] ?? '') === 'Встреча') {
                                        echo 'selected';
                                    } ?>>Встреча</option>
            <option value="Звонок" <?php if (($_SESSION['category'] ?? '') === 'Звонок') {
                                        echo 'selected';
                                    } ?>>Звонок</option>
            <option value="Совещание" <?php if (($_SESSION['category'] ?? '') === 'Совещание') {
                                            echo 'selected';
                                        } ?>>Совещание</option>
            <option value="Дело" <?php if (($_SESSION['category'] ?? '') === 'Дело') {
                                        echo 'selected';
                                    } ?>>Дело</option>
        </select><br>

        <label for="place">Место:*</label>
        <input type="text" id="place" name="place" value="<?= ($task['place'] ?? '') ?>" required><br>

        <label for="date">Дата и время:*</label>
        <input type="datetime-local" id="date" name="date" value="<?= ($task['date'] ?? '') ?>" required><br>

        <label for="deadline">Истечение в:*</label>
        <input type="datetime-local" id="deadline" name="deadline" value="<?= ($task['deadline'] ?? '') ?>" required><br>

        <label for="description">Комментарий:*</label>
        <input type="text" id="description" name="description" value="<?= ($task['description'] ?? '') ?>" required><br>

        <label for="completed">Выполнить:</label>
        <input type="checkbox" id="completed" name="completed" value=""><br>

        <input name="submit" type="submit" value="Редактировать">
    </form>

    <form name="showtable" method="POST">
        <label for="taskcategory">Тип:*</label>
        <select id="taskcategory" name="taskcategory">
            <option value="Текущие задачи" <?php if (($_POST['taskcategory'] ?? '') === 'Текущие задачи') {
                                                echo 'selected';
                                            } ?>>Текущие задачи</option>
            <option value="Просроченные задачи" <?php if (($_POST['taskcategory'] ?? '') === 'Просроченные задачи') {
                                                    echo 'selected';
                                                } ?>>Просроченные задачи</option>
            <option value="Выполненные задачи" <?php if (($_POST['taskcategory'] ?? '') === 'Выполненные задачи') {
                                                    echo 'selected';
                                                } ?>>Выполненные задачи</option>
            <option value="Задачи по дате" <?php if (($_POST['taskcategory'] ?? '') === 'Задачи по дате') {
                                                echo 'selected';
                                            } ?>>Задачи по дате</option>
        </select><br>

        <label for="taskdate">Дата и время:</label>
        <input type="datetime-local" id="taskdate" name="taskdate" value="<?php if (!isset($_POST['taskdate'])) {
                                                                                echo date('Y-m-d h:i', time());
                                                                            } else {
                                                                                echo $_POST['taskdate'];
                                                                            } ?>"><br>

        <input name="showsubmit" type="submit" value="Показать календарь">
    </form>
</body>

<a href="../index.php">Новая задача</a>
<a href="exit.php">Выход</a>

<?php
require_once('edit-show-submit.php');
require_once('edit-submit.php');
require_once('../footer/footer.php');
?>