<?php

namespace app;

session_start();

require_once('../register/register-in.php');

if (isset($_SESSION['username'])) {
    header("Location: ../index.php");
    exit();
}

if (isset($_POST['submit'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $controller = new Controller();
    $controller->connectToDatabase('../parts/config.php');
    $isValidate = $controller->isValidateRegData($username, $password);

    if ($isValidate) {
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
        $controller->registerNewUser($username, $hashedPassword);
        header("Location: authorization.php");
        exit();
    } else {
        echo "Пользователь существует!";
    }
}
?>

<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>Registration App My Calendar</title>
    <link rel="stylesheet" href="../css/authorization-style.css">
</head>

<head>
    <title>Регистрация</title>
</head>

<body>
    <h2>Регистрация</h2>
    <form method="POST">
        <label for="username">Имя пользователя:*</label>
        <input type="text" id="username" name="username" value="<?= ($_POST['username'] ?? '') ?>" required><br>

        <label for="password">Пароль:*</label>
        <input type="password" id="password" name="password" value="<?= ($_POST['password'] ?? '') ?>" required><br>

        <input type="submit" name="submit" value="Зарегистрироваться">
        <a href="authorization.php">Авторизироваться</a>
    </form>
</body>

</html>