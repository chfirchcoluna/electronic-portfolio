<?php

namespace app;

session_start();

require_once('../register/register-in.php');

if (isset($_SESSION['username'])) {
    header("Location: ../index.php");
    exit();
}

if (isset($_POST['submit'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $controller = new Controller();
    $controller->connectToDatabase('../parts/config.php');
    $isValidateArray = $controller->isValidateLoginData($username, $password);
    $isValidate = $isValidateArray[0];
    $userId = $isValidateArray[1];

    if ($isValidate) {
        $_SESSION['username'] = $username;
        $_SESSION['user_id'] = $userId;
        header("Location: ../index.php");
        exit();
    } else {
        echo "Логин или пароль неверны";
    }
}
?>

<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>Authorization App My Calendar</title>
    <link rel="stylesheet" href="../css/authorization-style.css">
</head>

<html>

<head>
    <title>Авторизация</title>
</head>

<body>
    <h2>Авторизация</h2>
    <form method="POST">
        <label for="username">Имя пользователя:*</label>
        <input type="text" id="username" name="username" value="<?= ($_POST['username'] ?? '') ?>" required><br>

        <label for="password">Пароль:*</label>
        <input type="password" id="password" name="password" value="<?= ($_POST['password'] ?? '') ?>" required><br>

        <input type="submit" name="submit" value="Войти">
        <a href="registration.php">Зарегистрироваться</a>
    </form>
</body>

</html>