<?php

namespace app;


if (isset($_POST['showsubmit'])) {
    $controller->connectToDatabase();
    $_SESSION['taskcategory'] = $_POST['taskcategory'];
    $_SESSION['taskdate'] = $_POST['taskdate'];
    if ($_POST['taskcategory'] === 'Текущие задачи') {
        $controller->getDataFromTableTask('parts/');
    } else if ($_POST['taskcategory'] === 'Просроченные задачи') {
        $controller->getOverdueDataFromTableTask('parts/');
    } else if ($_POST['taskcategory'] === 'Выполненные задачи') {
        $controller->getCompletedDataFromTableTask('parts/');
    } else if ($_POST['taskcategory'] === 'Задачи по дате') {
        $controller->getByDateDataFromTableTask('parts/', $_POST['taskdate']);
    }
} else {
    $controller->connectToDatabase();
    $controller->getDataFromTableTask('parts/');
}
