<?php

namespace app;


if (isset($_POST['submit'])) {
    $_SESSION['theme'] = $_POST['theme'];
    $_SESSION['category'] = $_POST['category'];
    $_SESSION['place'] = $_POST['place'];
    $_SESSION['date'] = $_POST['date'];
    $_SESSION['deadline'] = $_POST['deadline'];
    $_SESSION['description'] = $_POST['description'];
    if (isset($_POST['completed'])) {
        $_SESSION['completed'] = 1;
    } else {
        $_SESSION['completed'] = 0;
    }
    
    $controller->updateTaskData($id);
    header("Location: ../index.php");
    exit();
}
