<?php

namespace app;


if (isset($_POST['showsubmit'])) {
    $_SESSION['taskcategory'] = $_POST['taskcategory'];
    $_SESSION['taskdate'] = $_POST['taskdate'];
    if ($_POST['taskcategory'] === 'Текущие задачи') {
        $controller->getDataFromTableTask('');
    } else if ($_POST['taskcategory'] === 'Просроченные задачи') {
        $controller->getOverdueDataFromTableTask('');
    } else if ($_POST['taskcategory'] === 'Выполненные задачи') {
        $controller->getCompletedDataFromTableTask('');
    } else if ($_POST['taskcategory'] === 'Задачи по дате') {
        $controller->getByDateDataFromTableTask('', $_POST['taskdate']);
    }
}
else {
    $controller->getDataFromTableTask('');
}
