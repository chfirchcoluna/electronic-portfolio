<?php

namespace app;

use PDO;

class View
{
    public function __construct()
    {
    }

    public function showTableTask($pstmt, $path)
    {
        echo '<table>';
        echo '<tr>';
        echo '<th>№</th>';
        echo '<th>Тема:</th>';
        echo '<th>Тип:</th>';
        echo '<th>Место:</th>';
        echo '<th>Дата</th>';
        echo '<th>Срок дедлайна</th>';
        echo '<th>Комментарий</th>';
        echo '</tr>';
        $counter = 1;
        foreach ($pstmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
            $task_id = $row['id'];
            echo '<tr>';
            echo '<td>' . $counter++ . '</td>';
            echo '<td>' . '<a href="' . $path . 'edit-task.php?id=' . $task_id . '">' . $row['theme'] . '</a>' . '</td>';
            echo '<td>' . $row['category'] . '</td>';
            echo '<td>' . $row['place'] . '</td>';
            echo '<td>' . $row['date'] . '</td>';
            echo '<td>' . $row['deadline'] . '</td>';
            echo '<td>' . $row['description'] . '</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
}
