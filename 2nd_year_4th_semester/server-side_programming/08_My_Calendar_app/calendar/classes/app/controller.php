<?php

namespace app;

use PDO;


class Controller
{
    private $servername;
    private $dbname;
    private $username;
    private $password;
    private $conn;
    private $pstmt;
    private $view;
    private $theme;
    private $category;
    private $place;
    private $date;
    private $deadline;
    private $description;
    private $userId;
    private $completed;

    public function __construct()
    {
        $this->servername = null;
        $this->dbname = null;
        $this->username = null;
        $this->password = null;
        $this->conn = null;
        $this->pstmt = null;
    }

    protected function loadSendedTaskData()
    {
        $this->theme = htmlentities($_POST['theme']);
        $this->category = htmlentities($_POST['category']);
        $this->place = htmlentities($_POST['place']);
        $this->date = htmlentities($_POST['date']);
        $this->deadline = htmlentities($_POST['deadline']);
        $this->description = htmlentities($_POST['description']);
        $this->completed = htmlentities($_SESSION['completed']);
    }

    public function isValidateRegData($username, $password)
    {
        if (!is_null($this->conn)) {
            return $this->conn->checkIsValidateRegData($username, $password);
        } else {
            echo 'Warning: Connection does not exist';
            return false;
        }
    }

    public function isValidateLoginData($username, $password)
    {
        if (!is_null($this->conn)) {
            return $this->conn->checkIsValidateLoginData($username, $password);
        } else {
            echo 'Warning: Connection does not exist';
            return [false, null];
        }
    }

    public function registerNewUser($username, $password)
    {
        if (!is_null($this->conn)) {
            return $this->conn->insertNewUser($username, $password);
        } else {
            echo 'Warning: Connection does not exist';
            return false;
        }
    }

    public function setView($view_obj)
    {
        $this->view = $view_obj;
    }

    public function connectToDatabase($pathToCongigFile = 'parts/config.php')
    {
        if (is_null($this->username)) {
            $config = require_once $pathToCongigFile;
            $this->servername = $config['host'];
            $this->dbname = $config['dbname'];
            $this->username = $config['username'];
            $this->password = $config['password'];
            if (isset($_SESSION['user_id'])) {
                $this->userId = $_SESSION['user_id'];
            }
        }

        if (is_null($this->conn)) {
            $this->conn = new MyDatabase($this->servername, $this->dbname, $this->username, $this->password);
        } else {
            echo 'Warning: Connection already exist';
        }
    }

    protected function makeSQLQueryInsert()
    {
        if (!is_null($this->conn)) {
            $this->conn->insertDataToTaskTable($this->theme, $this->category, $this->place, $this->date, $this->deadline, $this->description, $this->userId);
        }
    }

    protected function makeSQLQueryUpdate($id)
    {
        if (!is_null($this->conn)) {
            $this->conn->updateDataToTaskTable($id, $this->theme, $this->category, $this->place, $this->date, $this->deadline, $this->description, $this->userId, $this->completed);
        }
    }

    public function sendTaskData()
    {
        if (isset($_POST['theme'])) {
            $this->loadSendedTaskData();
            $this->makeSQLQueryInsert();
        }
    }

    public function updateTaskData($id)
    {
        if (isset($_POST['theme'])) {
            $this->loadSendedTaskData();
            $this->makeSQLQueryUpdate($id);
        }
    }

    public function getDataFromTableTask($path)
    {
        if (!is_null($this->conn)) {
            $this->conn->selectFromTaskTable($this->userId);
            $this->pstmt = $this->conn->getPreparedStatement();
            $this->view->showTableTask($this->pstmt, $path);
        } else {
            echo 'Error: Connection does not exist';
        }
    }

    public function getOverdueDataFromTableTask($path)
    {
        if (!is_null($this->conn)) {
            $this->conn->selectOverdueDataFromTaskTable($this->userId);
            $this->pstmt = $this->conn->getPreparedStatement();
            $this->view->showTableTask($this->pstmt, $path);
        } else {
            echo 'Error: Connection does not exist';
        }
    }

    public function getCompletedDataFromTableTask($path)
    {
        if (!is_null($this->conn)) {
            $this->conn->selectCompletedDataFromTaskTable($this->userId);
            $this->pstmt = $this->conn->getPreparedStatement();
            $this->view->showTableTask($this->pstmt, $path);
        } else {
            echo 'Error: Connection does not exist';
        }
    }

    public function getByDateDataFromTableTask($path, $date)
    {
        if (!is_null($this->conn)) {
            $this->conn->selectByDateDataFromTaskTable($this->userId, $date);
            $this->pstmt = $this->conn->getPreparedStatement();
            $this->view->showTableTask($this->pstmt, $path);
        } else {
            echo 'Error: Connection does not exist';
        }
    }

    public function getEditTask($id)
    {
        if (!is_null($this->conn)) {
            $this->conn->selectToEditFromTaskTable($id, $this->userId);
            $this->pstmt = $this->conn->getPreparedStatement();
            return $this->pstmt->fetch(PDO::FETCH_ASSOC);
        } else {
            echo 'Error: Connection does not exist';
            return null;
        }
    }

    public function dropConnectionDatabase()
    {
        $this->conn = null;
    }
}
