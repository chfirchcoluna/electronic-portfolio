<?php

namespace app;

use PDO;
use PDOException;

class MyDatabase
{
    private $conn;
    private $pstmt;

    public function __construct($servername,  $dbname, $username, $password)
    {
        $this->connectToDatabase($servername,  $dbname, $username, $password);
        $this->pstmt = NULL;
    }

    protected function connectToDatabase($servername,  $dbname, $username, $password)
    {
        try {
            $this->conn = new PDO("mysql:host=$servername", $username, $password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->createDatabaseCalendar();
        } catch (PDOException $e) {
            die("Error connecting: " . $e->getMessage());
        }
    }

    protected function createDatabaseCalendar()
    {
        $sql = "CREATE DATABASE IF NOT EXISTS `chfirchco19`; USE `chfirchco19`;";
        try {
            $this->pstmt = $this->conn->prepare($sql);
            $this->pstmt->execute();
            $this->createTableUser();
            $this->createTableTask();
        } catch (PDOException $e) {
            die('Error creating database: ' . $e->getMessage());
            exit();
        }
    }

    protected function createTableTask()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `task` (
            `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `theme` VARCHAR(255) NOT NULL DEFAULT '',
            `category` ENUM('Встреча', 'Звонок', 'Совещание', 'Дело') NOT NULL,
            `place` VARCHAR(255) NOT NULL DEFAULT '',
            `date` DATETIME NOT NULL DEFAULT NOW(),
            `deadline` DATETIME NOT NULL DEFAULT NOW(),
            `description` VARCHAR(255) NOT NULL DEFAULT '',
            `user_id` INT(10) UNSIGNED NOT NULL,
            `completed` TINYINT(1) NOT NULL DEFAULT 0,
            `created_at` DATETIME NOT NULL DEFAULT NOW(),
            `updated_at` DATETIME NOT NULL DEFAULT NOW(),
            `deleted_at` DATETIME DEFAULT NULL,
            PRIMARY KEY (`id`),
            FOREIGN KEY (`user_id`)  REFERENCES `user` (`id`)
        );";
        try {
            $this->pstmt = $this->conn->prepare($sql);
            $this->pstmt->execute();
        } catch (PDOException $e) {
            die('Error creating table: ' . $e->getMessage());
            exit();
        }
    }

    public function selectFromTaskTable($userId)
    {
        $sql = "SELECT `id`, `theme`, `category`, `place`, `date`, `deadline`, `description` FROM `task` WHERE `user_id` = :user_id AND `completed` = 0 AND `deadline` > NOW() AND `deleted_at` IS NULL;";
        try {
            $this->pstmt = $this->conn->prepare($sql);
            $this->pstmt->bindParam(':user_id', $userId);
            $this->pstmt->execute();
        } catch (PDOException $e) {
            die('Error parsing: ' . $e->getMessage());
            exit();
        }
    }

    public function selectOverdueDataFromTaskTable($userId)
    {
        $sql = "SELECT `id`, `theme`, `category`, `place`, `date`, `deadline`, `description` FROM `task`WHERE `user_id` = :user_id AND `completed` = 0 AND `deadline` < NOW() AND `deleted_at` IS NULL;";
        try {
            $this->pstmt = $this->conn->prepare($sql);
            $this->pstmt->bindParam(':user_id', $userId);
            $this->pstmt->execute();
        } catch (PDOException $e) {
            die('Error parsing: ' . $e->getMessage());
            exit();
        }
    }

    public function selectCompletedDataFromTaskTable($userId)
    {
        $sql = "SELECT `id`, `theme`, `category`, `place`, `date`, `deadline`, `description` FROM `task` WHERE `user_id` = :user_id AND `completed` = 1 AND `deleted_at` IS NULL;";
        try {
            $this->pstmt = $this->conn->prepare($sql);
            $this->pstmt->bindParam(':user_id', $userId);
            $this->pstmt->execute();
        } catch (PDOException $e) {
            die('Error parsing: ' . $e->getMessage());
            exit();
        }
    }

    public function selectByDateDataFromTaskTable($userId, $date)
    {
        $sql = "SELECT `id`, `theme`, `category`, `place`, `date`, `deadline`, `completed`, `description` FROM `task` WHERE DAY(`date`) = DAY(:date) AND `user_id` = :user_id AND `deleted_at` IS NULL;";
        try {
            $this->pstmt = $this->conn->prepare($sql);
            $this->pstmt->bindParam(':user_id', $userId);
            $this->pstmt->bindParam(':date', $date);
            $this->pstmt->execute();
        } catch (PDOException $e) {
            die('Error parsing: ' . $e->getMessage());
            exit();
        }
    }

    public function selectToEditFromTaskTable($id, $userId)
    {
        $sql = "SELECT `theme`, `category`, `place`, `date`, `deadline`, `description` FROM `task` WHERE `user_id` = :user_id AND `id` = :id AND `deleted_at` IS NULL LIMIT 1;";
        try {
            $this->pstmt = $this->conn->prepare($sql);
            $this->pstmt->bindParam(':user_id', $userId);
            $this->pstmt->bindParam(':id', $id);
            $this->pstmt->execute();
        } catch (PDOException $e) {
            die('Error parsing: ' . $e->getMessage());
            exit();
        }
    }

    public function insertDataToTaskTable($theme, $category, $place, $date, $deadline, $description, $userId)
    {
        $sql = "INSERT INTO `task` (`theme`, `category`, `place`, `date`, `deadline`, `description`, `user_id`) VALUES (:theme, :category, :place, :date, :deadline, :description, :user_id)";
        try {
            $this->pstmt = $this->conn->prepare($sql);
            $this->pstmt->bindParam(':theme', $theme);
            $this->pstmt->bindParam(':category', $category);
            $this->pstmt->bindParam(':place', $place);
            $this->pstmt->bindParam(':date', $date);
            $this->pstmt->bindParam(':deadline', $deadline);
            $this->pstmt->bindParam(':description', $description);
            $this->pstmt->bindParam(':user_id', $userId);
            $this->pstmt->execute();
        } catch (PDOException $e) {
            die('Error inserting: ' . $e->getMessage());
            exit();
        }
    }

    public function updateDataToTaskTable($id, $theme, $category, $place, $date, $deadline, $description, $userId, $completed)
    {
        $sql = "UPDATE `task` SET `theme` = :theme, `category` = :category, `place` = :place, `date` = :date, `deadline` = :deadline, `description` = :description, `completed` = :completed, `updated_at` = NOW() WHERE `user_id` = :user_id AND `id` = :id AND `deleted_at` IS NULL LIMIT 1;";
        try {
            $this->pstmt = $this->conn->prepare($sql);
            $this->pstmt->bindParam(':id', $id);
            $this->pstmt->bindParam(':theme', $theme);
            $this->pstmt->bindParam(':category', $category);
            $this->pstmt->bindParam(':place', $place);
            $this->pstmt->bindParam(':date', $date);
            $this->pstmt->bindParam(':deadline', $deadline);
            $this->pstmt->bindParam(':description', $description);
            $this->pstmt->bindParam(':completed', $completed);
            $this->pstmt->bindParam(':user_id', $userId);
            $this->pstmt->execute();
        } catch (PDOException $e) {
            die('Error updating: ' . $e->getMessage());
            exit();
        }
    }

    public function insertNewUser($username, $hashedPassword)
    {
        $sql = "INSERT INTO `user` (`username`, `password`) VALUES (:username, :password);";
        try {
            $this->pstmt = $this->conn->prepare($sql);
            $this->pstmt->bindParam(':username', $username);
            $this->pstmt->bindParam(':password', $hashedPassword);
            $this->pstmt->execute();
        } catch (PDOException $e) {
            die('Error inserting: ' . $e->getMessage());
            exit();
        }
    }

    public function checkIsValidateRegData($username)
    {
        $sql = "SELECT `username` FROM `user` WHERE `username` = :username AND `deleted_at` IS NULL LIMIT 1;";
        try {
            $this->pstmt = $this->conn->prepare($sql);
            $this->pstmt->bindParam(':username', $username);
            $this->pstmt->execute();
            if (empty($this->pstmt->fetch(PDO::FETCH_ASSOC))) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die('Error checking: ' . $e->getMessage());
            exit();
        }
    }

    public function checkIsValidateLoginData($username, $password)
    {
        $sql = "SELECT `id`, `username`, `password` FROM `user` WHERE `username` = :username AND `deleted_at` IS NULL LIMIT 1;";
        try {
            $this->pstmt = $this->conn->prepare($sql);
            $this->pstmt->bindParam(':username', $username);
            $this->pstmt->execute();
            $result = $this->pstmt->fetch(PDO::FETCH_ASSOC);

            if (empty($result)) {
                return [false, null];
            } else if (password_verify($password, $result['password'])) {
                return [true, $result['id']];
            } else {
                return [false, null];
            }
        } catch (PDOException $e) {
            die('Error checking: ' . $e->getMessage());
            exit();
        }
    }

    protected function createTableUser()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `user` (
            `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `username` VARCHAR(255) NOT NULL,
            `password` VARCHAR(255) NOT NULL,
            `created_at` DATETIME NOT NULL DEFAULT NOW(),
            `updated_at` DATETIME NOT NULL DEFAULT NOW(),
            `deleted_at` DATETIME DEFAULT NULL,
            PRIMARY KEY (`id`)
        );";
        try {
            $this->pstmt = $this->conn->prepare($sql);
            $this->pstmt->execute();
        } catch (PDOException $e) {
            die('Error creating table: ' . $e->getMessage());
            exit();
        }
    }

    public function getPreparedStatement()
    {
        return $this->pstmt;
    }
}