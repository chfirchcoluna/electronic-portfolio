CREATE DATABASE IF NOT EXISTS `calendar`;
USE `calendar`;

-- Users
CREATE TABLE IF NOT EXISTS `user` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(255) NOT NULL,
	`password` VARCHAR(255) NOT NULL,
	`created_at` DATETIME NOT NULL DEFAULT NOW(),
	`updated_at` DATETIME NOT NULL DEFAULT NOW(),
	`deleted_at` DATETIME DEFAULT NULL,
	PRIMARY KEY (`id`)
);
-- INSERT INTO `user` (`username`, `password`)
-- VALUES ('admin', 'admin');

-- Tasks
CREATE TABLE IF NOT EXISTS `task` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `theme` VARCHAR(255) NOT NULL DEFAULT '',
	`category` ENUM('Встреча', 'Звонок', 'Совещание', 'Дело') NOT NULL,
	`place` VARCHAR(255) NOT NULL DEFAULT '',
	`date` DATETIME NOT NULL DEFAULT NOW(),
	`deadline` DATETIME NOT NULL DEFAULT NOW(),
	`description` VARCHAR(255) NOT NULL DEFAULT '',
	`user_id` INT(10) UNSIGNED NOT NULL,
	`completed` TINYINT(1) NOT NULL DEFAULT 0,
	`created_at` DATETIME NOT NULL DEFAULT NOW(),
	`updated_at` DATETIME NOT NULL DEFAULT NOW(),
	`deleted_at` DATETIME DEFAULT NULL,
    PRIMARY KEY (`id`),
	FOREIGN KEY (`user_id`)  REFERENCES `user` (`id`)
);
-- INSERT INTO `task` (`theme`, `category`, `place`, `date`, `deadline`, `description`, `completed`, `user_id`)
-- VALUES
-- 	('test task 1', 'Встреча', 'Дома', '2022-12-12 12-10-16', '2022-12-12 12-10-17', 'yes', 0, 1),
-- 	('test task 2', 'Встреча', 'На улице', '2022-12-12 12-10-16', '2022-12-12 12-10-17', 'yes', 0, 1),
-- 	('test task 3', 'Встреча', 'ВУЗ', '2022-12-12 12-10-16', '2022-12-12 12-10-17', 'yes', 0, 1);
