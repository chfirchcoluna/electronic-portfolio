<?php

$register = function ($classname) {
    $path = '../classes/';
    $filename = $path . strtolower($classname) . '.php';
    $filename = str_replace('\\', '/', $filename);
    require_once($filename);
};

spl_autoload_register($register);