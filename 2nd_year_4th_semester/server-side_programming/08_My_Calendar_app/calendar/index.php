<?php

namespace app;

session_start();

if (!isset($_SESSION['username'])) {
    header("Location: parts/authorization.php");
    exit();
}

require_once('register/register.php');
require_once('header/header.php');

$controller = new Controller();
$view = new View();
$controller->setView($view);
?>

<h1>Мой календарь</h1>

<h2>Новая задача</h2>

<body>
    <form name="new_task" method="POST">
        <label for="theme">Тема:*</label>
        <input type="text" id="theme" name="theme" value="<?= ($_SESSION['theme'] ?? '') ?>" required><br>

        <label for="category">Тип:*</label>
        <select id="category" name="category">
            <option value="Встреча" <?php if (($_SESSION['category'] ?? '') === 'Встреча') {
                                        echo 'selected';
                                    } ?>>Встреча</option>
            <option value="Звонок" <?php if (($_SESSION['category'] ?? '') === 'Звонок') {
                                        echo 'selected';
                                    } ?>>Звонок</option>
            <option value="Совещание" <?php if (($_SESSION['category'] ?? '') === 'Совещание') {
                                            echo 'selected';
                                        } ?>>Совещание</option>
            <option value="Дело" <?php if (($_SESSION['category'] ?? '') === 'Дело') {
                                        echo 'selected';
                                    } ?>>Дело</option>
        </select><br>

        <label for="place">Место:*</label>
        <input type="text" id="place" name="place" value="<?= ($_SESSION['place'] ?? '') ?>" required><br>

        <label for="date">Дата и время:*</label>
        <input type="datetime-local" id="date" name="date" value="<?= ($_SESSION['date'] ?? '') ?>" required><br>

        <label for="deadline">Истечение в:*</label>
        <input type="datetime-local" id="deadline" name="deadline" value="<?= ($_SESSION['deadline'] ?? '') ?>" required><br>

        <label for="description">Комментарий:*</label>
        <input type="text" id="description" name="description" value="<?= ($_SESSION['description'] ?? '') ?>" required><br>

        <input name="submit" type="submit" value="Добавить">
    </form>

    <form name="showtable" method="POST">
        <label for="taskcategory">Тип:*</label>
        <select id="taskcategory" name="taskcategory">
            <option value="Текущие задачи" <?php if (($_POST['taskcategory'] ?? '') === 'Текущие задачи') {
                                                echo 'selected';
                                            } ?>>Текущие задачи</option>
            <option value="Просроченные задачи" <?php if (($_POST['taskcategory'] ?? '') === 'Просроченные задачи') {
                                                    echo 'selected';
                                                } ?>>Просроченные задачи</option>
            <option value="Выполненные задачи" <?php if (($_POST['taskcategory'] ?? '') === 'Выполненные задачи') {
                                                    echo 'selected';
                                                } ?>>Выполненные задачи</option>
            <option value="Задачи по дате" <?php if (($_POST['taskcategory'] ?? '') === 'Задачи по дате') {
                                                echo 'selected';
                                            } ?>>Задачи по дате</option>
        </select><br>

        <label for="taskdate">Дата и время:</label>
        <input type="datetime-local" id="taskdate" name="taskdate" value="<?php if (!isset($_POST['taskdate'])) {
                                                                                echo date('Y-m-d h:i', time());
                                                                            } else {
                                                                                echo $_POST['taskdate'];
                                                                            } ?>"><br>

        <input name="showsubmit" type="submit" value="Показать календарь">
    </form>
</body>

<a href="parts/exit.php">Выход</a>

<?php
require_once('parts/show-submit.php');
require_once('parts/submit.php');
require_once('footer/footer.php');
?>