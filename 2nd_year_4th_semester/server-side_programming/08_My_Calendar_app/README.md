<h2 align="center">My Calendar</h2>

## 08 Task:

### Create a small web application "My Calendar".

1. On the home page you need to place a form with fields:
Subject,
Type: appointment, call, meeting, case,
Location,
Date and Time,
Duration,
Comment
2. You can expand the list of fields as you wish
3. Current tasks will be displayed below the form (or on a separate page).
4. Provide the possibility to go to the list of
current tasks
overdue tasks,
completed tasks,
tasks for a specific date.
5. By clicking on a task we go to the task card, where we can edit it.
6. Authorization is not required, but will be an additional plus.
7. To execute, we will need to design the database and create the necessary tables (we need to provide SQL code to create the application tables).
8. MVC approach is preferred.

## Demonstration of work [Calendar App](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/server-side_programming/08_My_Calendar_app/calendar/index.php):

![1](1.png "1")
![2](2.png "2")
![3](3.png "3")
![4](4.png "4")
![5](5.png "5")