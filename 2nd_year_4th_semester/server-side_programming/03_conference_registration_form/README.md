## 03 Task:

### Exercise 1
1. Create an html form to submit a conference application. Request:
participant's first name,
last name,
e-mail address
and phone number for contact,
the conference topic of interest:
Business,
Technology,
Advertising and Marketing,
preferred method of payment for participation:
WebMoney,
Yandex.Money,
PayPal,
credit card
specify his/her wish to receive the conference newsletter.
2. Accept the client's data, check that all fields are filled in.
In case of an error, inform the user about it (the already entered data should not be lost).
If there are no errors, save the received data to a file (you can also make a note about the date and time of sending the application).
The file name should be unique so that the next request does not overwrite the existing one.
3. Inform the user that his/her request has been successfully accepted.

### Exercise 2
1. Create a separate page for the administrator where you can view all the requests left as a list.
Let's assume that you don't need to lock the page with the administrator's password.
2. Provide an opportunity to select one or more requests (by checkboxes) to delete them by pressing the "Delete" button, which will send a form with a list of checked requests to be deleted.


## Demonstration of work [index.php](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_3rd_semester/server-side_programming/03_conference_registration_form/Form/indexphp.php):

![1](1.png "1")
![2](2.png "2")