<?php include_once('parts/headReg.php') ?>

<body>

<h1>Форма заявки на участие в конференции</h1>

<form name="registration_conference" method="POST">
    <label for="username">Имя:*</label>
    <input type="text" id="username" name="username" value="<?= ($_POST['username'] ?? '') ?>" required><br>

    <label for="surname">Фамилия:*</label>
    <input type="text" id="surname" name="surname" value="<?= ($_POST['surname'] ?? '') ?>" required><br>

    <label for="email">Email:*</label>
    <input type="email" id="email" name="email" value="<?= ($_POST['email'] ?? '') ?>" required><br>

    <label for="phone">Телефон:*</label>
    <input type="tel" id="phone" name="phone" value="<?= ($_POST['phone'] ?? '') ?>" required><br>

    <label for="interest">Тематика конференции:*</label>
    <select id="interest" name="interest" value="<?= ($_POST['interest'] ?? '') ?>" required>
        <option style="display:none" value=''>Выберите тематику</option>
        <option value="Бизнес" <?php if($_POST['interest'] === 'Бизнес') {echo "selected";}?>>Бизнес</option>
        <option value="Технологии" <?php if($_POST['interest'] === 'Технологии') {echo "selected";}?>>Технологии</option>
        <option value="Реклама и Маркетинг" <?php if($_POST['interest'] === 'Реклама и Маркетинг') {echo "selected";}?>>Реклама и Маркетинг</option>
    </select><br>

    <label for="payment-method">Метод оплаты:*</label>
    <select id="payment-method" name="paymentMethod" required>
        <option style="display:none" value=''>Выберите метод оплаты</option>
        <option value="WebMoney" <?php if($_POST['paymentMethod'] === 'WebMoney') {echo "selected";}?>>WebMoney</option>
        <option value="Яндекс.Деньги" <?php if($_POST['paymentMethod'] === 'Яндекс.Деньги') {echo "selected";}?>>Яндекс.Деньги</option>
        <option value="PayPal" <?php if($_POST['paymentMethod'] === 'PayPal') {echo "selected";}?>>PayPal</option>
        <option value="Кредитная карта" <?php if($_POST['paymentMethod'] === 'Кредитная карта') {echo "selected";}?>>Кредитная карта</option>
    </select><br>

    <label for="receive-mailing">Желаете получать рассылку о конференции?
    <input type="checkbox" id="receive-mailing" name="receiveMailing" <?php if($_POST['receiveMailing'] === 'on') {echo "checked";}?>></label><br>
    <button type="submit">Отправить заявку</button>
</form>

</body>

<?php include_once('parts/submit.php')?>