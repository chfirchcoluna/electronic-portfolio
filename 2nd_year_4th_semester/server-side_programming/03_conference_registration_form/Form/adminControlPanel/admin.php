<html>
<head>
    <meta charset="UTF-8">
    <title>Страница администратора</title>
</head>
<body>

<h1>Список заявок</h1>

<form action="delete.php" method="POST">
    <table>
        <tr>
            <th>№</th>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Email</th>
            <th>Телефон</th>
            <th>Тематика</th>
            <th>Метод оплаты</th>
            <th>Рассылка о конференции</th>
            <th>Время отправки заявки</th>
            <th>Удалить</th>
        </tr>
        <?php
        $files = glob('../requests/*.txt');
        $count = 0;
        foreach ($files as $file) {
            $data = file_get_contents($file);
            $lines = explode("\n", $data);
            $id = $count;
            $count++;

            echo '<tr>';
            echo '<td>' . $id . '</td>';
            foreach ($lines as $line) {
                echo '<td>' . substr($line, strpos($line, ':') + 1) . '</td>';
            }
            echo '<td><input type="checkbox" name="delete[]" value="' . $file . '"></td>';
            echo '</tr>';
        }
        ?>
    </table>
    <br>
    <input type="submit" name="submit" value="Удалить">
</form>
</body>
</html>