<?php

if ($_POST) {

    $username = htmlentities($_POST['username']);
    $surname = htmlentities($_POST['surname']);
    $email = htmlentities($_POST['email']);
    $phone = htmlentities($_POST['phone']);
    $interest = htmlentities($_POST['interest']);
    $paymentMethod = htmlentities($_POST['paymentMethod']);
    $receiveMailing = isset(($_POST['receiveMailing'])) ? 'Да' : 'Нет';
    $receiveTime = date('Y-m-d-H-i-s');

    if (empty($username) || empty($surname) || empty($email) || empty($phone) || empty($interest) || empty($paymentMethod)) {
        $message = 'Пожалуйста, заполните все обязательные поля!';
    } else {
        if (strlen($username) > 85 || strlen($surname) > 85 || strlen($email) > 85 || strlen($phone) > 85 || strlen($interest) > 85 || strlen($paymentMethod) > 85) {
            $message = 'Пожалуйста, не пишите больше чем 85 символов в ячейке!';
        } else {
            $filename = 'requests/req_' . date('YmdHis') . '_' . $username . '_' . $surname . '_' . rand(100000000, 999999999) .'.txt';

            // Создаем файл и записываем данные в него
            $file = fopen($filename, 'w');
            fwrite($file, "Имя: $username\r\n");
            fwrite($file, "Фамилия: $surname\r\n");
            fwrite($file, "Email: $email\r\n");
            fwrite($file, "Телефон: $phone\r\n");
            fwrite($file, "Тематика: $interest\r\n");
            fwrite($file, "Метод оплаты: $paymentMethod\r\n");
            fwrite($file, "Получать рассылку: $receiveMailing\r\n");
            fwrite($file, "Время отправки заявки: $receiveTime\r\n");
            fclose($file);
            $message = 'Ваша заявка успешно отправлена!';
        }
    }
    echo '<h2>' . $message . '</h2>';
}
?>
