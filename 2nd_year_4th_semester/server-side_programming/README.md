<h1 align="center">Server-side Programming</h1>

### About subject:
<p align="left">
Goals: To form a set of theoretical knowledge and practical skills within the framework of general cultural and professional competencies in the direction of
To form a set of theoretical knowledge and practical skills within the framework of general cultural and professional competencies that ensure successful activity in the given field of modern IT-technologies.
field of modern IT-technologies.

Objectives: To teach to work with PHP language, to understand web development, to familiarize with modern professional tools.
</p>

<h2></h2>

## [00 TASK getting to know HTTP](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/server-side_programming/00_getting_to_know_HTTP)
## [01 TASK multiplication table](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/server-side_programming/01_multiplication_table)
## [02 TASK classes and objects](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/server-side_programming/02_classes_and_objects)
## [03 TASK conference registration_form](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/server-side_programming/03_conference_registration_form)
## [04 TASK regular expressions](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/server-side_programming/04_regular_expressions)
## [05 TASK string functions](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/server-side_programming/05_&_06_string_functions_&_working_with_sessions)
## [06 TASK working with sessions](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/server-side_programming/05_&_06_string_functions_&_working_with_sessions)
## [07 TASK working with the database](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/server-side_programming/07_working_with_the_database)
## [08 TASK My Calendar App](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/server-side_programming/08_My_Calendar_app)