<h1 align="center">Request Game</h1>

### About Request Game:
Welcome! We created this game for learning languages through fun stories. Our team of three students from ISU made it possible. We want you to have a great time while improving your language skills.

In the game, you'll find stories written by ChatGPT, a smart computer program. These stories will help you learn languages in an enjoyable way. We also use technology called Dalle2 and a speech synthesizer from Yandex to make the stories come alive.

So, get ready to have fun and learn! Dive into the fascinating stories, listen to them, and explore the game. You'll improve your language skills while having a great time.

Remember, practice is key. By immersing yourself in the stories and interacting with the game, you'll become more proficient in the language you're learning. Enjoy the journey of language learning with our game!

Let's get started and unlock new language horizons. Happy learning!
</p>

<h2></h2>

# Git Link and Demonstration:

<h2><a href="https://gitlab.com/chfirchcoluna/request_game">Request Game</a></h2>