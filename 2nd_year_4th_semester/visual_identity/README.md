<h1 align="center">Visual Identidy</h1>

### About subject:
<p align="left">
Goals: To form an understanding of the principles, logic and technologies of the modern
design project and design product.

Objectives:
- Study of modern design in the field of advertising and corporate communications,
web-design, interface design, electronic presentations;
- Development and approval of the brief. Stages of work;
- Development of the concept of graphic design of a digital product and its realisation
in selected software environments.
</p>

<h2></h2>

## Solutions to initial problems: [TASK 1-6](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/visual_identity/1-6_tasks.pdf)
### Draft brief:
### Description task Ru:

1. Описать предполагаемый вид деятельности.
2. Описать какой товар, услугу вы предлагаете на рынке.
3. Чем ваш продукт отличается от продуктов конкурентов.
4. С какими ассоциациями связано название вашего продукта.
5. Название торгового знака. Проверить на доступность. Историческое происхождение слова/слов. Ассоциации, связанные с этим словом.
6. Макет шрифтового логотипа.
7. Макет графического логотипа. Использовать разные стили
8. Макет комбинированного логотипа.

### Description task En:

1. Describe the intended type of activity.
2. Describe what product or service you offer on the market.
3. How does your product differ from competitors products?
4. What associations the name of your product is associated with.
5. The name of the trademark. Check for availability. The historical origin of the word/words. Associations associated with this word.
6. Layout of the font logo.
7. Layout of the graphic logo. Use different styles
8. Layout of the combined logo.

## Solution: [TASK 7](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/visual_identity/7_task.pdf)

### Guideline:
### Description task Ru:

Создать pdf-файл с описанием элементов айдентики и правилами их использования.

### Description task En:

Create a pdf file describing the elements of the identity and the rules for their use.

## Solution: [TASK 8](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/visual_identity/8_task.pdf)

### Final work. Development of a set of identity for the direction of "Applied Informatics":
### Description task Ru:

Разработать и представить набор элементов визуальной айдентики для направления обучения "Прикладная информатика". 

Содержание работы с элементами айдентики:

1. Подбор ассоциативных слов, сочетаний;
2. Анализ подобных разработок сторонних вузов;
3. На основе выборки слов обоснование цветовой схемы;
4. На основе выборки слов разработка набора иконок;
5. Разработка логотипа (текстовый, графический, комбинированный);
6. Варианты динамической модификации логотипа;
7. Маскот;
8. Паттерны;
9. Набор мокапов;
10. Набор шаблонов для оформления презентаций (макеты)
11. Оформление соцсетей;
12. Вариант оформления сайта*
13. Оформить гайдлайн и описать правила использования элементов айдентики.

### Description task En:

To develop and present a set of visual identity elements for the direction of study "Applied Informatics". 

The content of work with the elements of the identity:

1. Selection of associative words, combinations;
2. Analysing similar developments of third-party universities. 3;
3. Justification of the colour scheme based on the selection of words;
4. On the basis of a selection of words, the development of a set of icons;
5. Logo development (text, graphic, combined);
6. Options for dynamic modification of the logo;
7. Mascot;
8. Patterns;
9. A set of mocaps;
10. Set of templates for presentation design (layouts)
11. design of social networks;
12. Variant of website design*
13. Design a guideline and describe the rules of using the elements of the identity.

## Solution: [TASK 9](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/visual_identity/9_task.pdf)