<h1 align="center">Summer Practice</h1>

### About subject:
<p align="left">
The main purpose of the internship is to apply theoretical knowledge in practical activities and to develop professional skills and abilities in the creation, implementation, analysis and maintenance of professionally oriented information technologies and information systems shells.  

Tasks of practice on obtaining primary professional skills and experience of professional activity:

1. the ability to search, critically analyse and synthesize information, apply a systematic approach to solve problems;
2. ability to define the range of tasks within the set goal and choose the best ways of their solution, based on the current legal norms, available resources and limitations.
3. ability to carry out business communication orally and in writing in the state language of the Russian Federation and foreign language(s).
4. ability to develop software components of web, multimedia, mobile applications and services, information systems of digital design, computer art;
5. ability to implement, adapt and use application software necessary for the development of web services, projects in the field of computer design and graphics, visual communications, implementation of web, mobile and multimedia applications.
6. ability to develop graphic and information design, as well as its separate elements for information systems and services, web, mobile and multimedia applications, visual communications;
7. ability to conduct research and development work on the development of information systems and their software components in the field of computer design and graphics, visual communications, implementation of web, mobile and multimedia applications.

The object of the research is the basics of typography.
The subject of the study is font compositions in design.

</p>

<h2></h2>

## [DEMONSTRATION OF SUMMER PRACTICE](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/summer_practice/summer_practice.pdf)