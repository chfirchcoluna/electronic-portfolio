<h1 align="center">Web Application Design</h1>

### About subject:
<p align="left">
    Study of modern approaches in the field of web application design, obtaining skills and competences within the framework of general cultural and professional competences that ensure successful activity in this field of modern IT technologies.
    successful activity in this area of modern IT-technologies.
    The task is to teach how to work with tools for creating layouts for the design of web-applications.
    web applications.
</p>

<h2></h2>

### 00 Task:
<p align="left">
    Select several websites that you like and dislike.
    Evaluate their usability from UX and UI perspectives, describing both positive and negative aspects.
    Propose your edits, suggesting improvements to the design and providing justification for your decisions.
    Also, pay attention to the responsiveness of the design and layout. 
</p>

## Solution: [Evaluation of website design](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/web_application_design/0_evaluation_of_website_design.pdf)

### 01 Task:
<p align="left">
    Select any existing website with several pages and/or sections.
    Describe the structure of the entire website, the structure can be represented as a tree or a mind map. Suggest changes to the structure.
    Describe the structure of the main page of the site. Suggest changes to the structure. 
</p>

## Solution: [Evaluation the structure of the site](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/web_application_design/1_evaluating_the_structure_of_the_site.pdf)

### 02 Task:
<p align="left">
    Design documentation for the future site.
    Describe in the document the goals and context of the site, its main features, structure and content.
</p>

## Solution: [Website project](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/blob/main/2nd_year_4th_semester/web_application_design/2_website_project.pdf)

### 03 Task:
<p align="left">
Design the website according to your project.

Recommendations:
1. Use Adobe XD, Figma or Sketch programmes.
2. Adaptability: desktop, mobile, possibly tablet view.
3. Use components. Place components on a separate sheet for clarity.
4. Provide basic states (buttons, form fields, links) for major components.
5. Choose colours responsibly.
6. Content area width: 1100-1200 px. Centre it relative to the width of the viewport.
7. Base font size: 14-16 px.
8. Stick to a mostly 12-column grid.
9. There should be no more than 10-12 words (70-80 characters) per paragraph line.
10. Use an 8px module.
11. You should not use paid and/or rare fonts. It is better to use something from Google Fonts.
</p>

## Solution: 
## [Front Page View](https://www.figma.com/proto/bXlkSFdQurOsSJA2S29szz/Sushinka-Suslikov?page-id=0%3A1&type=design&node-id=11-282&viewport=531%2C207%2C0.27&t=CFDiY8hZlBShfkHf-1&scaling=min-zoom&starting-point-node-id=11%3A282&show-proto-sidebar=1&mode=design) 
## [Primary Page View](https://www.figma.com/proto/bXlkSFdQurOsSJA2S29szz/Sushinka-Suslikov?page-id=159%3A4&type=design&node-id=159-6&viewport=25%2C246%2C0.46&t=pARe5RHN01aBvQ6h-1&scaling=min-zoom&starting-point-node-id=159%3A6&show-proto-sidebar=1&mode=design)

## [Project](https://www.figma.com/file/bXlkSFdQurOsSJA2S29szz/Sushinka-Suslikov?type=design&node-id=0%3A1&mode=design&t=HHmJOGH7CZd3wiEy-1)

## Demonstration:

![1](1.png "1")
![2](2.png "2")
![3](3.png "3")
![4](4.png "4")
![5](5.png "5")
![6](6.png "6")
![7](7.png "7")
![8](8.png "8")
![9](9.png "9")
![10](10.png "10")
![11](11.png "11")
![12](12.png "12")
![13](13.png "13")
![14](14.png "14")
![15](15.png "15")
![16](16.png "16")
![17](17.png "17")
![18](18.png "18")
![19](19.png "19")
![20](20.png "20")
![21](21.png "21")
![22](22.png "22")
![23](23.png "23")
![24](24.png "24")
![25](25.png "25")
![26](26.png "26")
![27](27.png "27")
![28](28.png "28")
![29](29.png "29")
