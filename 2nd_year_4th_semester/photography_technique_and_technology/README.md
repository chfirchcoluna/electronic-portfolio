<h1 align="center">Photography Technique and Technology</h1>

### About subject:
<p align="left">
Objectives: To equip students with professional skills in the field of photography
and mastering the theory and practice of the basics of photography.

Tasks: studying the technique of photography, studying the main genres of photography and their
use for further work in the sphere of advertising and PR, mastering of technical and creative methods of photography, mastering of specifics and technologies of commercial photography.
</p>

<h2></h2>

## [Demonstration](https://gitlab.com/chfirchcoluna/electronic-portfolio/-/tree/main/2nd_year_4th_semester/photography_technique_and_technology/photography.pdf)


