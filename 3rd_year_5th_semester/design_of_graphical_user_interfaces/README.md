<h1 align="center">Design of Graphical User Interfaces</h1>

### About subject:
<p align="left">
Goals: Formation of knowledge about means and methods of human-machine interaction as an object of engineering design.
Objectives: The main task of the discipline is to develop students' skills in
designing quality user interfaces, and as a consequence the development of effective information systems.
effective information systems.
</p>

<h2></h2>

# Git Link:

<h2><a href="https://gitlab.com/chfirchcoluna/design">Design of Graphical User Interfaces</a></h2>